@extends('layouts.cork')

@section('content')

    @can('isAdminAkademik')
        <div id="tableHover" class="col-lg-4 col-4 layout-spacing">
            <div class="statbox widget box box-shadow">
                <div class="widget-header">
                    <div class="row">
                        <div class="col-xl-12 col-md-12 col-sm-12 col-12" style="text-align: center">
                            <h4>Tahun Akademik Aktif</h4>
                            <br>
                            <h4>2021/2022</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="tableHover" class="col-lg-4 col-4 layout-spacing">
            <div class="statbox widget box box-shadow">
                <div class="widget-header">
                    <div class="row">
                        <div class="col-xl-12 col-md-12 col-sm-12 col-12" style="text-align: center">
                            <h4>Semester Aktif</h4>
                            <br>
                            <h4>Ganjil</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="tableHover" class="col-lg-4 col-4 layout-spacing">
            <div class="statbox widget box box-shadow">
                <div class="widget-header">
                    <div class="row">
                        <div class="col-xl-12 col-md-12 col-sm-12 col-12" style="text-align: center">
                            <h4>Jumlah Mahasiswa Pengisi KRS</h4>
                            <br>
                            <h4>100</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endcan
    @can('isAdmin')
        <div id="tableHover" class="col-lg-12 col-12 layout-spacing">
            <div class="statbox widget box box-shadow">
                <div class="widget-header">
                    <div class="row">
                        <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                            <h4>DATA KAMPUS</h4>
                        </div>
                    </div>
                </div>
                <div class="widget-content widget-content-area">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover mb-4">
                            <thead>
                                <tr>
                                    <th>Nama</th>
                                    <th>Logo</th>
                                    <th>Akreditasi</th>
                                    <th class="text-center">Deskripsi</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Shaun Park</td>
                                    {{-- <td>10/08/2020</td> --}}
                                    <td>320</td>
                                    <td class="text-center"><span class="text-success">Complete</span></td>
                                    <td class="text-center"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                            viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                                            stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash-2 icon">
                                            <polyline points="3 6 5 6 21 6"></polyline>
                                            <path
                                                d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2">
                                            </path>
                                            <line x1="10" y1="11" x2="10" y2="17"></line>
                                            <line x1="14" y1="11" x2="14" y2="17"></line>
                                        </svg></td>
                                    <td class="text-center">
                                        <a href="">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                                fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                                stroke-linejoin="round" class="feather feather-edit-2">
                                                <path d="M17 3a2.828 2.828 0 1 1 4 4L7.5 20.5 2 22l1.5-5.5L17 3z"></path>
                                            </svg>
                                        </a>
                                        <form action="" method="POST" onsubmit="return confirm('Hapus Data, Anda Yakin ?')">
                                            {!! method_field('delete') . csrf_field() !!}
                                            <button class="dropdown-item" type="submit">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                    viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                                                    stroke-linecap="round" stroke-linejoin="round"
                                                    class="feather feather-trash-2 icon">
                                                    <polyline points="3 6 5 6 21 6"></polyline>
                                                    <path
                                                        d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2">
                                                    </path>
                                                </svg>
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    @endcan
    @can('isAdminDosen')
        <div id="tableHover" class="col-lg-12 col-12 layout-spacing">
            <div class="statbox widget box box-shadow">
                <div class="widget-header">
                    <div class="row">
                        <div class="col-xl-6 col-md-6 col-sm-6 col-6">
                            <h4>Anton Susilo</h4>
                            <br>
                            <h5 style="margin-left: 15px">Dosen</h5>
                        </div>
                        <div class="col-xl-6 col-md-6 col-sm-6 col-6 text-right">
                            <h4>Semester Ganjil TA 2021/2022</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="tableHover" class="col-lg-4 col-4 layout-spacing">
            <div class="statbox widget box box-shadow">
                <div class="widget-header">
                    <div class="row">
                        <div class="col-xl-12 col-md-12 col-sm-12 col-12" style="text-align: center">
                            <h4>Kampus Tempat Mengajar</h4>
                            <br>
                            <h4>2</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="tableHover" class="col-lg-4 col-4 layout-spacing">
            <div class="statbox widget box box-shadow">
                <div class="widget-header">
                    <div class="row">
                        <div class="col-xl-12 col-md-12 col-sm-12 col-12" style="text-align: center">
                            <h4>Prodi Diampu</h4>
                            <br>
                            <h4>2</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="tableHover" class="col-lg-4 col-4 layout-spacing">
            <div class="statbox widget box box-shadow">
                <div class="widget-header">
                    <div class="row">
                        <div class="col-xl-12 col-md-12 col-sm-12 col-12" style="text-align: center">
                            <h4>Absensi Mahasiswa</h4>
                            <br>
                            <h4>2</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="tableHover" class="col-lg-4 col-4 layout-spacing">
            <div class="statbox widget box box-shadow">
                <div class="widget-header">
                    <div class="row">
                        <div class="col-xl-12 col-md-12 col-sm-12 col-12" style="text-align: center">
                            <h4>Cek jadwal Mengajar</h4>
                            <br>
                            <h4>2</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    @endcan

@endsection
