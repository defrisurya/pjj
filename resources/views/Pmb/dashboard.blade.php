@extends('Pmb.layouts.app')

@section('title', 'Dashboard Mahasiswa Baru')

@section('content')
    <!-- Mobile Menu end -->
    <div class="breadcome-area">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="breadcome-list">
                        <div class="row">
                            <div class="col-lg-12 col-md-6 col-sm-6 col-xs-12">
                                <ul class="text-center">
                                    <li class="nama0">
                                        <span class="text-center">Pendaftaran Anda Telah Berhasil !</span>
                                    </li>
                                    <li class="status0">
                                        <span>Silahkan Menunggu Informasi Lanjutan Dari Admin..</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection
