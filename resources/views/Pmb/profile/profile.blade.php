@extends('Mahasiswa.layouts.app')

@section('title', 'Profile Mahasiswa')

@section('content')
    <div class="breadcome-area">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="breadcome-list">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <ul class="breadcome-heading">
                                    <li>
                                        <span class="nama">Profile Mahasiswa</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="analytics-sparkle-area">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-6 col-sm-6 col-xs-12">
                    <div class="sparkline8-list">
                        <div class="sparkline8-hd">
                            <div class="portlet-title">
                            </div>
                        </div>
                        <div class="sparkline12-graph">
                            <div class="basic-login-form-ad">
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="all-form-element-inner">
                                            <form action="#">
                                                <div class="form-group-inner">
                                                    <label>Nomor Induk Mahasiswa (NIM)</label>
                                                    <input type="number" name="no_induk_mahasiswa" id="nim" class="form-control" />
                                                </div>
                                                <div class="form-group-inner">
                                                    <label>Nomor Induk Kependudukan (NIK)</label>
                                                    <input type="number" name="no_induk_kependudukan" id="nik" class="form-control" />
                                                </div>
                                                <div class="form-group-inner">
                                                    <label>Nama Lengkap Mahasiswa</label>
                                                    <input type="text" name="nama_lengkap" id="nama" class="form-control" />
                                                </div>
                                                <div class="form-group-inner form-select-list">
                                                    <label>Jenis Kelamin</label>
                                                    <select name="jenis_kelamin" class="form-control custom-select-value">
                                                        <option value="">-- Pilih Jenis Kelamin --</option>
                                                        <option value="Laki-Laki">Laki-Laki</option>
                                                        <option value="Perempuan">Perempuan</option>
                                                    </select>
                                                </div>
                                                <div class="form-group-inner form-select-list">
                                                    <label>Program Studi</label>
                                                    <select name="prodi_id" class="form-control custom-select-value">
                                                        <option value="">-- Pilih Program Studi --</option>
                                                        <option value="Sistem Informasi">Sistem Informasi</option>
                                                        <option value="Teknik Informatika">Teknik Informatika</option>
                                                        <option value="Manajemen Informatika">Manajemen Informatika</option>
                                                    </select>
                                                </div>
                                                <div class="form-group-inner">
                                                    <label>Agama</label>
                                                    <input type="text" name="agama" id="agama" class="form-control" />
                                                </div>
                                                <div class="form-group-inner form-select-list">
                                                    <label>Kewarganegaraan</label>
                                                    <select name="kewarganegaraan" class="form-control custom-select-value">
                                                        <option value="">-- Pilih Kewarganegaraan --</option>
                                                        <option value="WNI">WNI</option>
                                                        <option value="WNA">WNA</option>
                                                    </select>
                                                </div>
                                                <div class="form-group-inner">
                                                    <label>Tempat Lahir</label>
                                                    <input type="text" name="tempat_lahir" id=""
                                                        class="form-control" />
                                                </div>
                                                <div class="form-group-inner">
                                                    <label>Tanggal Lahir</label>
                                                    <input type="date" name="tanggal_lahir" id=""
                                                        class="form-control" />
                                                </div>
                                                <div class="form-group-inner">
                                                    <label>E-mail</label>
                                                    <input type="text" name="email" id="email" class="form-control" />
                                                </div>
                                                <div class="form-group-inner">
                                                    <label>Nomor Handphone</label>
                                                    <input type="number" name="no_telpon" id="no_hp" class="form-control" />
                                                </div>
                                                <div class="form-group-inner">
                                                    <label>Asal Sekolah</label>
                                                    <input type="text" name="asal_sekolah" id="asal_sekolah"
                                                        class="form-control" />
                                                </div>
                                                <div class="form-group-inner">
                                                    <label>Penerima KPS (Kartu Perlindungan Sosial)</label>
                                                    <div class="bt-df-checkbox">
                                                        <label>
                                                            <input class="pull-left radio-checked" type="radio" checked=""
                                                                value="Ya" id="optionsRadios1" name="optionsRadios">
                                                            YA</label>
                                                    </div>
                                                    <div class="bt-df-checkbox">
                                                        <label>
                                                            <input class="pull-left" type="radio" value="Tidak"
                                                                id="optionsRadios2" name="optionsRadios"> TIDAK</label>
                                                    </div>
                                                </div>
                                                <div class="form-group-inner">
                                                    <label>Penerima KPS (YA/TIDAK)</label>
                                                    <input type="text" name="penerima_kps" id="penerima_kps"
                                                        class="form-control" />
                                                </div>
                                                <div class="form-group-inner">
                                                    <label>Alamat</label>
                                                    <textarea name="alamat" id="alamat" class="form-control"></textarea>
                                                </div>
                                                <div class="form-group-inner">
                                                    <label>Kecamatan</label>
                                                    <input type="text" name="kecamatan" id="kecamatan"
                                                        class="form-control" />
                                                </div>
                                                <div class="form-group-inner">
                                                    <label>Kabupaten</label>
                                                    <input type="text" name="kabupaten" id="kabupaten"
                                                        class="form-control" />
                                                </div>
                                                <div class="form-group-inner">
                                                    <label>Provinsi</label>
                                                    <input type="text" name="provinsi" id="provinsi"
                                                        class="form-control" />
                                                </div>
                                                <div class="form-group-inner">
                                                    <label>Kode Pos</label>
                                                    <input type="number" name="kode_pos" id="kode_pos"
                                                        class="form-control" />
                                                </div>
                                                <div class="breadcome-list">
                                                    <div class="row">
                                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                            <ul class="breadcome-heading">
                                                                <li>
                                                                    <span class="nama">
                                                                        Biodata Ayah Kandung
                                                                    </span>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group-inner">
                                                    <label>Nomor Induk Kependudukan (NIK)</label>
                                                    <input type="number" name="no_induk_kependudukan_ayah" id="nik_ayah"
                                                        class="form-control" />
                                                </div>
                                                <div class="form-group-inner">
                                                    <label>Nama Lengkap</label>
                                                    <input type="text" name="nama_lengkap_ayah" id="nama_ayah"
                                                        class="form-control" />
                                                </div>
                                                <div class="form-group-inner">
                                                    <label>Tanggal Lahir</label>
                                                    <input type="date" name="tanggal_lahir_ayah" id="tgl_lahir_ayah"
                                                        class="form-control" />
                                                </div>
                                                <div class="form-group-inner form-select-list">
                                                    <label>Pendidikan Terakhir</label>
                                                    <select name="pendidikan_ayah" class="form-control custom-select-value">
                                                        <option value="">-- Pilih Pendidikan Terakhir --</option>
                                                        <option value="SD">SD</option>
                                                        <option value="SMP">SMP</option>
                                                        <option value="SMA">SMA</option>
                                                        <option value="Sarjana">Sarjana</option>
                                                        <option value="Diploma">Diploma</option>
                                                    </select>
                                                </div>
                                                <div class="form-group-inner form-select-list">
                                                    <label>Pekerjaan</label>
                                                    <select name="pekerjaan_ayah" class="form-control custom-select-value">
                                                        <option value="">-- Pilih Pekerjaan --</option>
                                                        <option value="Buruh">Buruh</option>
                                                        <option value="Wiraswasta">Wiraswasta</option>
                                                        <option value="PNS">PNS</option>
                                                    </select>
                                                </div>
                                                <div class="form-group-inner">
                                                    <label>Rentang Penghasilan</label>
                                                    <input type="text" name="penghasilan_ayah" id="penghasilan_ayah"
                                                        class="form-control" />
                                                </div>
                                                <div class="form-group-inner">
                                                    <label>Nomor Handphone</label>
                                                    <input type="number" name="no_telpon_ayah" id="no_telpon_ayah"
                                                        class="form-control" />
                                                </div>
                                                <div class="form-group-inner">
                                                    <label>Alamat</label>
                                                    <textarea name="alamat_ayah" id="alamat_ayah"
                                                        class="form-control"></textarea>
                                                </div>
                                                <div class="breadcome-list">
                                                    <div class="row">
                                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                            <ul class="breadcome-heading">
                                                                <li>
                                                                    <span class="nama">
                                                                        Biodata Ibu Kandung
                                                                    </span>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group-inner">
                                                    <label>Nomor Induk Kependudukan (NIK)</label>
                                                    <input type="number" name="no_induk_kependudukan_ibu" id="no_induk_kependudukan_ibu"
                                                        class="form-control" />
                                                </div>
                                                <div class="form-group-inner">
                                                    <label>Nama Lengkap</label>
                                                    <input type="text" name="nama_lengkap_ibu" id="nama_lengkap_ibu"
                                                        class="form-control" />
                                                </div>
                                                <div class="form-group-inner">
                                                    <label>Tanggal Lahir</label>
                                                    <input type="date" name="tanggal_lahir_ibu" id="tanggal_lahir_ibu"
                                                        class="form-control" />
                                                </div>
                                                <div class="form-group-inner form-select-list">
                                                    <label>Pendidikan Terakhir</label>
                                                    <select name="pendidikan_ibu" class="form-control custom-select-value">
                                                        <option value="">-- Pilih Pendidikan Terakhir --</option>
                                                        <option value="SD">SD</option>
                                                        <option value="SMP">SMP</option>
                                                        <option value="SMA">SMA</option>
                                                        <option value="Sarjana">Sarjana</option>
                                                        <option value="Diploma">Diploma</option>
                                                    </select>
                                                </div>
                                                <div class="form-group-inner form-select-list">
                                                    <label>Pekerjaan</label>
                                                    <select name="pekerjaan_ibu" class="form-control custom-select-value">
                                                        <option value="">-- Pilih Pekerjaan --</option>
                                                        <option value="Ibu Rumah Tangga">Ibu Rumah Tangga</option>
                                                        <option value="Buruh">Buruh</option>
                                                        <option value="Wiraswasta">Wiraswasta</option>
                                                        <option value="PNS">PNS</option>
                                                    </select>
                                                </div>
                                                <div class="form-group-inner">
                                                    <label>Rentang Penghasilan</label>
                                                    <input type="text" name="penghasilan_ibu" id="penghasilan_ibu"
                                                        class="form-control" />
                                                </div>
                                                <div class="form-group-inner">
                                                    <label>Nomor Handphone</label>
                                                    <input type="number" name="no_telpon_ibu" id="no_telpon_ibu"
                                                        class="form-control" />
                                                </div>
                                                <div class="form-group-inner">
                                                    <label>Alamat</label>
                                                    <textarea name="alamat_ibu" id="alamat_ibu"
                                                        class="form-control"></textarea>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="breadcome-list link-submit">
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <a href="{{ route('mhs') }}" class="link-profile">
                                        <button class="btn btn-primary btn-md">
                                            Kembali
                                        </button>
                                    </a>
                                    <button type="submit" class="btn btn-success btn-md">
                                        Simpan
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
