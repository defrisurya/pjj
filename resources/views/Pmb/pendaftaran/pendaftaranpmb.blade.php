@extends('Pmb.layouts.app')
@section('title', 'Form Pendaftaran Mahasiswa Baru')

@section('content')
    <div class="breadcome-area">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="breadcome-list">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <ul class="breadcome-heading">
                                    <li>
                                        <span class="nama" style="text-transform: uppercase">Pendaftaran Mahasiswa Baru
                                            <b>{{ $kampus->nama_kampus }}</b> </span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="analytics-sparkle-area">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-6 col-sm-6 col-xs-12">
                    <div class="sparkline8-list">
                        <div class="sparkline8-hd">
                            <div class="portlet-title">
                            </div>
                        </div>
                        <div class="sparkline12-graph">
                            <div class="basic-login-form-ad">
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="all-form-element-inner">
                                            <form action="{{ route('daftarpmbstore') }}" method="POST"
                                                enctype="multipart/form-data">
                                                @csrf
                                                <input type="hidden" name="user_id" value="{{ auth()->user()->id }}">
                                                <div class="form-group">
                                                    <label>Nama kampus</label>
                                                    <input type="hidden" name="kampus_id" value="{{ $kampus->id }}">
                                                    <input type="text"
                                                        class="form-control @error('kampus_id') is-invalid @enderror"
                                                        value="{{ $kampus->nama_kampus }}" readonly>
                                                </div>
                                                <div class="form-group">
                                                    <label>Nama Prodi</label>
                                                    <select
                                                        class="form-control basic @error('prodi_id') is-invalid @enderror"
                                                        name="prodi_id">
                                                        <option value="">Pilih Prodi</option>
                                                        @foreach ($kampus->prodi as $p)
                                                            <option value="{{ $p->id }}">{{ $p->nama_prodi }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                    @error('prodi_id')
                                                        <div class="invalid-feedback">Anda Belum Memilih Prodi ! </div>
                                                    @enderror
                                                </div>
                                                <div class="form-group">
                                                    <label>Nama Fakultas</label>
                                                    <select
                                                        class="form-control basic @error('fakultas_id') is-invalid @enderror"
                                                        name="fakultas_id">
                                                        <option value="">Pilih Fakultas</option>
                                                        @foreach ($kampus->fakultas as $f)
                                                            <option value="{{ $f->id }}">{{ $f->nama_fakultas }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                    @error('fakultas_id')
                                                        <div class="invalid-feedback">Anda Belum Memilih Fakultas ! </div>
                                                    @enderror
                                                </div>
                                                <div class="form-group">
                                                    <label>Nama Lengkap</label>
                                                    <input type="text" name="nama_camaba"
                                                        class="form-control @error('nama_camaba') is-invalid @enderror"
                                                        placeholder="Masukkan Nama Lengkap"
                                                        value="{{ old('nama_camaba', $user->name) }}">
                                                    @error('nama_camaba')
                                                        <div class="invalid-feedback">Nama Lengkap Masih Kosong ! </div>
                                                    @enderror
                                                </div>
                                                <div class="form-group">
                                                    <label>Tempat Lahir</label>
                                                    <input type="text" name="tempat_lahir"
                                                        class="form-control @error('tempat_lahir') is-invalid @enderror"
                                                        placeholder="Masukkan Tempat Lahir"
                                                        value="{{ old('tempat_lahir') }}">
                                                    @error('tempat_lahir')
                                                        <div class="invalid-feedback">Tempat Lahir Masih Kosong ! </div>
                                                    @enderror
                                                </div>
                                                <div class="form-group">
                                                    <label>Tanggal Lahir</label>
                                                    <input type="date" name="tanggal_lahir"
                                                        class="form-control @error('tanggal_lahir') is-invalid @enderror"
                                                        placeholder="" value="{{ old('tanggal_lahir') }}">
                                                    @error('tanggal_lahir')
                                                        <div class="invalid-feedback">Tanggal Lahir Masih Kosong ! </div>
                                                    @enderror
                                                </div>
                                                <div class="form-group">
                                                    <label>Jenis Kelamin</label>
                                                    <select name="jenis_kelamin" class="form-control custom-select-value">
                                                        <option value="">-- Pilih Jenis Kelamin --</option>
                                                        <option value="LAKI-LAKI">LAKI-LAKI</option>
                                                        <option value="PEREMPUAN">PEREMPUAN</option>
                                                    </select>
                                                    @error('jenis_kelamin')
                                                        <div class="invalid-feedback">Anda Belum Memilih Jenis Kelamin ! </div>
                                                    @enderror
                                                </div>
                                                <div class="form-group">
                                                    <label>AGAMA</label>
                                                    <select name="agama" class="form-control custom-select-value">
                                                        <option value="">-- Pilih Agama --</option>
                                                        <option value="ISLAM">ISLAM</option>
                                                        <option value="KRISTEN">KRISTEN</option>
                                                        <option value="KHATOLIK">KHATOLIK</option>
                                                        <option value="HINDU">HINDU</option>
                                                        <option value="BUDHA">BUDHA</option>
                                                        <option value="KONG HUU CHU">KONG HUU CHU</option>
                                                    </select>
                                                    @error('agama')
                                                        <div class="invalid-feedback">Anda Belum Memilih Agama ! </div>
                                                    @enderror
                                                </div>
                                                <div class="form-group">
                                                    <label>Email</label>
                                                    <input type="email" name="email"
                                                        class="form-control @error('email') is-invalid @enderror"
                                                        placeholder="Masukkan Email"
                                                        value="{{ old('email', $user->email) }}" readonly>
                                                    @error('email')
                                                        <div class="invalid-feedback">Email Masih Kosong ! </div>
                                                    @enderror
                                                </div>
                                                <div class="form-group">
                                                    <label>Nomor Handphone</label>
                                                    <input type="number" name="no_tlp"
                                                        class="form-control @error('no_tlp') is-invalid @enderror"
                                                        placeholder="Masukkan No Telpon" value="{{ old('no_tlp') }}">
                                                    @error('no_tlp')
                                                        <div class="invalid-feedback">Nomor Handphone Masih Kosong ! </div>
                                                    @enderror
                                                </div>
                                                <div class="form-group">
                                                    <label>Alamat</label>
                                                    <textarea type="text" name="alamat" class="form-control @error('alamat') is-invalid @enderror"
                                                        placeholder="Masukkan Alamat" value="{{ old('alamat') }}"> </textarea>
                                                    @error('alamat')
                                                        <div class="invalid-feedback">Alamat Masih Kosong ! </div>
                                                    @enderror
                                                </div>
                                                <div class="form-group">
                                                    <label>Provinsi</label>
                                                    <input type="hidden" name="provinsi_id" id="provinsi_id">
                                                    <select type="text"
                                                        class="form-control @error('provinsi_id') is-invalid @enderror"
                                                        id="provinsi">
                                                        <option> --- Pilih Provinsi --- </option>
                                                        @foreach ($province as $provinsi)
                                                            <option value="{{ $provinsi->id }}">
                                                                {{ $provinsi->name }}</option>
                                                        @endforeach
                                                    </select>
                                                    @error('provinsi_id')
                                                        <div class="invalid-feedback">Anda Belum Memilih Provinsi ! </div>
                                                    @enderror
                                                </div>
                                                <div class="form-group">
                                                    <label>Kabupaten</label>
                                                    <input type="hidden" name="kabupaten_id" id="kabupaten_id">
                                                    <select type="text"
                                                        class="form-control @error('kabupaten_id') is-invalid @enderror"
                                                        id="kabupaten">
                                                        <option> --- Pilih Kabupaten --- </option>
                                                    </select>
                                                    @error('kabupaten_id')
                                                        <div class="invalid-feedback">Anda Belum Memilih Kabupaten ! </div>
                                                    @enderror
                                                </div>
                                                <div class="form-group">
                                                    <label>Kecamatan</label>
                                                    <input type="hidden" name="kecamatan_id" id="kecamatan_id">
                                                    <select type="text"
                                                        class="form-control @error('kecamatan_id') is-invalid @enderror"
                                                        id="kecamatan">
                                                        <option> --- Pilih Kecamatan --- </option>
                                                    </select>
                                                    @error('kecamatan_id')
                                                        <div class="invalid-feedback">Anda Belum Memilih Kecamatan ! </div>
                                                    @enderror
                                                </div>
                                                <div class="form-group">
                                                    <label>Desa</label>
                                                    <input type="hidden" name="desa_id" id="desa_id">
                                                    <select type="text"
                                                        class="form-control @error('desa_id') is-invalid @enderror"
                                                        id="desa">
                                                        <option> --- Pilih Desa --- </option>
                                                    </select>
                                                    @error('desa_id')
                                                        <div class="invalid-feedback">Anda Belum Memilih Desa ! </div>
                                                    @enderror
                                                </div>
                                                <div class="form-group">
                                                    <label>Asal Sekolah</label>
                                                    <input type="text" name="asal_sekolah"
                                                        class="form-control @error('asal_sekolah') is-invalid @enderror"
                                                        placeholder="Masukkan Nama Asal Sekolah"
                                                        value="{{ old('asal_sekolah') }}">
                                                    @error('asal_sekolah')
                                                        <div class="invalid-feedback">Asal Sekolah Masih Kosong ! </div>
                                                    @enderror
                                                </div>
                                                <div class="form-group">
                                                    <label>Tahun Ajaran</label>
                                                    <input type="text" name="tahun_ajaran"
                                                        class="form-control @error('tahun_ajaran') is-invalid @enderror"
                                                        placeholder="Masukkan Nama Tahun Ajaran"
                                                        value="{{ date('Y') }}">
                                                    @error('tahun_ajaran')
                                                        <div class="invalid-feedback">Tahun Ajaran Masih Kosong ! </div>
                                                    @enderror
                                                </div>
                                                <div class="form-group">
                                                    <label>NIK</label>
                                                    <input type="text" name="nik"
                                                        class="form-control @error('nik') is-invalid @enderror"
                                                        placeholder="Masukkan NIk" value="{{ old('nik') }}">
                                                    @error('nik')
                                                        <div class="invalid-feedback">NIK Masih Kosong ! </div>
                                                    @enderror
                                                </div>
                                                <div class="form-group mb-4 mt-3">
                                                    <label for="exampleFormControlFile1">Foto <i><span
                                                                style="color: red">*</span><span style="color: red">JPEG,
                                                                JPG, PNG | Max: 1 Mb</span></i></label>
                                                    <input type="file" name="foto"
                                                        class="form-control-file @error('foto') is-invalid @enderror"
                                                        id="exampleFormControlFile1">
                                                    @error('foto')
                                                        <div class="invalid-feedback">Ukuran Foto tidak boleh lebih dari 1 Mb
                                                        </div>
                                                    @enderror
                                                </div>
                                                <div class="form-group-inner">
                                                    <label>Ijazah <i><span style="color: red">*</span><span
                                                                style="color: red">PDF | Max: 1 Mb</span></i></label>
                                                    <input type="file" name="file_ijazah" id=""
                                                        class="form-control-file @error('file_ijazah') is-invalid @enderror">
                                                    @error('file_ijazah')
                                                        <div class="invalid-feedback">Ukuran File tidak boleh lebih dari 1 Mb
                                                        </div>
                                                    @enderror
                                                </div>
                                                <div class="form-group-inner">
                                                    <label>Transkip Nilai <i><span style="color: red">*</span><span
                                                                style="color: red">PDF | Max: 1 Mb</span></i></label>
                                                    <input type="file" name="file_transkrip" id=""
                                                        class="form-control-file @error('file_transkrip') is-invalid @enderror">
                                                    @error('file_transkrip')
                                                        <div class="invalid-feedback">Ukuran File tidak boleh lebih dari 1 Mb
                                                        </div>
                                                    @enderror
                                                </div>
                                                <div class="form-group-inner">
                                                    <label>Raport <i><span style="color: red">*</span><span
                                                                style="color: red">PDF | Max: 1 Mb</span></i></label>
                                                    <input type="file" name="file_raport" id=""
                                                        class="form-control-file @error('file_raport') is-invalid @enderror">
                                                    @error('file_raport')
                                                        <div class="invalid-feedback">Ukuran File tidak boleh lebih dari 1 Mb
                                                        </div>
                                                    @enderror
                                                </div>
                                                <button type="submit" class="btn btn-success btn-md">Simpan</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"
        integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous">
    </script>

    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $(function() {
                // provinsi
                $('#provinsi').on('change', function() {
                    let id_provinsi = $('#provinsi').val();
                    console.log(id_provinsi);
                    $('#provinsi_id').val(id_provinsi);

                    $.ajax({
                        type: 'POST',
                        url: "{{ route('getkabupaten') }}",
                        data: {
                            id_provinsi: id_provinsi
                        },
                        cache: false,

                        success: function(msg) {
                            $('#kabupaten').html(msg);
                        },
                        error: function(data) {
                            console.log('error:', data);
                        },
                    })
                })

                // kabupaten
                $('#kabupaten').on('change', function() {
                    let id_kabupaten = $('#kabupaten').val();
                    console.log(id_kabupaten);
                    $('#kabupaten_id').val(id_kabupaten);

                    $.ajax({
                        type: 'POST',
                        url: "{{ route('getkecamatan') }}",
                        data: {
                            id_kabupaten: id_kabupaten
                        },
                        cache: false,

                        success: function(msg) {
                            $('#kecamatan').html(msg);
                        },
                        error: function(data) {
                            console.log('error:', data);
                        },
                    })
                })

                // Desa
                $('#kecamatan').on('change', function() {
                    let id_kecamatan = $('#kecamatan').val();
                    console.log(id_kecamatan);
                    $('#kecamatan_id').val(id_kecamatan);

                    $.ajax({
                        type: 'POST',
                        url: "{{ route('getdesa') }}",
                        data: {
                            id_kecamatan: id_kecamatan
                        },
                        cache: false,

                        success: function(msg) {
                            $('#desa').html(msg);
                        },
                        error: function(data) {
                            console.log('error:', data);
                        },
                    })
                })

                $('#desa').on('change', function() {
                    let id_desa = $('#desa').val();
                    console.log(id_desa);
                    $('#desa_id').val(id_desa);
                })
            })
        });
    </script>
@endpush
