<!-- Start Left menu area -->
<div class="left-sidebar-pro">
    <nav id="sidebar" class="">
        <div class="sidebar-header">
            <a href="{{ route('profilkampus', auth()->user()->id) }}"><img class="main-logo"
                    src="{{ asset('mahasiswa') }}/img/logo/logo.png" alt="" /></a>
            <strong><a href="{{ route('profilkampus', auth()->user()->id) }}"><img src="{{ asset('mahasiswa') }}/img/logo/logosn.png"
                        alt="" /></a></strong>
        </div>
        <div class="left-custom-menu-adp-wrap comment-scrollbar">
            <nav class="sidebar-nav left-sidebar-menu-pro">
                <ul class="metismenu" id="menu1">
                    <li>
                        <img class="foto" src="{{ asset('mahasiswa/img/contact/2.jpg') }}" alt="">
                        {{-- <img class="foto1" src="{{ asset($user->foto1) }}" alt=""> --}}
                    </li>
                    {{-- <li>
                        <span class="nama">Anton Susilo</span>
                    </li> --}}
                    <li>
                        <span class="nim">
                            {{-- nama camaba --}}
                            @if (auth()->user() != null)
                            <a href="{{ route('pmb') }}">
                                <img src="https://img.icons8.com/ios/50/000000/test-account.png" alt="">
                                {{ auth()->user()->name }}
                            </a>
                        @endif
                        </span>
                    </li>
                    {{-- <li>
                        <a href="{{ route('mahasiswa.index') }}" class="link-profile">
                            <button class="btn btn-custon-rounded-three btn-primary btn-sm">Lihat Profile</button>
                        </a>
                    </li> --}}
                    <hr class="line">
                    <li class="active">
                        <a href="{{ route('pmb') }}">
                            <span class="educate-icon educate-home icon-wrap"></span>
                            <span class="mini-click-non">Dashboard PMB</span>
                        </a>
                    </li>
                        {{-- <a href="{{ route('form.daftar') }}"> --}}
                            {{-- <a href="{{ route('daftarpmb') }}">
                            <span class="educate-icon educate-data-table icon-wrap"></span>
                            <span class="mini-click-non">Form Pendaftaran</span>
                        </a> --}}
                </ul>
            </nav>
        </div>
    </nav>
</div>
<!-- End Left menu area -->
