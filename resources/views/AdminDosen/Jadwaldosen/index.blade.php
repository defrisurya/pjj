@extends('layouts.cork')

@section('content')
    <div id="tableHover" class="col-lg-12 col-12 layout-spacing">
        <div class="statbox widget box box-shadow">
            <div class="widget-header">
                <div class="row">
                    <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                        <h4>Jadwal Kuliah Semester Ganjil 2021 / 2022</h4>
                    </div>
                </div>
            </div>
            <div class="widget-content widget-content-area">
                <div class="table-responsive">
                    <table class="table table-bordered table-hover mb-4">
                        <thead>
                            <tr>
                                <th class="text-center">No</th>
                                <th class="text-center">Hari, Tanggal</th>
                                <th class="text-center">Matakuliah</th>
                                <th class="text-center">Kampus</th>
                                <th class="text-center">Prodi</th>
                            </tr>
                        </thead>
                        <tbody>
                            {{-- {{ dd($data) }} --}}
                            {{-- @forelse ($data as $item => $kampus) --}}
                            <tr>
                                <td class="text-center">{{-- {{ $data->firstItem() + $item }} --}}1</td>
                                <td class="text-center">{{-- {{ $kampus->namakampus }} --}}Kamis, 10 Februari 2022</td>
                                <td class="text-center">{{-- {{ $kampus->akreditasi }} --}}Algoritma Pemrograman</td>
                                <td class="text-center">{{-- {{ $kampus->deskripsi }} --}}Universitas Teknologi Yogyakarta</td>
                                <td class="text-center">{{-- {{ $kampus->deskripsi }} --}}Sistem Informasi</td>
                            </tr>
                            {{-- @empty
                                @endforelse --}}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
