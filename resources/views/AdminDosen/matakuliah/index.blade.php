@extends('layouts.cork')

@section('content')
    <div id="tableHover" class="col-lg-12 col-12 layout-spacing">
        <div class="statbox widget box box-shadow">
            <div class="widget-header">
                {{-- <div class="row">
                    <div class="col-xl-6 col-md-6 col-sm-6 col-12">
                        <h4>DATA ABSENSI</h4>
                    </div>
                    <div class="col-xl-6 col-md-6 col-sm-6 col-12 text-right">
                        <a class="btn btn-outline-primary mt-3 mr-3" href="{{ route('absensi.create') }}">PROSES</a>
                    </div>
                </div> --}}

                <div class="row">
                    <div class="col-xl-2 col-md-6 col-sm-6 col-12">
                        <h4>ABSENSI SEMESTER GANJIL 2021/2022</h4>
                    </div>
                    <div class="col-xl-2 col-md-6 col-sm-6 col-12 text-right">
                        <div class="form-group mt-3">
                            <select class="form-control" id="">
                                <option value="">Filter Tahun Akademik</option>
                                <option value="1">2020</option>
                                <option value="2">2021</option>
                                <option value="3">2022</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-xl-2 col-md-6 col-sm-6 col-12 text-right">
                        <div class="form-group mt-3">
                            <select class="form-control" id="">
                                <option value="">Filter Semester</option>
                                <option value="1">Semester 1</option>
                                <option value="2">Semester 2</option>
                                <option value="3">Semester 3</option>
                                <option value="4">Semester 4</option>
                                <option value="5">Semester 5</option>
                                <option value="6">Semester 6</option>
                                <option value="7">Semester 7</option>
                                <option value="8">Semester 8</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-xl-2 col-md-6 col-sm-6 col-12 text-right">
                        <div class="form-group mt-3">
                            <select class="form-control" id="">
                                <option value="">Filter Kampus</option>
                                <option value="1">Kampus 1</option>
                                <option value="2">Kampus 2</option>
                                <option value="3">Kampus 3</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-xl-2 col-md-6 col-sm-6 col-12 text-right">
                        <div class="form-group mt-3">
                            <select class="form-control" id="">
                                <option value="">Filter Prodi</option>
                                <option value="1">Prodi 1</option>
                                <option value="2">Prodi 2</option>
                                <option value="3">Prodi 3</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-xl-2 col-md-6 col-sm-6 col-12 text-right">
                        <div class="form-group mt-2">
                            <a class="btn btn-outline-primary mt-3 mr-3" href="{{ route('absensi.create') }}">PROSES</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="widget-content widget-content-area">
                <div class="table-responsive">
                    <table class="table table-bordered table-hover mb-4">
                        <thead>
                            <tr>
                                <th class="text-center">No</th>
                                <th class="text-center">Hari</th>
                                <th class="text-center">Tanggal</th>
                                <th class="text-center">Matakuliah</th>
                                <th class="text-center">Mahasiswa</th>
                                <th class="text-center">Kampus</th>
                                <th class="text-center">Prodi</th>
                                <th class="text-center">Kehadiran</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>1</td>
                                <td>10/08/2020</td>
                                <td>320</td>
                                <td>10/08/2020</td>
                                <td>10/08</td>
                                <td>320</td>
                                <td class="text-center"><span class="text-success">Complete</span></td>
                                <td class="text-center"><span class="text-success">Hadir</span></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
