@extends('layouts.cork')

@section('content')
<div id="flStackForm" class="col-lg-12 layout-spacing layout-top-spacing">
    <div class="statbox widget box box-shadow col-md-12">
        <div class="widget-header">                                
            <div class="row">
                <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                    <h4>Tambah Data </h4>
                </div>                                                                        
            </div>
        </div>
        <div class="widget-content widget-content-area">
            <form action="{{ route('kampus.store') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <label>Pilih Kampus</label>
                    <select class="form-control  basic" name="kampus_id">
                        <option value=""> --- Pilih kampus --- </option>
                       
                    </select>
                </div>
                <div class="form-group">
                    <label>Pilih Tahun Akademik</label>
                    <select class="form-control  basic" name="kampus_id">
                        <option value=""> --- Pilih Tahun Akademik --- </option>
                       
                    </select>
                </div>
                <div class="form-group">
                    <label>Pilih Prodik</label>
                    <select class="form-control  basic" name="kampus_id">
                        <option value=""> --- Pilih Prodi --- </option>
                       
                    </select>
                </div>
                <div class="form-group">
                    <label>Pilih Semester</label>
                    <select class="form-control  basic" name="kampus_id">
                        <option value=""> --- Pilih Semester --- </option>
                       
                    </select>
                </div>
                <div class="form-group">
                    <label>Pilih Matakuliah</label>
                    <select class="form-control  basic" name="kampus_id">
                        <option value=""> --- Pilih Matakuliah --- </option>
                       
                    </select>
                </div>
                <div class="form-group">
                    <label>Nilai Angka</label>
                    <input type="number" name="namakampus" class="form-control" placeholder=""
                        value="{{ old('namakampus') }}">
                </div>
                <div class="form-group">
                    <label>Nilai Huruf</label>
                    <input type="number" name="namakampus" class="form-control" placeholder=""
                        value="{{ old('namakampus') }}">
                </div>
                <button type="submit" class="btn btn-primary mt-3">Submit</button>
            </form>

           
        </div>
    </div>
</div>
@endsection