@extends('layouts.cork')

@section('content')
    <div class="content">
        <div class="text-content">
            <div class="home text">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="left-custom-menu-adp-wrap comment-scrollbar">
                        <nav class="sidebar-nav left-sidebar-menu-pro">
                            <ul class="metismenu" id="menu1">
                                <div class="library-book-area">
                                    <div class="container-fluid">
                                        <div class="row">
                                            <div class="single-review-st-materi res-mg-t-30 table-mg-t-pro-n">
                                                <h1>Project Managemen Process</h1>
                                                <h4>Sarah Graves</h4>
                                                <div class="single-product-image-materi">
                                                    <img src="{{ asset('mahasiswa') }}/img/product/book-4.jpg" alt="">
                                                </div>
                                            </div>
                                            <div class="list">
                                                <div class="title">Tugas Kuliah</div>
                                                <p class="tugas">
                                                    Lorem ipsum dolor sit amet consectetur, adipisicing elit. Maxime quasi
                                                    nulla sunt officiis magni molestiae! Rerum commodi repudiandae corrupti,
                                                    ea minus placeat expedita animi, dolor enim fuga quisquam porro dolorem
                                                    ullam sequi culpa ab doloribus incidunt, saepe architecto. Hic earum
                                                    saepe debitis voluptatem reiciendis. Delectus animi accusamus, est
                                                    officiis harum quas vero nesciunt sunt error praesentium id recusandae
                                                    dolore unde tempora dignissimos commodi amet ratione. Enim debitis
                                                    deserunt expedita itaque culpa odio minima quaerat similique. Similique
                                                    ex repellat consequuntur ipsa ea sint quod reiciendis rem totam
                                                    temporibus id, labore quasi, et nobis obcaecati alias in officiis
                                                    molestiae laborum velit quos.
                                                </p>
                                                <h2>Cek Tugas dari Mahasiswa</h2>
                                                <div class="widget-content widget-content-area">
                                                    <div class="table-responsive">
                                                        <table class="table table-bordered table-hover mb-4">
                                                            <thead>
                                                                <tr>
                                                                    <th class="text-center">No</th>
                                                                    <th class="text-center">Nama Mahasiswa</th>
                                                                    <th class="text-center">Kampus</th>
                                                                    <th class="text-center">Prodi</th>
                                                                    <th class="text-center">Semester</th>
                                                                    <th class="text-center">File TUgas</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                    <td>1</td>
                                                                    <td>10/08/2020</td>
                                                                    <td>320</td>
                                                                    <td>320</td>
                                                                    <td class="text-center"><span
                                                                            class="text-success">Complete</span></td>
                                                                    <td class="text-center">
                                                                        <a class="btn btn-outline-primary mt-3 mr-3"
                                                                            href="">Download File</a>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
