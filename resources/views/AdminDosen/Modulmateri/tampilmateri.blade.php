@extends('layouts.cork')

@section('content')
    <div id="tableHover" class="col-lg-12 col-12 layout-spacing">
        <div class="statbox widget box box-shadow">
            <div class="widget-header">
                <div class="row">
                    <div class="col-xl-2 col-md-6 col-sm-6 col-12">
                        <h4>DETAIL MATERI</h4>
                    </div>
                    <div class="col-xl-10 col-md-6 col-sm-6 col-12 text-right">
                        <div class="form-group mt-2">
                            <a class="btn btn-outline-primary mt-3 mr-3" href="{{ route('modulmateri.create') }}">TAMBAH MATERI</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="widget-content widget-content-area">
                <div class="table-responsive">
                    <table class="table table-bordered table-hover mb-4">
                        <thead>
                            <tr>
                                <th class="text-center">No</th>
                                <th class="text-center">MATERI</th>
                                <th class="text-center">PERTEMUAN</th>
                                <th class="text-center"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>1</td>
                                <td>10/08/2020</td>
                                <td class="text-center"><span class="text-success">Complete</span></td>
                                <td class="text-center">
                                    <a class="btn btn-outline-primary mt-3 mr-3" href="{{ route('modulmateri.show', 1)}}">Detail Materi</a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
