@extends('layouts.cork')

@section('content')
    <div id="flStackForm" class="col-lg-12 layout-spacing layout-top-spacing">
        <div class="statbox widget box box-shadow col-md-12">
            <div class="widget-header">
                <div class="row">
                    <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                        <h4>Tambah Data Materi Kuliah</h4>
                    </div>
                </div>
            </div>
            <div class="widget-content widget-content-area">
                <form action="{{ route('kampus.store') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label>Tanggal Upload</label>
                        <input type="date" name="namakampus" class="form-control" placeholder=""
                            value="{{ old('namakampus') }}">
                    </div>
                    <div class="form-group">
                        <label>Upload Video Materi</label>
                        <input type="file" name="namakampus" class="form-control" placeholder=""
                            value="{{ old('namakampus') }}">
                    </div>
                    <div class="form-group mb-4">
                        <label for="exampleFormControlTextarea1">Deskripsi Tugas Kuliah (Opsional)</label>
                        <textarea name="tahapan" class="form-control" id="exampleFormControlTextarea1"
                            rows="3">{{ old('deskripsi') }}</textarea>
                    </div>
                    <button type="submit" class="btn btn-primary mt-3">Submit</button>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script>
        CKEDITOR.replace('tahapan');
    </script>
@endsection
