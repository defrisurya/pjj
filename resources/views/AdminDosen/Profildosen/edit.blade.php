@extends('layouts.cork')

@section('content')
<div id="flStackForm" class="col-lg-12 layout-spacing layout-top-spacing">
    <div class="statbox widget box box-shadow col-md-12">
        <div class="widget-header">                                
            <div class="row">
                <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                    <h4>UPDATE DATA DOSEN </h4>
                </div>                                                                        
            </div>
        </div>
        <div class="widget-content widget-content-area">
            <form action="" method="POST" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="form-group">
                    <label>Pilih Kampus</label>
                    <select class="form-control  basic" name="kampus_id">
                        <option value=""> --- Pilih kampus --- </option>
                       
                    </select>
                </div>
                <div class="form-group">
                    <label>Pilih Prodik</label>
                    <select class="form-control  basic" name="prodi_id">
                        <option value=""> --- Pilih Prodi --- </option>
                       
                    </select>
                </div>
                <div class="form-group">
                    <label>Pilih Fakultas</label>
                    <select class="form-control  basic" name="fakultas_id">
                        <option value=""> --- Pilih Fakultas --- </option>
                       
                    </select>
                </div>
                <div class="form-group">
                    <label>Nama Dosen</label>
                    <input type="text" name="nama_dosen" class="form-control" placeholder="Masukkan Nama "
                        value="{{ old('nama_dosen') }}">
                </div>
                <div class="form-group">
                    <label>NIP</label>
                    <input type="number" name="nip" class="form-control" placeholder="Masukkan Nip "
                        value="{{ old('nip') }}">
                </div>
                <div class="form-group">
                    <label>Jenis Kelamin Dosen</label>
                    <select class="form-control" name="jenis_kelamin" id="">
                        <option value="L"{{--  @if($mahasiswa->jenis_kelamin == 'L') selected @endif --}} >LAKI-LAKI</option>
                        <option value="P" {{-- @if($mahasiswa->jenis_kelamin == 'P') selected @endif --}} >PEREMPUAN</option>
                    </select>
                </div>
                <div class="form-group">
                    <label>Pendidikan Terakhir</label>
                    <select class="form-control" name="pendidikan_ayah" id="">
                        <option value="SMU" {{-- @if($mahasiswa->pendidikan_ayah == 'SMU') selected @endif --}} >SMU</option>
                        <option value="S1" {{-- @if($mahasiswa->pendidikan_ayah == 'S1') selected @endif --}} >S1</option>
                        <option value="S2" {{-- @if($mahasiswa->pendidikan_ayah == 'S2') selected @endif --}} >S2</option>
                        <option value="S3" {{-- @if($mahasiswa->pendidikan_ayah == 'S3') selected @endif --}} >S3</option>
                    </select>
                </div> 
                <div class="form-group">
                    <label>Email Dosen</label>
                    <input type="email" name="email" class="form-control" placeholder="Masukkan Email "
                        value="{{ old('email') }}">
                </div>
                <div class="form-group">
                    <label>No Telpon</label>
                    <input type="text" name="no_telpon" class="form-control" placeholder="Masukkan No Telpon"
                        value="{{ old('no_telpon') }}">
                </div>
                <div class="form-group mb-4 mt-3">
                    <label for="exampleFormControlFile1">Foto Dosen</label>
                    <input type="file" name="foto_dosen" class="form-control-file" id="exampleFormControlFile1">
                </div>
                <div class="form-group-inner">
                    <label>Alamat Dosen</label>
                    <textarea name="alamat" id="alamat"
                        class="form-control"> {{-- {{ old('alamat', $mahasiswa->alama) }} --}} </textarea>
                </div>
                <button type="submit" class="btn btn-primary mt-3">UPDATE DOSEN</button>
            </form>

           
        </div>
    </div>
</div>
@endsection