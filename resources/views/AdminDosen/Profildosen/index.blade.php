@extends('layouts.cork')

@section('content')
    <div id="tableHover" class="col-lg-12 col-12 layout-spacing">
        <div class="statbox widget box box-shadow">
            <div class="widget-header">
                <div class="row">
                    <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                        <h4>Profil Dosen</h4>
                        {{-- <a class="btn btn-outline-primary mt-3 mr-3" href="{{ route('absensi.create') }}">UPDATE DATA DOSEN</a> --}}
                        {{-- <a class="btn btn-outline-primary mt-3 mr-3" href="{{ route('profildosen.edit', 1) }}">UPDATE DATA DOSEN</a> --}}
               
                    </div>
                </div>
            </div>

            {{-- <div class="col-xl-2 col-md-6 col-sm-6 col-12 text-right">
                <div class="form-group mt-2">
                    <a class="btn btn-outline-primary mt-3 mr-3" href="{{ route('absensi.create') }}">PROSES</a>
                </div>
            </div> --}}
            
            <div class="widget-content widget-content-area">
                <img width="200px" src="{{ asset($dosen->foto_dosen) }}" alt="" style="margin-left: 40%">
                <h4 style="margin-top: 10px; margin-left: 40%;">{{ $value->name }}</h4>
                <p style="margin-left: 40%">NIP. {{ $dosen->nip }}</p>
                <br>
                <div class="row" style="margin-left: 20%">
                    <div class="col-xl-6 col-md-6 col-sm-6 col-12">
                        <p><strong>Pendidikan : </strong>{{ $dosen->pendidikan }}</p>
                    </div>
                    <div class="col-xl-6 col-md-6 col-sm-6 col-12">
                        @if ($dosen->jenis_kelamin == 'L')
                            <p><strong>Jenis Kelamin : </strong> Laki-Laki</p>
                        @elseif ($dosen->jenis_kelamin == 'P')
                            <p><strong>Jenis Kelamin : </strong> Perempuan</p>
                        @endif
                    </div>
                </div>
                <br>
                <div class="row" style="margin-left: 20%">
                    <div class="col-xl-6 col-md-6 col-sm-6 col-12">
                        <p><strong>No Telpon : </strong>{{ $dosen->no_telpon }}</p>
                    </div>
                    <div class="col-xl-6 col-md-6 col-sm-6 col-12">
                        <p><strong>E-mail : </strong>{{ $dosen->email }}</p>
                    </div>
                </div>
                <br>
                <div class="row" style="margin-left: 20%">
                    <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                        <p><strong>Alamat : </strong> {!! $dosen->alamat !!}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div id="tableHover" class="col-lg-12 col-12 layout-spacing">
        <div class="statbox widget box box-shadow">
            {{-- <div class="widget-header">
                <div class="row">
                    <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                        <h4>Profil Dosen</h4>
                    </div>
                </div>
            </div> --}}
            <div class="widget-content widget-content-area">
                {{-- <img width="200px" src="{{ asset('front') }}/images/testimonial-3.jpg" alt="" style="margin-left: 40%">
                <h4 style="margin-top: 10px; margin-left: 40%;">Anton Susilo</h4>
                <p style="margin-left: 40%">NIP. 112.45556.98</p>
                <br>
                <div class="row" style="margin-left: 20%">
                    <div class="col-xl-6 col-md-6 col-sm-6 col-12">
                        <p><strong>Pendidikan : </strong>Magister Teknik Informatika</p>
                    </div>
                    <div class="col-xl-6 col-md-6 col-sm-6 col-12">
                        <p><strong>Jenis Kelamin : </strong> Laki-Laki</p>
                    </div>
                </div>
                <br>
                <div class="row" style="margin-left: 20%">
                    <div class="col-xl-6 col-md-6 col-sm-6 col-12">
                        <p><strong>No Telpon : </strong>08779569807</p>
                    </div>
                    <div class="col-xl-6 col-md-6 col-sm-6 col-12">
                        <p><strong>E-mail : </strong>dosen@gmail.com</p>
                    </div>
                </div>
                <br>
                <div class="row" style="margin-left: 20%">
                    <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                        <p><strong>Alamat : </strong>Jl. Garuda</p>
                    </div>
                </div> --}}

                
            </div>
        </div>
    </div>


@endsection
