@extends('Mahasiswa.layouts.app')

@section('title', 'Profile Mahasiswa')

@section('content')
    <div class="breadcome-area">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="breadcome-list">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <ul class="breadcome-heading">
                                    <li>
                                        <span class="nama">Profile Mahasiswa Test Update</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="analytics-sparkle-area">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-6 col-sm-6 col-xs-12">
                    <div class="sparkline8-list">
                        <div class="sparkline8-hd">
                            <div class="portlet-title">
                            </div>
                        </div>
                        <div class="sparkline12-graph">
                            <div class="basic-login-form-ad">
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="all-form-element-inner">
                                            <form action="{{ route('mahasiswa.store') }}" method="POST" enctype="multipart/form-data">
                                                {{ csrf_field() }}

                                                {{-- <input type="hidden" name="user_id" value="{{ auth()->user()->id }}"> --}}
                                                <div class="form-group-inner">
                                                    <label>Nama Lengkap</label>
                                                    <input type="text" name="nama_lengkap" id="nama_lengkap" class="form-control @error('nama_lengkap') is-invalid @enderror" 
                                                    placeholder="Masukkan Nama Lengkap" value="{{ old('nama_lengkap') }}">
                                                    @error('nama_lengkap')
                                                        <div class="invalid-feedback">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                                <div class="form-group-inner">
                                                    <label>No Induk Mahasiswa (NIM)</label>
                                                    <input type="number" name="no_induk_mahasiswa" id="no_induk_mahasiswa" class="form-control @error('no_induk_mahasiswa') is-invalid @enderror" 
                                                    placeholder="Masukkan No Induk Mahasiswa" value="{{ old('no_induk_mahasiswa') }}">
                                                    @error('no_induk_mahasiswa')
                                                        <div class="invalid-feedback">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                                <div class="form-group-inner">
                                                    <label>No Induk Kependudukan (NIK)</label>
                                                    <input type="number" name="no_induk_kependudukan" id="no_induk_kependudukan" class="form-control @error('no_induk_kependudukan') is-invalid @enderror" 
                                                    placeholder="Masukkan NIK" value="{{ old('no_induk_kependudukan') }}">
                                                    @error('no_induk_kependudukan')
                                                        <div class="invalid-feedback">{{ $message }}</div>
                                                    @enderror
                                                </div>

                                                <div class="form-group-inner form-select-list">
                                                    <label>Jenis Kelamin</label>
                                                    <select name="jenis_kelamin" class="form-control custom-select-value">
                                                        <option value="">-- Pilih Jenis Kelamin --</option>
                                                        <option value="Laki-Laki">Laki-Laki</option>
                                                        <option value="Perempuan">Perempuan</option>
                                                    </select>
                                                </div>
                                                <div class="form-group-inner form-select-list">
                                                    <label>Agama</label>
                                                    <select name="agama" class="form-control custom-select-value">
                                                        <option value="">-- Pilih Agama --</option>
                                                        <option value="ISLAM">ISLAM</option>
                                                        <option value="KRISTEN">KRISTEN</option>
                                                        <option value="KHATOLIK">KHATOLIK</option>
                                                        <option value="HINDU">HINDU</option>
                                                        <option value="BUDHA">BUDHA</option>
                                                        <option value="KONG HUU CHUU">KONG HUU CHUU</option>
                                                    </select>
                                                </div>
                                                <div class="form-group-inner">
                                                    <label>Program Studi Kampus</label>
                                                    <input type="text" name="program_studi" id="program_studi" class="form-control @error('program_studi') is-invalid @enderror" 
                                                    placeholder="Masukkan Program Studi Kampus" value="{{ old('program_studi') }}">
                                                    @error('program_studi')
                                                        <div class="invalid-feedback">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                                <div class="form-group-inner form-select-list">
                                                    <label>Kewarganegaraan</label>
                                                    <select name="kewarganegaraan" class="form-control custom-select-value">
                                                        <option value="">-- Pilih Kewarganegaraan --</option>
                                                        <option value="WNI">WNI</option>
                                                        <option value="WNA">WNA</option>
                                                    </select>
                                                </div>
                                                <div class="form-group-inner">
                                                    <label>Tempat Lahir</label>
                                                    <input type="text" name="tempat_lahir" id="tempat_lahir" class="form-control @error('tempat_lahir') is-invalid @enderror" 
                                                    placeholder="Masukkan Tempat Lahir" value="{{ old('tempat_lahir') }}">
                                                    @error('tempat_lahir')
                                                        <div class="invalid-feedback">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                                <div class="form-group-inner">
                                                    <label>Tanggal Lahir</label>
                                                    <input type="date" name="tanggal_lahir" id="tanggal_lahir" class="form-control @error('tanggal_lahir') is-invalid @enderror" 
                                                     value="">
                                                    @error('tanggal_lahir')
                                                        <div class="invalid-feedback">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                                <div class="form-group-inner">
                                                    <label>Email</label>
                                                    <input type="text" name="email" id="email" class="form-control @error('email') is-invalid @enderror" 
                                                    placeholder="Masukkan Email" value="{{ old('email') }}">
                                                    @error('email')
                                                        <div class="invalid-feedback">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                                <div class="form-group-inner">
                                                    <label>No Handphone</label>
                                                    <input type="text" name="no_telpon" id="no_telpon" class="form-control @error('no_telpon') is-invalid @enderror" 
                                                    placeholder="Masukkan No Telpon" value="{{ old('no_telpon') }}">
                                                    @error('no_telpon')
                                                        <div class="invalid-feedback">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                                <div class="form-group-inner">
                                                    <label>Asal Sekolah</label>
                                                    <input type="text" name="asal_sekolah" id="asal_sekolah" class="form-control @error('asal_sekolah') is-invalid @enderror" 
                                                    placeholder="Masukkan asal_sekolah" value="{{ old('asal_sekolah') }}">
                                                    @error('asal_sekolah')
                                                        <div class="invalid-feedback">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                                <div class="form-group-inner">
                                                    <label>Penerima KPS (YA / TIDAK)</label>
                                                    <div class="bt-df-checkbox">
                                                        <label>
                                                            <input class="pull-left radio-checked" type="radio" checked="" value="Ya" id="optionsRadios1" name="penerima_kps"> YA
                                                        </label>
                                                    </div>
                                                    <div class="bt-df-checkbox">
                                                        <label>
                                                            <input class="pull-left" type="radio" value="Tidak" id="optionsRadios2" name="penerima_kps"> TIDAK</label>
                                                    </div>
                                                </div>
                                                <div class="form-group-inner">
                                                    <label>Alamat</label>
                                                    <textarea name="alamat" id="alamat" class="form-control"></textarea>
                                                </div>
                                                <div class="form-group-inner">
                                                    <label>Kode Pos</label>
                                                    <input type="number" name="kode_pos" id="kode_pos" class="form-control @error('kode_pos') is-invalid @enderror" 
                                                    placeholder="Masukkan Kode Pos" value="{{ old('kode_pos') }}">
                                                    @error('kode_pos')
                                                        <div class="invalid-feedback">{{ $message }}</div>
                                                    @enderror
                                                </div>

                                                <div class="breadcome-list">
                                                    <div class="row">
                                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                            <ul class="breadcome-heading">
                                                                <li>
                                                                    <span class="nama">
                                                                        Biodata Ayah Kandung
                                                                    </span>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group-inner">
                                                    <label>No Induk Kependudukan Ayah (NIK)</label>
                                                    <input type="number" name="no_induk_kependudukan_ayah" id="no_induk_kependudukan_ayah" class="form-control @error('no_induk_kependudukan_ayah') is-invalid @enderror" 
                                                    placeholder="Masukkan NIK Ayah" value="{{ old('no_induk_kependudukan_ayah') }}">
                                                    @error('no_induk_kependudukan_ayah')
                                                        <div class="invalid-feedback">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                                <div class="form-group-inner">
                                                    <label>Nama Lengkap Ayah</label>
                                                    <input type="text" name="nama_lengkap_ayah" id="nama_lengkap_ayah" class="form-control @error('nama_lengkap_ayah') is-invalid @enderror" 
                                                    placeholder="Masukkan Nama Lengkap Ayah" value="{{ old('nama_lengkap_ayah') }}">
                                                    @error('nama_lengkap_ayah')
                                                        <div class="invalid-feedback">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                                <div class="form-group-inner">
                                                    <label>Tanggal Lahir Ayah</label>
                                                    <input type="date" name="tanggal_lahir_ayah" id="tgl_lahir_ayah"
                                                        class="form-control" />
                                                </div>
                                                <div class="form-group-inner form-select-list">
                                                    <label>Pendidikan Terakhir Ayah</label>
                                                    <select name="pendidikan_ayah" class="form-control custom-select-value">
                                                        <option value="">-- Pilih Pendidikan Terakhir --</option>
                                                        <option value="SD">SD</option>
                                                        <option value="SMP">SMP</option>
                                                        <option value="SMA">SMA</option>
                                                        <option value="Diploma">Diploma</option>
                                                        <option value="S1">S1</option>
                                                        <option value="S2">S2</option>
                                                        <option value="S2">S3</option>
                                                    </select>
                                                </div>
                                                <div class="form-group-inner">
                                                    <label>Pekerjaan Ayah</label>
                                                    <input type="text" name="pekerjaan_ayah" id="pekerjaan_ayah" class="form-control @error('pekerjaan_ayah') is-invalid @enderror" 
                                                    placeholder="Masukkan Pekerjaan Ayah" value="{{ old('pekerjaan_ayah') }}">
                                                    @error('pekerjaan_ayah')
                                                        <div class="invalid-feedback">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                                <div class="form-group-inner">
                                                    <label>Rentan Penghasilan Ayah</label>
                                                    <input type="text" name="rentang_penghasilan_ayah" id="rentang_penghasilan_ayah" class="form-control @error('rentang_penghasilan_ayah') is-invalid @enderror" 
                                                    placeholder="Masukkan Penghasilan Ayah" value="{{ old('rentang_penghasilan_ayah') }}">
                                                    @error('rentang_penghasilan_ayah')
                                                        <div class="invalid-feedback">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                                <div class="form-group-inner">
                                                    <label>Nomor HanPhone Ayah</label>
                                                    <input type="number" name="no_telpon_ayah" id="no_telpon_ayah" class="form-control @error('no_telpon_ayah') is-invalid @enderror" 
                                                    placeholder="Masukkan No Telpon Ayah" value="{{ old('no_telpon_ayah') }}">
                                                    @error('no_telpon_ayah')
                                                        <div class="invalid-feedback">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                                <div class="form-group-inner">
                                                    <label>Alamat Ayah</label>
                                                    <textarea name="alamat_ayah" id="alamat_ayah"
                                                        class="form-control"></textarea>
                                                </div>

                                                <div class="breadcome-list">
                                                    <div class="row">
                                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                            <ul class="breadcome-heading">
                                                                <li>
                                                                    <span class="nama">
                                                                        Biodata Ibu Kandung
                                                                    </span>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group-inner">
                                                    <label>No Induk Kependudukan Ibu (NIK)</label>
                                                    <input type="number" name="no_induk_kependudukan_ibu" id="no_induk_kependudukan_ibu" class="form-control @error('no_induk_kependudukan_ibu') is-invalid @enderror" 
                                                    placeholder="Masukkan Nama" value="{{ old('no_induk_kependudukan_ibu') }}">
                                                    @error('no_induk_kependudukan_ibu')
                                                        <div class="invalid-feedback">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                                <div class="form-group-inner">
                                                    <label>Nama Lengkap Ibu</label>
                                                    <input type="text" name="nama_lengkap_ibu" id="nama_lengkap_ibu" class="form-control @error('nama_lengkap_ibu') is-invalid @enderror" 
                                                    placeholder="Masukkan Nama" value="{{ old('nama_lengkap_ibu') }}">
                                                    @error('nama_lengkap_ibu')
                                                        <div class="invalid-feedback">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                                <div class="form-group-inner">
                                                    <label>Tanggal Lahir Ibu</label>
                                                    <input type="date" name="tanggal_lahir_ibu" id="tanggal_lahir_ibu"
                                                        class="form-control" />
                                                </div>
                                                <div class="form-group-inner form-select-list">
                                                    <label>Pendidikan Terakhir Ibu</label>
                                                    <select name="pendidikan_ibu" class="form-control custom-select-value">
                                                        <option value="">-- Pilih Pendidikan Terakhir --</option>
                                                        <option value="SD">SD</option>
                                                        <option value="SMP">SMP</option>
                                                        <option value="SMA">SMA</option>
                                                        <option value="Diploma">Diploma</option>
                                                        <option value="S1">S1</option>
                                                        <option value="S2">S2</option>
                                                        <option value="S2">S3</option>
                                                    </select>
                                                </div>
                                                <div class="form-group-inner">
                                                    <label>Pekerjaan Ibu</label>
                                                    <input type="text" name="pekerjaan_ibu" id="pekerjaan_ibu" class="form-control @error('pekerjaan_ibu') is-invalid @enderror" 
                                                    placeholder="Masukkan Nama" value="{{ old('pekerjaan_ibu') }}">
                                                    @error('pekerjaan_ibu')
                                                        <div class="invalid-feedback">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                                <div class="form-group-inner">
                                                    <label>Rentan Penghasilan Ibu</label>
                                                    <input type="text" name="rentang_penghasilan_ibu" id="rentang_penghasilan_ibu" class="form-control @error('rentang_penghasilan_ibu') is-invalid @enderror" 
                                                    placeholder="Masukkan Nama" value="{{ old('rentang_penghasilan_ibu') }}">
                                                    @error('rentang_penghasilan_ibu')
                                                        <div class="invalid-feedback">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                                <div class="form-group-inner">
                                                    <label>Nomor HanPhone Ibu</label>
                                                    <input type="number" name="no_telpon_ibu" id="no_telpon_ibu" class="form-control @error('no_telpon_ibu') is-invalid @enderror" 
                                                    placeholder="Masukkan Nama" value="{{ old('no_telpon_ibu') }}">
                                                    @error('no_telpon_ibu')
                                                        <div class="invalid-feedback">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                                <div class="form-group-inner">
                                                    <label>Alamat Ibu</label>
                                                    <textarea name="alamat_ibu" id="alamat_ibu"
                                                        class="form-control"></textarea>
                                                </div>
                                                <div class="breadcome-list link-submit">
                                                    <div class="row">
                                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                            <button type="submit" class="btn btn-success btn-md">Simpan</button>
                                                            <a href="{{ route('mhs') }}" class="link-profile">
                                                                <button class="btn btn-primary btn-md">Kembali</button>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@push('script')
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"
        integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous">
    </script>

    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $(function() {
                // provinsi
                $('#provinsi').on('change', function() {
                    let id_provinsi = $('#provinsi').val();
                    console.log(id_provinsi);
                    $('#provinsi_id').val(id_provinsi);

                    $.ajax({
                        type: 'POST',
                        url: "{{ route('getkabupaten') }}",
                        data: {
                            id_provinsi: id_provinsi
                        },
                        cache: false,

                        success: function(msg) {
                            $('#kabupaten').html(msg);
                        },
                        error: function(data) {
                            console.log('error:', data);
                        },
                    })
                })

                // kabupaten
                $('#kabupaten').on('change', function() {
                    let id_kabupaten = $('#kabupaten').val();
                    console.log(id_kabupaten);
                    $('#kabupaten_id').val(id_kabupaten);

                    $.ajax({
                        type: 'POST',
                        url: "{{ route('getkecamatan') }}",
                        data: {
                            id_kabupaten: id_kabupaten
                        },
                        cache: false,

                        success: function(msg) {
                            $('#kecamatan').html(msg);
                        },
                        error: function(data) {
                            console.log('error:', data);
                        },
                    })
                })

                // Desa
                $('#kecamatan').on('change', function() {
                    let id_kecamatan = $('#kecamatan').val();
                    console.log(id_kecamatan);
                    $('#kecamatan_id').val(id_kecamatan);

                    $.ajax({
                        type: 'POST',
                        url: "{{ route('getdesa') }}",
                        data: {
                            id_kecamatan: id_kecamatan
                        },
                        cache: false,

                        success: function(msg) {
                            $('#desa').html(msg);
                        },
                        error: function(data) {
                            console.log('error:', data);
                        },
                    })
                })

                $('#desa').on('change', function() {
                    let id_desa = $('#desa').val();
                    console.log(id_desa);
                    $('#desa_id').val(id_desa);
                })
            })
        });
    </script>
@endpush



{{-- <div class="form-group-inner">
                                                    <label>Nama Lengkap Mahasiswa</label>
                                                    <input type="text" name="nama_lengkap" id="nama" class="form-control" />
                                                </div> --}}


{{-- <div class="form-group-inner">
                                                    <label>Penerima KPS (YA/TIDAK)</label>
                                                    <input type="text" name="penerima_kps" id="penerima_kps" class="form-control" />
                                                </div> --}}
                                                
                                                {{-- <div class="form-group-inner form-select-list">
                                                    <label>Penerima KPS (YA/TIDAK)</label>
                                                    <select name="kewarganegaraan" class="form-control custom-select-value">
                                                        <option value="YA">YA</option>
                                                        <option value="TIDAK">TIDAK</option>
                                                    </select>
                                                </div> --}}

                                                      {{-- <div class="form-group-inner form-select-list">
                                                    <label>Program Studi</label>
                                                    <select name="prodi_id" class="form-control custom-select-value">
                                                        <option value="">-- Pilih Program Studi --</option>
                                                        <option value="Sistem Informasi">Sistem Informasi</option>
                                                        <option value="Teknik Informatika">Teknik Informatika</option>
                                                        <option value="Manajemen Informatika">Manajemen Informatika</option>
                                                    </select>
                                                </div> --}}




                                                {{-- <div class="form-group-inner">
                                                    <div class="col-md-3 mb-3">
                                                        <label>No Induk Mhs</label>
                                                        <input type="number" name="no_induk_mahasiswa" class="form-control @error('no_induk_mahasiswa') is-invalid @enderror" 
                                                        placeholder="Masukkan No Induk" value="{{ old('no_induk_mahasiswa') }}">
                                                        @error('no_induk_mahasiswa')
                                                            <div class="invalid-feedback">{{ $message }} </div>
                                                        @enderror
                                                    </div>
                                                </div>
                                                <div class="form-group-inner">
                                                    <div class="col-md-3 mb-3">
                                                        <label>No Induk Kependudukan</label>
                                                        <input type="number" name="no_induk_kependudukan" class="form-control @error('no_induk_kependudukan') is-invalid @enderror" 
                                                        placeholder="Masukkan No Induk" value="{{ old('no_induk_kependudukan') }}">
                                                        @error('no_induk_kependudukan')
                                                            <div class="invalid-feedback">{{ $message }} </div>
                                                        @enderror
                                                    </div>
                                                </div> --}}



                                                {{-- <div class="breadcome-list link-submit">
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <a href="{{ route('mhs') }}" class="link-profile">
                                        <button class="btn btn-primary btn-md">Kembali</button>
                                    </a>
                                    <button type="submit" class="btn btn-success btn-md">Simpan</button>
                                </div>
                            </div>
                        </div> --}}