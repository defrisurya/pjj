@extends('Mahasiswa.layouts.app')

@section('title', 'E-Learning')

@section('content')
    <div class="analytics-sparkle-area">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-6 col-sm-6 col-xs-12">
                    <div class="sparkline8-list">
                        <div class="sparkline8-hd">
                            <div class="portlet-title">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <div class="caption pro-sl-hd">
                                            <span class="caption-subject"><b>Jadwal Kuliah</b></span>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <div class="actions graph-rp graph-rp-dl">
                                            <p style="color: black; font-size: 13px">
                                                <i>Cek materi sesuai dosen pengampu</i>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="sparkline8-graph">
                            <div class="static-table-list">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Hari</th>
                                            <th>Matakuliah</th>
                                            <th>Dosen Pengampu</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>1</td>
                                            <td>Mamun</td>
                                            <td>Roshid</td>
                                            <td>@Facebook</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="breadcome-area-keterangan">
        <div class="container-fluid">
            <p>Silahkan lihat materi sesuai dosen pengampu</p>
        </div>
    </div>
    <div class="library-book-area">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                    <div class="single-review-st-item res-mg-t-30 table-mg-t-pro-n">
                        <div class="single-review-st-text">
                            <img src="img/notification/1.jpg" alt="">
                            <div class="review-ctn-hf">
                                <h3>Sarah Graves</h3>
                                <p>Basis Data</p>
                                <a class="link-materi" href="{{ route('materi') }}">
                                    <p>Lihat Materi <i class="fa fa-chevron-circle-right"></i></p>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
