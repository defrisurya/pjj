@extends('Mahasiswa.layouts.app')

@section('title', 'Materi Perkuliahan')

@section('content')
    <div class="content">
        <input type="radio" name="slider" checked id="home">
        <input type="radio" name="slider" id="blog">
        <input type="radio" name="slider" id="help">
        <input type="radio" name="slider" id="code">
        <input type="radio" name="slider" id="about">
        <div class="text-content">
            <div class="home text">
                <div class="col-lg-12 col-md-4 col-sm-4 col-xs-12">
                    <div class="left-custom-menu-adp-wrap comment-scrollbar">
                        <nav class="sidebar-nav left-sidebar-menu-pro">
                            <ul class="metismenu" id="menu1">
                                <div class="library-book-area">
                                    <div class="container-fluid">
                                        <div class="row">
                                            <div class="single-review-st-materi res-mg-t-30 table-mg-t-pro-n">
                                                <h1>Materi 1</h1>
                                                <h4>Sarah Graves</h4>
                                                <div class="single-product-image-materi">
                                                    <img src="img/product/book-4.jpg" alt="">
                                                </div>
                                            </div>
                                            <div class="list">
                                                <div class="title">Tugas</div>
                                                <p class="tugas">
                                                    Lorem ipsum dolor sit amet consectetur, adipisicing elit. Maxime quasi
                                                    nulla sunt officiis magni molestiae! Rerum commodi repudiandae corrupti,
                                                    ea minus placeat expedita animi, dolor enim fuga quisquam porro dolorem
                                                    ullam sequi culpa ab doloribus incidunt, saepe architecto. Hic earum
                                                    saepe debitis voluptatem reiciendis. Delectus animi accusamus, est
                                                    officiis harum quas vero nesciunt sunt error praesentium id recusandae
                                                    dolore unde tempora dignissimos commodi amet ratione. Enim debitis
                                                    deserunt expedita itaque culpa odio minima quaerat similique. Similique
                                                    ex repellat consequuntur ipsa ea sint quod reiciendis rem totam
                                                    temporibus id, labore quasi, et nobis obcaecati alias in officiis
                                                    molestiae laborum velit quos.
                                                </p>
                                                <div class="title">Upload Tugas</div>
                                                <div class="dropzone-pro">
                                                    <div id="dropzone" class="multi-uploader-cs">
                                                        <form action="/upload" class="dropzone dropzone-custom needsclick"
                                                            id="demo-upload">
                                                            <div class="dz-message needsclick download-custom">
                                                                <i class="fa fa-cloud-download" aria-hidden="true"></i>
                                                                <h2>Drop files here or click to upload.</h2>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
            <div class="blog text">
                <div class="col-lg-12 col-md-4 col-sm-4 col-xs-12">
                    <div class="left-custom-menu-adp-wrap comment-scrollbar">
                        <nav class="sidebar-nav left-sidebar-menu-pro">
                            <ul class="metismenu" id="menu1">
                                <div class="library-book-area">
                                    <div class="container-fluid">
                                        <div class="row">
                                            <div class="single-review-st-item res-mg-t-30 table-mg-t-pro-n">
                                                <h1>Materi 2</h1>
                                                <h4>Sarah Graves</h4>
                                                <div class="single-product-image-materi">
                                                    <img src="img/product/profile-bg.jpg" alt="">
                                                </div>
                                            </div>
                                            <div class="list">
                                                <div class="title">Tugas</div>
                                                <p class="tugas">
                                                    Lorem ipsum dolor sit amet consectetur, adipisicing elit. Maxime quasi
                                                    nulla sunt officiis magni molestiae! Rerum commodi repudiandae corrupti,
                                                    ea minus placeat expedita animi, dolor enim fuga quisquam porro dolorem
                                                    ullam sequi culpa ab doloribus incidunt, saepe architecto. Hic earum
                                                    saepe debitis voluptatem reiciendis. Delectus animi accusamus, est
                                                    officiis harum quas vero nesciunt sunt error praesentium id recusandae
                                                    dolore unde tempora dignissimos commodi amet ratione. Enim debitis
                                                    deserunt expedita itaque culpa odio minima quaerat similique. Similique
                                                    ex repellat consequuntur ipsa ea sint quod reiciendis rem totam
                                                    temporibus id, labore quasi, et nobis obcaecati alias in officiis
                                                    molestiae laborum velit quos.
                                                </p>
                                                <div class="title">Upload Tugas</div>
                                                <div class="dropzone-pro">
                                                    <div id="dropzone" class="multi-uploader-cs">
                                                        <form action="/upload" class="dropzone dropzone-custom needsclick"
                                                            id="demo-upload">
                                                            <div class="dz-message needsclick download-custom">
                                                                <i class="fa fa-cloud-download" aria-hidden="true"></i>
                                                                <h2>Drop files here or click to upload.</h2>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
            <div class="help text">
                <div class="col-lg-12 col-md-4 col-sm-4 col-xs-12">
                    <div class="left-custom-menu-adp-wrap comment-scrollbar">
                        <nav class="sidebar-nav left-sidebar-menu-pro">
                            <ul class="metismenu" id="menu1">
                                <div class="library-book-area">
                                    <div class="container-fluid">
                                        <div class="row">
                                            <div class="single-review-st-item res-mg-t-30 table-mg-t-pro-n">
                                                <h1>Materi 3</h1>
                                                <h4>Sarah Graves</h4>
                                                <div class="single-product-image-materi">
                                                    <img src="img/courses/1.jpg" alt="">
                                                </div>
                                            </div>
                                            <div class="list">
                                                <div class="title">Tugas</div>
                                                <p class="tugas">
                                                    Lorem ipsum dolor sit amet consectetur, adipisicing elit. Maxime quasi
                                                    nulla sunt officiis magni molestiae! Rerum commodi repudiandae corrupti,
                                                    ea minus placeat expedita animi, dolor enim fuga quisquam porro dolorem
                                                    ullam sequi culpa ab doloribus incidunt, saepe architecto. Hic earum
                                                    saepe debitis voluptatem reiciendis. Delectus animi accusamus, est
                                                    officiis harum quas vero nesciunt sunt error praesentium id recusandae
                                                    dolore unde tempora dignissimos commodi amet ratione. Enim debitis
                                                    deserunt expedita itaque culpa odio minima quaerat similique. Similique
                                                    ex repellat consequuntur ipsa ea sint quod reiciendis rem totam
                                                    temporibus id, labore quasi, et nobis obcaecati alias in officiis
                                                    molestiae laborum velit quos.
                                                </p>
                                                <div class="title">Upload Tugas</div>
                                                <div class="dropzone-pro">
                                                    <div id="dropzone" class="multi-uploader-cs">
                                                        <form action="/upload" class="dropzone dropzone-custom needsclick"
                                                            id="demo-upload">
                                                            <div class="dz-message needsclick download-custom">
                                                                <i class="fa fa-cloud-download" aria-hidden="true"></i>
                                                                <h2>Drop files here or click to upload.</h2>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
            <div class="code text">
                <div class="col-lg-12 col-md-4 col-sm-4 col-xs-12">
                    <div class="left-custom-menu-adp-wrap comment-scrollbar">
                        <nav class="sidebar-nav left-sidebar-menu-pro">
                            <ul class="metismenu" id="menu1">
                                <div class="library-book-area">
                                    <div class="container-fluid">
                                        <div class="row">
                                            <div class="single-review-st-item res-mg-t-30 table-mg-t-pro-n">
                                                <h1>Materi 4</h1>
                                                <h4>Sarah Graves</h4>
                                                <div class="single-product-image-materi">
                                                    <img src="img/product/book-4.jpg" alt="">
                                                </div>
                                            </div>
                                            <div class="list">
                                                <div class="title">Tugas</div>
                                                <p class="tugas">
                                                    Lorem ipsum dolor sit amet consectetur, adipisicing elit. Maxime quasi
                                                    nulla sunt officiis magni molestiae! Rerum commodi repudiandae corrupti,
                                                    ea minus placeat expedita animi, dolor enim fuga quisquam porro dolorem
                                                    ullam sequi culpa ab doloribus incidunt, saepe architecto. Hic earum
                                                    saepe debitis voluptatem reiciendis. Delectus animi accusamus, est
                                                    officiis harum quas vero nesciunt sunt error praesentium id recusandae
                                                    dolore unde tempora dignissimos commodi amet ratione. Enim debitis
                                                    deserunt expedita itaque culpa odio minima quaerat similique. Similique
                                                    ex repellat consequuntur ipsa ea sint quod reiciendis rem totam
                                                    temporibus id, labore quasi, et nobis obcaecati alias in officiis
                                                    molestiae laborum velit quos.
                                                </p>
                                                <div class="title">Upload Tugas</div>
                                                <div class="dropzone-pro">
                                                    <div id="dropzone" class="multi-uploader-cs">
                                                        <form action="/upload" class="dropzone dropzone-custom needsclick"
                                                            id="demo-upload">
                                                            <div class="dz-message needsclick download-custom">
                                                                <i class="fa fa-cloud-download" aria-hidden="true"></i>
                                                                <h2>Drop files here or click to upload.</h2>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
            <div class="about text">
                <div class="col-lg-12 col-md-4 col-sm-4 col-xs-12">
                    <div class="left-custom-menu-adp-wrap comment-scrollbar">
                        <nav class="sidebar-nav left-sidebar-menu-pro">
                            <ul class="metismenu" id="menu1">
                                <div class="library-book-area">
                                    <div class="container-fluid">
                                        <div class="row">
                                            <div class="single-review-st-item res-mg-t-30 table-mg-t-pro-n">
                                                <h1>Materi 5</h1>
                                                <h4>Sarah Graves</h4>
                                                <div class="single-product-image-materi">
                                                    <img src="img/product/book-4.jpg" alt="">
                                                </div>
                                            </div>
                                            <div class="list">
                                                <div class="title">Tugas</div>
                                                <p class="tugas">
                                                    Lorem ipsum dolor sit amet consectetur, adipisicing elit. Maxime quasi
                                                    nulla sunt officiis magni molestiae! Rerum commodi repudiandae corrupti,
                                                    ea minus placeat expedita animi, dolor enim fuga quisquam porro dolorem
                                                    ullam sequi culpa ab doloribus incidunt, saepe architecto. Hic earum
                                                    saepe debitis voluptatem reiciendis. Delectus animi accusamus, est
                                                    officiis harum quas vero nesciunt sunt error praesentium id recusandae
                                                    dolore unde tempora dignissimos commodi amet ratione. Enim debitis
                                                    deserunt expedita itaque culpa odio minima quaerat similique. Similique
                                                    ex repellat consequuntur ipsa ea sint quod reiciendis rem totam
                                                    temporibus id, labore quasi, et nobis obcaecati alias in officiis
                                                    molestiae laborum velit quos.
                                                </p>
                                                <div class="title">Upload Tugas</div>
                                                <div class="dropzone-pro">
                                                    <div id="dropzone" class="multi-uploader-cs">
                                                        <form action="/upload" class="dropzone dropzone-custom needsclick"
                                                            id="demo-upload">
                                                            <div class="dz-message needsclick download-custom">
                                                                <i class="fa fa-cloud-download" aria-hidden="true"></i>
                                                                <h2>Drop files here or click to upload.</h2>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
            <div class="left-custom-menu-adp-wrap comment-scrollbar">
                <nav class="sidebar-nav left-sidebar-menu-pro">
                    <ul class="metismenu" id="menu1">
                        <div class="library-book-area">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="single-review-st-materi res-mg-t-30 table-mg-t-pro-n">
                                        <div class="single-review-st-text">
                                            <img src="img/notification/1.jpg" alt="">
                                            <div class="review-ctn-hf">
                                                <h3>Sarah Graves</h3>
                                                <p>Basis Data</p>
                                            </div>
                                        </div>
                                    </div>
                                    <p class="desk">Silabus Materi Perkuliahan Basis Data
                                        Semester
                                        Ganjil 2021-2022</p>
                                    <hr class="line">
                                    <div class="list">
                                        <p class="desk">Materi Kuliah</p>
                                        <li>
                                            <label for="home" class="home">
                                                <span class="educate-icon educate-data-table icon-wrap"></span>
                                                <span class="title">Materi 1</span>
                                            </label>
                                        </li>
                                        <li>
                                            <label for="blog" class="blog">
                                                <span class="educate-icon educate-data-table icon-wrap"></span>
                                                <span class="title">Materi 2</span>
                                            </label>
                                        </li>
                                        <li>
                                            <label for="help" class="help">
                                                <span class="educate-icon educate-data-table icon-wrap"></span>
                                                <span class="title">Materi 3</span>
                                            </label>
                                        </li>
                                        <li>
                                            <label for="code" class="code">
                                                <span class="educate-icon educate-data-table icon-wrap"></span>
                                                <span class="title">Materi 4</span>
                                            </label>
                                        </li>
                                        <li>
                                            <label for="about" class="about">
                                                <span class="educate-icon educate-data-table icon-wrap"></span>
                                                <span class="title">Materi 5</span>
                                            </label>
                                        </li>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
@endsection
