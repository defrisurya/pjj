@extends('Mahasiswa.layouts.app')

@section('title', 'Transkrip Nilai')

@section('content')
    <div class="product-sales-area mg-tb-30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="product-sales-chart">
                        <div class="portlet-title">
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <div class="caption pro-sl-hd">
                                        <span class="caption-subject"><b>Transkrip Nilai</b></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="sparkline8-graph">
                            <div class="static-table-list">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Hari</th>
                                            <th>Matakuliah</th>
                                            <th>Dosen Pengampu</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>1</td>
                                            <td>Mamun</td>
                                            <td>Roshid</td>
                                            <td>@Facebook</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="breadcome-list-print">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <a href="#" class="link-profile">
                                            <button class="btn btn-primary btn-md">
                                                Cetak Transkrip Nilai
                                            </button>
                                        </a>
                                    </div>
                                    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                                        <ul>
                                            <li>
                                                <p>Matakuliah Wajib : </p>
                                            </li>
                                            <li>
                                                <p>Matakuliah Pilihan : </p>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                                        <ul>
                                            <li>
                                                <p>Konsentrasi : </p>
                                            </li>
                                            <li>
                                                <p>Total : </p>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="breadcome-list-ipk">
                                <div class="row">
                                    <div class="col-lg-12 col-md-6 col-sm-6 col-xs-12">
                                        <ul>
                                            <li>
                                                <p><b>Indeks Prestasi Kumulatif : </b></p>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
