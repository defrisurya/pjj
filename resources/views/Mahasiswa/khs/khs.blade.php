@extends('Mahasiswa.layouts.app')

@section('title', 'Kartu Hasil Studi')

@section('content')
    <div class="breadcome-area">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="breadcome-list">
                        <div class="sparkline12-graph">
                            <div class="basic-login-form-ad">
                                <div class="row">
                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                        <div class="sparkline10-list mt-b-30">
                                            <div class="sparkline10-hd">
                                                <div class="main-sparkline10-hd">
                                                    <h1>Kartu Hasil Studi (KHS)</h1>
                                                </div>
                                            </div>
                                            <div class="sparkline10-graph">
                                                <div class="basic-login-form-ad">
                                                    <div class="row">
                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                            <div class="basic-login-inner inline-basic-form">
                                                                <form action="#">
                                                                    <div class="form-group-inner">
                                                                        <div class="row">
                                                                            <div
                                                                                class="col-lg-5 col-md-3 col-sm-3 col-xs-12">
                                                                                <div
                                                                                    class="form-group-inner form-select-list">
                                                                                    <label>Semester</label>
                                                                                    <select
                                                                                        class="form-control custom-select-value">
                                                                                        <option value="">-- Pilih Semester
                                                                                            --
                                                                                        </option>
                                                                                        <option value="Semester 1">Semester
                                                                                            1
                                                                                        </option>
                                                                                        <option value="Semester 2">Semester
                                                                                            2
                                                                                        </option>
                                                                                        <option value="Semester 3">Semester
                                                                                            3
                                                                                        </option>
                                                                                        <option value="Semester 4">Semester
                                                                                            4
                                                                                        </option>
                                                                                        <option value="Semester 5">Semester
                                                                                            5
                                                                                        </option>
                                                                                        <option value="Semester 6">Semester
                                                                                            6
                                                                                        </option>
                                                                                        <option value="Semester 7">Semester
                                                                                            7
                                                                                        </option>
                                                                                        <option value="Semester 8">Semester
                                                                                            8
                                                                                        </option>
                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                            <div
                                                                                class="col-lg-5 col-md-3 col-sm-3 col-xs-12">
                                                                                <div
                                                                                    class="form-group-inner form-select-list">
                                                                                    <label>Tahun Akademik</label>
                                                                                    <select
                                                                                        class="form-control custom-select-value">
                                                                                        <option value="">-- Pilih Tahun
                                                                                            Akademik --
                                                                                        </option>
                                                                                        <option value="2022">2022
                                                                                        </option>
                                                                                        <option value="2023">2023
                                                                                        </option>
                                                                                        <option value="2024">2024
                                                                                        </option>
                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                            <div
                                                                                class="col-lg-2 col-md-6 col-sm-6 col-xs-12">
                                                                                <div class="login-btn-inner">
                                                                                    <div class="row">
                                                                                        <div
                                                                                            class="col-lg-6 col-md-5 col-sm-5 col-xs-12">
                                                                                            <div
                                                                                                class="login-horizental lg-hz-mg">
                                                                                                <button
                                                                                                    class="btn btn-sm btn-primary login-submit-cs"
                                                                                                    type="submit">
                                                                                                    Tampilkan KHS
                                                                                                </button>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="analytics-sparkle-area">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-6 col-sm-6 col-xs-12">
                    <div class="sparkline8-list">
                        <div class="sparkline8-hd">
                            <div class="portlet-title">
                            </div>
                        </div>
                        <div class="sparkline8-graph">
                            <div class="static-table-list">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Kode Matakuliah</th>
                                            <th>Matakuliah</th>
                                            <th>SKS</th>
                                            <th>Nilai</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>1</td>
                                            <td>Mamun</td>
                                            <td>Roshid</td>
                                            <td>@Facebook</td>
                                            <td>@Facebook</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="breadcome-list-print">
                                <div class="row">
                                    <div class="col-lg-8 col-md-6 col-sm-6 col-xs-12">
                                        <a href="{{ route('mhs') }}" class="link-profile">
                                            <button class="btn btn-primary btn-md">
                                                Cetak KHS
                                            </button>
                                        </a>
                                    </div>
                                    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                                        <ul>
                                            <li>
                                                <p>Jumlah SKS : </p>
                                            </li>
                                            <li>
                                                <p>Indeks Prestasi : </p>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
