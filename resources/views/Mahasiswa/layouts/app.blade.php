<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>@yield('title')</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('mahasiswa') }}/img/favicon.ico">
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Lora:wght@400;500;600&display=swap" rel="stylesheet">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ asset('mahasiswa') }}/css/bootstrap.min.css">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ asset('mahasiswa') }}/css/font-awesome.min.css">
    <!-- owl.carousel CSS -->
    <link rel="stylesheet" href="{{ asset('mahasiswa') }}/css/owl.carousel.css">
    <link rel="stylesheet" href="{{ asset('mahasiswa') }}/css/owl.theme.css">
    <link rel="stylesheet" href="{{ asset('mahasiswa') }}/css/owl.transitions.css">
    <!-- animate CSS -->
    <link rel="stylesheet" href="{{ asset('mahasiswa') }}/css/animate.css">
    <!-- normalize CSS -->
    <link rel="stylesheet" href="{{ asset('mahasiswa') }}/css/normalize.css">
    <!-- meanmenu icon CSS -->
    <link rel="stylesheet" href="{{ asset('mahasiswa') }}/css/meanmenu.min.css">
    <!-- main CSS -->
    <link rel="stylesheet" href="{{ asset('mahasiswa') }}/css/main.css">
    <!-- educate icon CSS -->
    <link rel="stylesheet" href="{{ asset('mahasiswa') }}/css/educate-custon-icon.css">
    <!-- morrisjs CSS -->
    <link rel="stylesheet" href="{{ asset('mahasiswa') }}/css/morrisjs/morris.css">
    <!-- mCustomScrollbar CSS -->
    <link rel="stylesheet" href="{{ asset('mahasiswa') }}/css/scrollbar/jquery.mCustomScrollbar.min.css">
    <!-- metisMenu CSS -->
    <link rel="stylesheet" href="{{ asset('mahasiswa') }}/css/metisMenu/metisMenu.min.css">
    <link rel="stylesheet" href="{{ asset('mahasiswa') }}/css/metisMenu/metisMenu-vertical.css">
    <!-- calendar CSS -->
    <link rel="stylesheet" href="{{ asset('mahasiswa') }}/css/calendar/fullcalendar.min.css">
    <link rel="stylesheet" href="{{ asset('mahasiswa') }}/css/calendar/fullcalendar.print.min.css">
    <!-- style CSS -->
    <link rel="stylesheet" href="{{ asset('mahasiswa') }}/css/style.css">
    <!-- responsive CSS -->
    <link rel="stylesheet" href="{{ asset('mahasiswa') }}/css/responsive.css">
    <!-- modernizr JS -->
    <script src="{{ asset('mahasiswa') }}/js/vendor/modernizr-2.8.3.min.js"></script>
</head>

<body>
    <!--[if lt IE 8]>
    <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

    @include('Mahasiswa.layouts.component.sidebar')

    @include('Mahasiswa.layouts.component.navbar')

    @yield('content')

    <!-- jquery -->
    <script src="{{ asset('mahasiswa') }}/js/vendor/jquery-1.12.4.min.js"></script>
    <!-- bootstrap JS -->
    <script src="{{ asset('mahasiswa') }}/js/bootstrap.min.js"></script>
    <!-- wow JS -->
    <script src="{{ asset('mahasiswa') }}/js/wow.min.js"></script>
    <!-- price-slider JS -->
    <script src="{{ asset('mahasiswa') }}/js/jquery-price-slider.js"></script>
    <!-- meanmenu JS -->
    <script src="{{ asset('mahasiswa') }}/js/jquery.meanmenu.js"></script>
    <!-- owl.carousel JS -->
    <script src="{{ asset('mahasiswa') }}/js/owl.carousel.min.js"></script>
    <!-- sticky JS -->
    <script src="{{ asset('mahasiswa') }}/js/jquery.sticky.js"></script>
    <!-- scrollUp JS -->
    <script src="{{ asset('mahasiswa') }}/js/jquery.scrollUp.min.js"></script>
    <!-- counterup JS -->
    <script src="{{ asset('mahasiswa') }}/js/counterup/jquery.counterup.min.js"></script>
    <script src="{{ asset('mahasiswa') }}/js/counterup/waypoints.min.js"></script>
    <script src="{{ asset('mahasiswa') }}/js/counterup/counterup-active.js"></script>
    <!-- mCustomScrollbar JS -->
    <script src="{{ asset('mahasiswa') }}/js/scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="{{ asset('mahasiswa') }}/js/scrollbar/mCustomScrollbar-active.js"></script>
    <!-- metisMenu JS -->
    <script src="{{ asset('mahasiswa') }}/js/metisMenu/metisMenu.min.js"></script>
    <script src="{{ asset('mahasiswa') }}/js/metisMenu/metisMenu-active.js"></script>
    <!-- morrisjs JS -->
    <script src="{{ asset('mahasiswa') }}/js/morrisjs/raphael-min.js"></script>
    <script src="{{ asset('mahasiswa') }}/js/morrisjs/morris.js"></script>
    <script src="{{ asset('mahasiswa') }}/js/morrisjs/morris-active.js"></script>
    <!-- morrisjs JS -->
    <script src="{{ asset('mahasiswa') }}/js/sparkline/jquery.sparkline.min.js"></script>
    <script src="{{ asset('mahasiswa') }}/js/sparkline/jquery.charts-sparkline.js"></script>
    <script src="{{ asset('mahasiswa') }}/js/sparkline/sparkline-active.js"></script>
    <!-- calendar JS -->
    <script src="{{ asset('mahasiswa') }}/js/calendar/moment.min.js"></script>
    <script src="{{ asset('mahasiswa') }}/js/calendar/fullcalendar.min.js"></script>
    <script src="{{ asset('mahasiswa') }}/js/calendar/fullcalendar-active.js"></script>
    <!-- plugins JS -->
    <script src="{{ asset('mahasiswa') }}/js/plugins.js"></script>
    <!-- main JS -->
    <script src="{{ asset('mahasiswa') }}/js/main.js"></script>
    <!-- tawk chat JS -->
    {{-- <script src="{{ asset('mahasiswa') }}/js/tawk-chat.js"></script> --}}
</body>

</html>
