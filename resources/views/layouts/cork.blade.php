<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
   
    @can('isAdmin')
        <title>Admin Kemahasiswaan</title>
    @endcan
    @can('isAdminAkademik')
        <title>Admin Akademik</title>
    @endcan
    @can('isAdminDosen')
        <title>Admin Dosen</title>
    @endcan
   
    {{-- Favicon --}}
    <link rel="icon" type="image/x-icon" href="#" />

    <link href="{{ asset('admin') }}/assets/css/loader.css" rel="stylesheet" type="text/css" />
    <script src="{{ asset('admin') }}/assets/js/loader.js"></script>

    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:400,600,700" rel="stylesheet">
    <link href="{{ asset('admin') }}/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="{{ asset('admin') }}/assets/css/plugins.css" rel="stylesheet" type="text/css" />
    <!-- END GLOBAL MANDATORY STYLES -->

    <!-- BEGIN PAGE LEVEL PLUGINS/CUSTOM STYLES -->
    <link href="{{ asset('admin') }}/plugins/apex/apexcharts.css" rel="stylesheet" type="text/css">
    <link href="{{ asset('admin') }}/assets/css/dashboard/dash_1.css" rel="stylesheet" type="text/css" />
    <!-- END PAGE LEVEL PLUGINS/CUSTOM STYLES -->

    {{-- CK Editor --}}
    <script src="//cdn.ckeditor.com/4.17.1/full/ckeditor.js"></script>
    {{-- <link rel="stylesheet" href="{{ asset('mahasiswa') }}/css/style.css"> --}}
    <style>
        .foto {
            width: 60%;
            margin-left: 20%;
            border-radius: 50%;
        }

        .nama1 {
            color: #444;
            font-size: 16px;
            font-weight: bold;
            padding: 2%;
        }

        .status1 {
            color: #444;
            font-size: 16px;
            font-weight: bold;
            padding: 2%;
            margin-bottom: 20px;
        }

        .nama0 {
            color: #444;
            font-size: 16px;
            font-weight: bold;
            padding: 2%;
        }

        .status0 {
            color: #444;
            font-size: 16px;
            font-weight: bold;
            padding: 2%;
        }

        .nim {
            color: #444;
            font-size: 16px;
            text-align: center;
        }

        .btn-custon-rounded-three {
            border-radius: 20px;
        }

        .link-profile {
            text-align: center;
        }

        hr {
            border: none;
            height: 3px;
            color: #333;
            background-color: #006DF0;
        }

        h6 .info {
            display: flex;
            float: right;
        }

        .link-submit {
            margin-left: 40%;
        }

        .breadcome-list-print {
            padding-top: 5%;
        }

        .breadcome-list-ipk {
            margin-left: 68%;
        }

        .breadcome-list-materi {
            padding-bottom: 20px;
            background: #fff;
            margin-bottom: 30px;
            margin-top: 20px;
        }

        .link-materi p {
            font-size: 14px;
            padding-top: 5px;
            text-decoration: none;
        }

        .link-materi p:hover {
            color: blue;
        }

        .breadcome-area-keterangan p {
            font-size: 20px;
            font-weight: bold;
            text-align: center;
            margin-top: 30px;
        }

        .foto-dosen {
            width: 40%;
            margin-left: 30%;
            border-radius: 50%;
        }

        .desk {
            padding: 3%;
            font-weight: bold;
        }

        .tugas {
            border: 1px solid black;
            border-radius: 10px;
            margin: 2%;
        }

        .container {
            max-width: 950px;
            width: 100%;
            padding: 40px 50px 40px 40px;
            background: #fff;
            margin: 0 20px;
            border-radius: 12px;
            box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
        }

        .container .topic {
            font-size: 30px;
            font-weight: 500;
            margin-bottom: 20px;
        }

        .content {
            display: flex;
            align-items: center;
            justify-content: space-between;
        }

        .content .list {
            display: flex;
            flex-direction: column;
            width: 100%;
            position: relative;
        }

        .content .list label {
            font-size: 15px;
            cursor: pointer;
            padding: 2%;
            transition: all 0.5s ease;
            color: #333;
            z-index: 12;
        }

        #home:checked~.list label.home,
        #blog:checked~.list label.blog,
        #help:checked~.list label.help,
        #code:checked~.list label.code,
        #about:checked~.list label.about {
            color: #fff;
        }

        .content .list label:hover {
            color: #6d50e2;
        }

        .content .slider {
            position: absolute;
            left: 0;
            top: 0;
            height: 60px;
            width: 100%;
            border-radius: 12px;
            background: #6d50e2;
            transition: all 0.4s ease;
        }

        #home:checked~.list .slider {
            top: 0;
        }

        #blog:checked~.list .slider {
            top: 60px;
        }

        #help:checked~.list .slider {
            top: 120px;
        }

        #code:checked~.list .slider {
            top: 180px;
        }

        #about:checked~.list .slider {
            top: 240px;
        }

        .content .text-content {
            width: 80%;
            height: 100%;
        }

        .content .text {
            display: none;
        }

        .content .text .title {
            font-size: 25px;
            padding: 2%;
        }

        .content .text p {
            text-align: justify;
            padding: 1%;
        }

        .content .text-content .home {
            display: block;
        }

        #home:checked~.text-content .home,
        #blog:checked~.text-content .blog,
        #help:checked~.text-content .help,
        #code:checked~.text-content .code,
        #about:checked~.text-content .about {
            display: block;
        }

        #blog:checked~.text-content .home,
        #help:checked~.text-content .home,
        #code:checked~.text-content .home,
        #about:checked~.text-content .home {
            display: none;
        }

        .content input {
            display: none;
        }

    </style>
</head>

<body class="alt-menu sidebar-noneoverflow">
    <!-- BEGIN LOADER -->
    <div id="load_screen">
        <div class="loader">
            <div class="loader-content">
                <div class="spinner-grow align-self-center"></div>
            </div>
        </div>
    </div>
    <!--  END LOADER -->

    <!--  BEGIN NAVBAR  -->
    @include('layouts.include.navbar')
    <!--  END NAVBAR  -->

    <!--  BEGIN MAIN CONTAINER  -->
    <div class="main-container sidebar-closed sbar-open" id="container">

        <div class="overlay"></div>
        <div class="cs-overlay"></div>
        <div class="search-overlay"></div>

        <!--  BEGIN SIDEBAR  -->
        @include('layouts.include.sidebar')
        <!--  END SIDEBAR  -->

        <!--  BEGIN CONTENT AREA  -->
        <div id="content" class="main-content">
            <div class="layout-px-spacing">

                {{-- @include('sweetalert::alert') --}}
                {{-- validasi kampus --}}
                {{-- <div class="row justify-content-center">
                    <div class="col-md-10">
                        @foreach ($errors->all() as $error)
                            <div class="alert alert-warning alert-dismissible fade show" role="alert">
                                <strong>{{ $error }}</strong>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        @endforeach
                    </div>
                </div> --}}

                <div class="row layout-top-spacing">

                    @yield('content')

                </div>

            </div>
        </div>
        <!--  END CONTENT AREA  -->

    </div>
    <!-- END MAIN CONTAINER -->

    <!-- BEGIN GLOBAL MANDATORY SCRIPTS -->
    <script src="{{ asset('admin') }}/assets/js/libs/jquery-3.1.1.min.js"></script>
    <script src="{{ asset('admin') }}/bootstrap/js/popper.min.js"></script>
    <script src="{{ asset('admin') }}/bootstrap/js/bootstrap.min.js"></script>
    <script src="{{ asset('admin') }}/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>
    <script src="{{ asset('admin') }}/assets/js/app.js"></script>
    <script>
        $(document).ready(function() {
            App.init();
        });
    </script>
    <script src="{{ asset('admin') }}/plugins/highlight/highlight.pack.js"></script>
    <script src="{{ asset('admin') }}/assets/js/custom.js"></script>
    <!-- END GLOBAL MANDATORY SCRIPTS -->

    <!-- BEGIN PAGE LEVEL PLUGINS/CUSTOM SCRIPTS -->
    <script src="{{ asset('admin') }}/plugins/apex/apexcharts.min.js"></script>
    <script src="{{ asset('admin') }}/assets/js/dashboard/dash_1.js"></script>
    <!-- BEGIN PAGE LEVEL PLUGINS/CUSTOM SCRIPTS -->

    @yield('js')

</body>

</html>
