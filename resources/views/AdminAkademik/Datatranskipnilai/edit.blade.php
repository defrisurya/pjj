@extends('layouts.cork')

@section('content')
    <div id="flStackForm" class="col-lg-12 layout-spacing layout-top-spacing">
        <div class="statbox widget box box-shadow col-md-12">
            <div class="widget-header">
                <div class="row">
                    <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                        <h4>Edit Data Prodi</h4>
                    </div>
                </div>
            </div>
            <div class="widget-content widget-content-area">
                <form action="{{ route('prodi.store') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label>Nama Kampus</label>
                        <select class="form-control basic" name="kampus_id">
                            <option value="">Pilih kampus</option>
                            @forelse ($kampus as $item)
                                <option {{ $prodi->kampus_id == $item->id ? 'selected' : '' }}
                                    value="{{ $item->id }}">{{ $item->namakampus }}</option>
                            @empty

                            @endforelse
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Kode Prodi</label>
                        <input type="text" name="kode_prodi" class="form-control" placeholder="Masukkan Kode Prodi"
                            value="{{ old('kode_prodi') }}">
                    </div>
                    <div class="form-group">
                        <label>Nama Prodi</label>
                        <input type="text" name="nama_prodi" class="form-control" placeholder="Masukkan Nama Prodi"
                            value="{{ old('nama_prodi') }}">
                    </div>
                    <div class="form-group mb-4">
                        <label for="exampleFormControlTextarea1">Profile Prodi</label>
                        <textarea name="profile_prodi" class="form-control" id="exampleFormControlTextarea1"
                            rows="3">{{ old('profile_prodi') }}</textarea>
                    </div>
                    <div class="form-group">
                        <label>Program Jurusan</label>
                        <input type="text" name="program_jurusan" class="form-control"
                            placeholder="Masukkan Program Jurusan" value="{{ old('program_jurusan') }}">
                    </div>
                    <div class="form-group">
                        <label>Biaya Kuliah</label>
                        <input type="text" name="biaya_kuliah" class="form-control" placeholder="Masukkan Biaya Kuliah"
                            value="{{ old('biaya_kuliah') }}">
                    </div>

                    <button type="submit" class="btn btn-primary mt-3">Submit</button>
                </form>


            </div>
        </div>
    </div>
@endsection

@section('js')
    <script src="{{ asset('admin') }}/assets/js/scrollspyNav.js"></script>
    <script src="{{ asset('admin') }}/plugins/select2/select2.min.js"></script>
    <script src="{{ asset('admin') }}/plugins/select2/custom-select2.js"></script>
    <script>
        var ss = $(".basic").select2({
            tags: true,
        });
    </script>
@endsection
