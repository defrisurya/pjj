@extends('layouts.cork')

@section('content')
    <div id="flStackForm" class="col-lg-12 layout-spacing layout-top-spacing">
        <div class="statbox widget box box-shadow col-md-12">
            <div class="widget-header">
                <div class="row">
                    <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                        <h4>Tambah Data Prodi</h4>
                    </div>
                </div>
            </div>
            <div class="widget-content widget-content-area">
                
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="product-sales-chart">
                            <div class="portlet-title">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <div class="caption pro-sl-hd">
                                            <span class="caption-subject"><b>Transkrip Nilai</b></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="sparkline8-graph">
                                <div class="static-table-list">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Kode</th>
                                                <th>Matakuliah</th>
                                                <th>SKS</th>
                                                <th>Nilai Huruf</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>1</td>
                                                <td>Mamun</td>
                                                <td>Roshid</td>
                                                <td>12</td>
                                                <td>@Facebook</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="breadcome-list-print">
                                    <div class="row">
                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                            <a href="#" class="link-profile">
                                                <button class="btn btn-primary btn-md">
                                                    Cetak Transkrip Nilai
                                                </button>
                                            </a>
                                        </div>
                                        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                                            <ul>
                                                <li>
                                                    <p>Matakuliah Wajib : </p>
                                                </li>
                                                <li>
                                                    <p>Matakuliah Pilihan : </p>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                                            <ul>
                                                <li>
                                                    <p>Konsentrasi : </p>
                                                </li>
                                                <li>
                                                    <p>Total : </p>
                                                </li>
                                                <li>
                                                    <p><b >Indeks Prestasi Kumulatif : </b></p>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script src="{{ asset('admin') }}/assets/js/scrollspyNav.js"></script>
    <script src="{{ asset('admin') }}/plugins/select2/select2.min.js"></script>
    <script src="{{ asset('admin') }}/plugins/select2/custom-select2.js"></script>
    <script>
        var ss = $(".basic").select2({
            tags: true,
        });
    </script>
@endsection


    {{-- <div class="col-lg-12 col-md-6 col-sm-6 col-xs-12"> --}}