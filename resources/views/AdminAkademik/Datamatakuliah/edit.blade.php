 @extends('layouts.cork')

@section('content')
    <div id="flStackForm" class="col-lg-12 layout-spacing layout-top-spacing">
        <div class="statbox widget box box-shadow col-md-12">
            <div class="widget-header">
                <div class="row">
                    <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                        <h4>Edit Data Matakuliah</h4>
                    </div>
                </div>
            </div>
            <div class="widget-content widget-content-area">
                <form action="{{ route('datamatakuliah.update', $datamatakuliah) }}" method="POST">
                    @csrf
                    @method('PUT')
                    <div class="form-group">
                        <label>Nama Kampus</label>
                        <select class="form-control basic" name="kampus_id">
                            <option value="">Pilih kampus</option>
                            @forelse ($kampus as $item)
                                <option {{ $datamatakuliah->kampus_id == $item->id ? 'selected' : '' }}
                                    value="{{ $item->id }}">{{ $item->nama_kampus }}</option>
                            @empty
                            @endforelse
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Nama Prodi</label>
                        <select class="form-control basic" name="prodi_id">
                            <option value="">Pilih Prodi</option>
                            @forelse ($prodi as $item)
                                <option {{ $datamatakuliah->prodi_id == $item->id ? 'selected' : '' }}
                                    value="{{ $item->id }}">{{ $item->nama_prodi }}</option>
                            @empty
                            @endforelse
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Semester</label>
                        <select name="semester" id="" class="form-control">
                            <option value="SEMESTER 1" @if($datamatakuliah->semester == 'SEMESTER 1') selected @endif>SEMESTER 1</option>
                            <option value="SEMESTER 2" @if($datamatakuliah->semester == 'SEMESTER 2') selected @endif>SEMESTER 2</option>
                            <option value="SEMESTER 3" @if($datamatakuliah->semester == 'SEMESTER 3') selected @endif>SEMESTER 3</option>
                            <option value="SEMESTER 4" @if($datamatakuliah->semester == 'SEMESTER 4') selected @endif>SEMESTER 4</option>
                            <option value="SEMESTER 5" @if($datamatakuliah->semester == 'SEMESTER 5') selected @endif>SEMESTERr 5</option>
                            <option value="SEMESTER 6" @if($datamatakuliah->semester == 'SEMESTER 6') selected @endif>SEMESTER 6</option>
                            <option value="SEMESTER 7" @if($datamatakuliah->semester == 'SEMESTER 7') selected @endif>SEMESTER 7</option>
                            <option value="SEMESTER 8" @if($datamatakuliah->semester == 'SEMESTER 8') selected @endif>SEMESTER 8</option>
                            <option value="SEMESTER 9" @if($datamatakuliah->semester == 'SEMESTER 9') selected @endif>SEMESTER 9</option>
                            <option value="SEMESTER 10" @if($datamatakuliah->semester == 'SEMESTER 10') selected @endif>SEMESTER 10</option>
                            <option value="SEMESTER 11" @if($datamatakuliah->semester == 'SEMESTER 11') selected @endif>SEMESTER 11</option>
                            <option value="SEMESTER 12" @if($datamatakuliah->semester == 'SEMESTER 12') selected @endif>SEMESTER 12</option>
                            <option value="SEMESTER 13" @if($datamatakuliah->semester == 'SEMESTER 13') selected @endif>SEMESTER 13</option>
                            <option value="SEMESTER 14" @if($datamatakuliah->semester == 'SEMESTER 14') selected @endif>SEMESTER 14</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Sifat Matakuliah</label>
                        <select class="form-control @error('sifat_matakuliah') is-invalid @enderror" name="sifat_matakuliah" id="">
                            <option value="wajib">WAJIB</option>
                            <option value="pilihan">PILIHAN</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Kode Matakuliah</label>
                        <input type="text" name="kode_matakuliah" class="form-control @error('kode_matakuliah') is-invalid @enderror" 
                        placeholder="Masukkan Nama Dosen" value="{{ $datamatakuliah->kode_matakuliah ?? old('kode_matakuliah') }}">
                        @error('kode_matakuliah')
                            <div class="invalid-feedback">{{ $message }} </div>
                        @enderror
                   </div>
                   <div class="form-group">
                        <label>Nama Matakuliah</label>
                        <input type="text" name="nama_matakuliah" class="form-control @error('nama_matakuliah') is-invalid @enderror" 
                        placeholder="Masukkan Nama Dosen" value="{{ $datamatakuliah->nama_matakuliah ?? old('nama_matakuliah') }}">
                        @error('nama_matakuliah')
                            <div class="invalid-feedback">{{ $message }} </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label>Jumlah SKS</label>
                        <input type="text" name="jumlah_sks" class="form-control @error('jumlah_sks') is-invalid @enderror" 
                        placeholder="Masukkan Nama Dosen" value="{{ $datamatakuliah->jumlah_sks ?? old('jumlah_sks') }}">
                        @error('jumlah_sks')
                            <div class="invalid-feedback">{{ $message }} </div>
                        @enderror
                    </div>
                    <button type="submit" class="btn btn-primary mt-3">UPDATE</button>
                </form>
            </div>
        </div>
    </div>
@endsection
