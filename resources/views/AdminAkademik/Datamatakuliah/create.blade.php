@extends('layouts.cork')

@section('content')
    <div id="flStackForm" class="col-lg-12 layout-spacing layout-top-spacing">
        <div class="statbox widget box box-shadow col-md-12">
            <div class="widget-header">
                <div class="row">
                    <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                        <h4>Tambah Data Matakuliah</h4>
                    </div>
                </div>
            </div>
            <div class="widget-content widget-content-area">
                <form action="{{ route('datamatakuliah.store') }}" method="POST">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label>Nama Kampus</label>
                        <select class="form-control  basic" name="kampus_id">
                            @forelse ($kampus as $item)
                                <option value="{{ $item->id }}">{{ $item->nama_kampus }}</option>
                            @empty
                            @endforelse
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Nama Prodi</label>
                        <select class="form-control basic" name="prodi_id">
                            {{-- <option value="">Pilih Prodi</option> --}}
                            @forelse ($prodi as $item)
                                <option value="{{ $item->id }}">{{ $item->nama_prodi }}</option>
                            @empty
                            @endforelse
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Semester</label>
                        <select class="form-control" name="semester" id="">
                            <option value="SEMESTER 1">SEMESTER 1</option>
                            <option value="SEMESTER 2">SEMESTER 2</option>
                            <option value="SEMESTER 3">SEMESTER 3</option>
                            <option value="SEMESTER 4">SEMESTER 4</option>
                            <option value="SEMESTER 5">SEMESTER 5</option>
                            <option value="SEMESTER 6">SEMESTER 6</option>
                            <option value="SEMESTER 7">SEMESTER 7</option>
                            <option value="SEMESTER 8">SEMESTER 8</option>
                            <option value="SEMESTER 9">SEMESTER 9</option>
                            <option value="SEMESTER 10">SEMESTER 10</option>
                            <option value="SEMESTER 11">SEMESTER 11</option>
                            <option value="SEMESTER 12">SEMESTER 12</option>
                            <option value="SEMESTER 13">SEMESTER 13</option>
                            <option value="SEMESTER 14">SEMESTER 14</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Kode Matakuliah</label>
                        <input type="text" name="kode_matakuliah" class="form-control @error('kode_matakuliah') is-invalid @enderror" 
                        placeholder="Masukkan Nama Dosen" value="{{ old('kode_matakuliah') }}">
                        @error('kode_matakuliah')
                            <div class="invalid-feedback">{{ $message }} </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label>Nama Matakuliah</label>
                        <input type="text" name="nama_matakuliah" class="form-control @error('nama_matakuliah') is-invalid @enderror" 
                        placeholder="Masukkan Nama Dosen" value="{{ old('nama_matakuliah') }}">
                        @error('nama_matakuliah')
                            <div class="invalid-feedback">{{ $message }} </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label>Jumlah SKS</label>
                        <input type="text" name="jumlah_sks" class="form-control @error('jumlah_sks') is-invalid @enderror" 
                        placeholder="Masukkan Jumlah SKS" value="{{ old('jumlah_sks') }}">
                        @error('jumlah_sks')
                            <div class="invalid-feedback">{{ $message }} </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label>Sifat Matakuliah</label>
                        <select class="form-control" name="sifat_matakuliah" id="">
                            <option value="WAJIB">WAJIB</option>
                            <option value="PILIHAN">PILIHAN</option>
                        </select>
                    </div>
                    <button type="submit" class="btn btn-primary mt-3">SIMPAN</button>
                </form>
            </div>
        </div>
    </div>
@endsection
