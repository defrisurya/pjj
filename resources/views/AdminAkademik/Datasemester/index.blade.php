@extends('layouts.cork')

@section('content')
    <div id="tableHover" class="col-lg-6 col-6 layout-spacing">
        <div class="statbox widget box box-shadow">
            <div class="widget-header">
                <div class="row">
                    <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                        <h4>DATA SEMESTER</h4>
                    </div>
                </div>
            </div>
            <div class="widget-content widget-content-area">
                <div class="table-responsive">
                    <table class="table table-bordered table-hover mb-4">
                        <thead>
                            <tr>
                                <th class="text-center">No</th>
                                <th class="text-center">Nama Kampus</th>
                                <th class="text-center">Tahun Akademik</th>
                                <th class="text-center">Semester</th>
                                <th class="text-center">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($data as $item => $jaran )
                            <tr class="text-center">
                                <td> {{ $data->firstItem() + $item }}</td>
                                <td> {{ $jaran->kampus['nama_kampus']}}</td>
                                <td> {{ $jaran->tahun_akademik }}</td>
                                <td> {{ $jaran->semester }}</td>
                                <td class="text-center">
                                    <a href="{{ route('datasemester.edit', $jaran) }}">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                            fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                            stroke-linejoin="round" class="feather feather-edit-2">
                                            <path d="M17 3a2.828 2.828 0 1 1 4 4L7.5 20.5 2 22l1.5-5.5L17 3z"></path>
                                        </svg>
                                    </a>
                                    <form action="{{ route('datasemester.destroy', $jaran) }}" method="POST" onsubmit="return confirm('Hapus Data, Anda Yakin ?')">
                                        {!! method_field('delete') . csrf_field() !!}
                                        <button class="dropdown-item" type="submit">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                                                stroke-linecap="round" stroke-linejoin="round"
                                                class="feather feather-trash-2 icon">
                                                <polyline points="3 6 5 6 21 6"></polyline>
                                                <path
                                                    d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2">
                                                </path>
                                            </svg>
                                        </button>
                                    </form>
                                </td>
                            </tr>
                        @empty
                        @endforelse
                        </tbody>
                    </table>
                </div>
            
                    {{ $data->links() }}  
            
            </div>
        </div>
    </div>
    <div id="tableHover" class="col-lg-6 col-6 layout-spacing">
        <div class="statbox widget box box-shadow">
            <div class="widget-header">
                <div class="row">
                    <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                        <h4>TAMBAH DATA SEMESTER</h4>
                    </div>
                </div>
            </div>
            <div class="widget-content widget-content-area">
                <form action="{{ route('datasemester.store') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label>Pilih Kampus</label>
                       <select class="form-control basic" name="kampus_id"> 
                        @forelse ($kampus as $item )
                            <option value="{{ $item->id }}">{{ $item->nama_kampus }} </option>
                        @empty
                        @endforelse
                       </select>
                    </div>
                    <div class="form-group">
                        <label>Tahun Akademik</label>
                        <input type="text" name="tahun_akademik" class="form-control @error('tahun_akademik') is-invalid @enderror" placeholder="Masukkan Tahun Akademik"
                            value="{{ old('tahun_akademik') }}">
                            @error('tahun_akademik')
                                <div class="invalid-feedback">{{ $message }} </div>
                            @enderror
                    </div>
                    <div class="form-group">
                        <label>Semester</label>
                        <input type="text" name="semester" class="form-control @error('tahun_akademik') is-invalid @enderror" placeholder="Masukkan Semester"
                            value="{{ old('semester') }}">
                    </div>
                    <button type="submit" class="btn btn-primary mt-3">SIMPAN</button>
                </form>
            </div>
        </div>
    </div>
@endsection
