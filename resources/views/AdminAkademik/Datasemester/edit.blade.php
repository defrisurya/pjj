@extends('layouts.cork')

@section('content')
    <div id="flStackForm" class="col-lg-12 layout-spacing layout-top-spacing">
        <div class="statbox widget box box-shadow col-md-12">
            <div class="widget-header">
                <div class="row">
                    <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                        <h4>Edit Data Semester</h4>
                    </div>
                </div>
            </div>
            <div class="widget-content widget-content-area">
                <form action="{{ route('datasemester.update', $datasemester) }}" method="POST">
                    @csrf
                    @method('PUT')
                    <div class="form-group">
                        <label>Nama Kampus</label>
                        <select class="form-control basic" name="kampus_id">
                            <option value="">Pilih kampus</option>
                            @forelse ($kampus as $item)
                                <option {{ $datasemester->kampus_id == $item->id ? 'selected' : '' }}
                                    value="{{ $item->id }}">{{ $item->nama_kampus }}</option>
                            @empty
                            @endforelse
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Tahun Akademik</label>
                        <input type="text" name="tahun_akademik" class="form-control @error('tahun_akademik') is-invalid @enderror" placeholder="Masukkan Tahun Akademik"
                        value="{{ $datasemester->tahun_akademik ?? old('tahun_akademik') }}">
                            @error('tahun_akademik')
                                <div class="invalid-feedback">{{ $message }} </div>
                            @enderror
                    </div>
                    <div class="form-group">
                        <label>Semester</label>
                        <input type="text" name="semester" class="form-control" placeholder="Masukkan Data Semester"
                            value="{{ $datasemester->semester ?? old('semester') }}">
                    </div>
                    <button type="submit" class="btn btn-primary mt-3">UPDATE</button>
                </form>
            </div>
        </div>
    </div>
@endsection
