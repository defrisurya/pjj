@extends('layouts.cork')

@section('content')
<div id="flStackForm" class="col-lg-12 layout-spacing layout-top-spacing">
    <div class="statbox widget box box-shadow col-md-12">
        <div class="widget-header">                                
            <div class="row">
                <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                    <h4>Edit Data KHS</h4>
                </div>                                                                        
            </div>
        </div>
        <div class="widget-content widget-content-area">
            <form action="{{ route('datakhs.update', $datakhs) }}" method="POST" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="form-group">
                    <label>Nama Kampus</label>
                    <select class="form-control basic" name="kampus_id">
                        <option value="">Pilih kampus</option>
                        @forelse ($kampus as $item)
                            <option {{ $datakhs->kampus_id == $item->id ? 'selected' : '' }}
                                value="{{ $item->id }}">{{ $item->nama_kampus }}</option>
                        @empty
                        @endforelse
                    </select>
                </div>
                <div class="form-group">
                    <label>Nama Prodi</label>
                    <select class="form-control basic" name="prodi_id">
                        <option value="">Pilih Prodi</option>
                        @forelse ($prodi as $item)
                            <option {{ $datakhs->prodi_id == $item->id ? 'selected' : '' }}
                                value="{{ $item->id }}">{{ $item->nama_prodi }}</option>
                        @empty
                        @endforelse
                    </select>
                </div>
                <div class="form-group">
                    <label>Pilih Tahun Akademik</label>
                    <input type="text" name="tahun_akademik" class="form-control @error('tahun_akademik') is-invalid @enderror" 
                    placeholder="Masukkan Nama Tahun akademik" value="{{  $datakhs->tahun_akademik ?? old('tahun_akademik') }}">
                    @error('tahun_akademik')
                        <div class="invalid-feedback">{{ $message }} </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label>Pilih Semester</label>
                    <input type="text" name="semester" class="form-control @error('semester') is-invalid @enderror" 
                    placeholder="Masukkan Nama semester" value="{{ $datakhs->semester ?? old('semester') }}">
                    @error('semester')
                        <div class="invalid-feedback">{{ $message }} </div>
                    @enderror
               </div>
               <div class="form-group">
                        <label>Pilih Matakuliah</label>
                        <input type="text" name="matakuliah" class="form-control @error('matakuliah') is-invalid @enderror" 
                        placeholder="Masukkan Nama matakuliah" value="{{ $datakhs->matakuliah ?? old('matakuliah') }}">
                        @error('matakuliah')
                            <div class="invalid-feedback">{{ $message }} </div>
                        @enderror
                </div>
                <div class="form-group">
                    <label>Nilai Angka</label>
                    <input type="text" name="nilai_angka" class="form-control @error('nilai_angka') is-invalid @enderror" 
                    placeholder="Masukkan Nama Nilai Angka" value="{{ $datakhs->nilai_angka ?? old('nilai_angka') }}">
                    @error('nilai_angka')
                        <div class="invalid-feedback">{{ $message }} </div>
                    @enderror
               </div>
               <div class="form-group">
                    <label>Nilai Huruf</label>
                    <input type="text" name="nilai_huruf" class="form-control @error('nilai_huruf') is-invalid @enderror" 
                    placeholder="Masukkan Nama Nilai Huruf" value="{{ $datakhs->nilai_huruf ?? old('nilai_huruf') }}">
                    @error('nilai_huruf')
                        <div class="invalid-feedback">{{ $message }} </div>
                    @enderror
                </div>
                <button type="submit" class="btn btn-primary mt-3">UPDATE</button>
            </form>
        </div>
    </div>
</div>
@endsection