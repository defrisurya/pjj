@extends('layouts.cork')

@section('content')
    <div id="flStackForm" class="col-lg-12 layout-spacing layout-top-spacing">
        <div class="statbox widget box box-shadow col-md-12">
            <div class="widget-header">
                <div class="row">
                    <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                        <h4>Edit Data Tahun Akademik</h4>
                    </div>
                </div>
            </div>
            <div class="widget-content widget-content-area">
                <form action="{{ route('datatahunakademik.update', $datatahunakademik) }}" method="POST">
                    @csrf
                    @method('PUT')
                    <div class="form-group">
                        <label>Nama Kampus</label>
                        <select class="form-control basic" name="kampus_id">
                            <option value="">Pilih kampus</option>
                            @forelse ($kampus as $item)
                                <option {{ $datatahunakademik->kampus_id == $item->id ? 'selected' : '' }}
                                    value="{{ $item->id }}">{{ $item->nama_kampus }}</option>
                            @empty
                            @endforelse
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Tahun Akademik</label>
                        <input type="text" name="tahun_akademik" class="form-control @error('tahun_akademik') is-invalid @enderror" 
                        placeholder="Masukkan Tahun Akademik" value="{{ $datatahunakademik->tahun_akademik ?? old('tahun_akademik') }}">
                        @error('tahun_akademik')
                            <div class="invalid-feedback">{{ $message }} </div>
                        @enderror
                    </div>
                   
                    <div class="form-group">
                        <label>Status</label>
                        <select name="status" class="form-control">
                            <option value="Tidak Aktif" @if($datatahunakademik->status == 'Tidak Aktif') selected @endif>Tidak Aktif</option>
                            <option value="Aktif" @if($datatahunakademik->status == 'Aktif') selected @endif>Aktif</option>
                        </select>
                    </div>
                    <button type="submit" class="btn btn-primary mt-3">UPDATE</button>
                </form>
            </div>
        </div>
    </div>
@endsection
