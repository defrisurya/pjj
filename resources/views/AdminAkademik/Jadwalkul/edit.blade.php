@extends('layouts.cork')

@section('content')
<div id="flStackForm" class="col-lg-12 layout-spacing layout-top-spacing">
    <div class="statbox widget box box-shadow col-md-12">
        <div class="widget-header">                                
            <div class="row">
                <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                    <h4>Edit Data Jadwal Perkuliahan</h4>
                </div>                                                                        
            </div>
        </div>
        <div class="widget-content widget-content-area">
            <form action="{{ route('jadwalkuliah.update', $datajadwalkuliah) }}" method="POST" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="form-group">
                    <label>Nama Kampus</label>
                    <select class="form-control basic" name="kampus_id">
                        <option value="">Pilih kampus</option>
                        @forelse ($kampus as $item)
                            <option {{ $datajadwalkuliah->kampus_id == $item->id ? 'selected' : '' }}
                                value="{{ $item->id }}">{{ $item->nama_kampus }}</option>
                        @empty
                        @endforelse
                    </select>
                </div>
                <div class="form-group">
                    <label>Nama Prodi</label>
                    <select class="form-control basic" name="prodi_id">
                        <option value="">Pilih Prodi</option>
                        @forelse ($prodi as $item)
                            <option {{ $datajadwalkuliah->prodi_id == $item->id ? 'selected' : '' }}
                                value="{{ $item->id }}">{{ $item->nama_prodi }}</option>
                        @empty
                        @endforelse
                    </select>
                </div>
                <div class="form-group">
                    <label>Pilih Dosen</label>
                    <select class="form-control basic" name="dosen_id">
                        <option value="">Nama Dosen</option>
                        @forelse ($dosen as $item)
                            <option {{ $datajadwalkuliah->dosen_id == $item->id ? 'selected' : '' }}
                                value="{{ $item->id }}">{{ $item->nama_dosen }}</option>
                        @empty
                        @endforelse
                    </select>
                </div>
                <div class="form-group">
                    <label>Pilih Tahun Akademik</label>
                    <input type="text" name="tahun_akademik" class="form-control @error('tahun_akademik') is-invalid @enderror" 
                    placeholder="Masukkan tahun akademik" value="{{ $datajadwalkuliah->tahun_akademik ?? old('tahun_akademik') }}">
                    @error('tahun_akademik')
                        <div class="invalid-feedback">{{ $message }} </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label>Hari</label>
                    <input type="text" name="hari" class="form-control @error('hari') is-invalid @enderror" 
                    placeholder="Masukkan Nama Hari" value="{{ $datajadwalkuliah->hari ?? old('hari') }}">
                    @error('hari')
                        <div class="invalid-feedback">{{ $message }} </div>
                    @enderror
               </div>

               
               <div class="form-group">
                <label>Nama Matakuliah</label>
                <input type="text" name="nama_matakuliah" class="form-control @error('nama_matakuliah') is-invalid @enderror" 
                placeholder="Masukkan Nama Nama Matakuliah" value="{{ $datajadwalkuliah->nama_matakuliah ?? old('nama_matakuliah') }}">
                    @error('nama_matakuliah')
                        <div class="invalid-feedback">{{ $message }} </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label>Sifat Mata Kuliah</label>
                    <select class="form-control" name="sifat_matakuliah" id="">
                        <option value="WAJIB" @if($datajadwalkuliah->sifat_matakuliah == 'WAJIB') selected @endif>WAJIB</option>
                        <option value="PILIHAN" @if($datajadwalkuliah->sifat_matakuliah == 'PILIHAN') selected @endif>PILIHAN</option>
                    </select>
                </div>
                <div class="form-group">
                    <label>Jumlah SKS</label>
                    <input type="number" name="jumlah_sks" class="form-control @error('jumlah_sks') is-invalid @enderror" 
                    placeholder="Jumlah SKS" value="{{ $datajadwalkuliah->jumlah_sks ?? old('jumlah_sks') }}">
                    @error('jumlah_sks')
                        <div class="invalid-feedback">{{ $message }} </div>
                    @enderror
                </div>
                <button type="submit" class="btn btn-primary mt-3">UPDATE</button>
            </form>
        </div>
    </div>
</div>
@endsection