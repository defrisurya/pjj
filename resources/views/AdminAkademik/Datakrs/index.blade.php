@extends('layouts.cork')

@section('content')
    <div id="tableHover" class="col-lg-12 col-12 layout-spacing">
            <div class="widget-header">
                <div class="row">
                    <div class="col-xl-2 col-md-6 col-sm-6 col-12">
                        <h4>DATA KRS</h4>
                    </div>
                    <div class="col-xl-2 col-md-6 col-sm-6 col-12 text-right">
                        <div class="form-group mt-3">
                            <select class="form-control" id="">
                                <option value="">Filter Tahun Akademik</option>
                                <option value="1">2020</option>
                                <option value="2">2021</option>
                                <option value="3">2022</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-xl-2 col-md-6 col-sm-6 col-12 text-right">
                        <div class="form-group mt-3">
                            <select class="form-control" id="">
                                <option value=""> Filter Semester </option>
                                <option value="1">Prodi 1</option>
                                <option value="2">Prodi 2</option>
                                <option value="3">Prodi 3</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-xl-2 col-md-6 col-sm-6 col-12 text-right">
                        <div class="form-group mt-3">
                            <select class="form-control" id="">
                                <option value=""> Filter Kampus </option>
                                <option value="1">Prodi 1</option>
                                <option value="2">Prodi 2</option>
                                <option value="3">Prodi 3</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-xl-2 col-md-6 col-sm-6 col-12 text-right">
                        <div class="form-group mt-3">
                            <select class="form-control" id="">
                                <option value=""> Filter Prodi </option>
                                <option value="1">Prodi 1</option>
                                <option value="2">Prodi 2</option>
                                <option value="3">Prodi 3</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-xl-2 col-md-6 col-sm-6 col-12 text-right">
                        <div class="form-group mt-2">
                            <a class="btn btn-outline-primary mt-3 mr-3" href="{{ route('datakrs.create') }}">PROSES</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="widget-content widget-content-area">
                <div class="table-responsive">
                    <table class="table table-bordered table-hover mb-4">
                        <thead>
                            <tr>
                                <th class="text-center">No</th>
                                <th class="text-center">Kampus</th>
                                <th class="text-center">Prodi</th>
                                <th class="text-center">Semester</th>
                                {{-- <th class="text-center">Kode Makul</th> --}}
                                <th class="text-center">Nama Makul</th>
                                <th class="text-center">Jumlah SKS</th>
                                <th class="text-center">Sifat</th>
                                <th class="text-center">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($data as $item => $jangkrik )
                            <tr class="text-center">
                                <td>{{ $data->firstItem() + $item }}</td>
                                <td>{{ $jangkrik->kampus['nama_kampus'] }} </td>
                                <td>{{ $jangkrik->prodi['nama_prodi'] }} </td>
                                <td>{{ $jangkrik->semester }} </td>
                                <td>{{ $jangkrik->matakuliah }} </td>
                                <td>{{ $jangkrik->jumlah_sks }} </td>
                                <td>{{ $jangkrik->sifat }} </td>
                                <td class="text-center">
                                    <a href="{{ route('datakrs.edit', $jangkrik) }}">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                            viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                                            stroke-linecap="round" stroke-linejoin="round"
                                            class="feather feather-edit-2">
                                            <path d="M17 3a2.828 2.828 0 1 1 4 4L7.5 20.5 2 22l1.5-5.5L17 3z"></path>
                                        </svg>
                                    </a>
                                    <form action="{{ route('datakrs.destroy', $jangkrik) }}" method="POST"
                                        onsubmit="return confirm('Hapus Data, Anda Yakin ?')">
                                        {!! method_field('delete') . csrf_field() !!}
                                        <button class="dropdown-item" type="submit">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                                                stroke-linecap="round" stroke-linejoin="round"
                                                class="feather feather-trash-2 icon">
                                                <polyline points="3 6 5 6 21 6"></polyline>
                                                <path
                                                    d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2">
                                                </path>
                                            </svg>
                                        </button>
                                    </form>
                                </td>
                            </tr>
                            @empty
                            @endforelse
                        </tbody>
                    </table>
                </div>
                {{ $data->links() }}  
            </div>
        </div>
    </div>
@endsection


<tr>
    <td>1</td>
    <td>10/08/2020</td>
    <td>10/08</td>
    <td>320</td>
    <td>10/08/2020</td>
    <td>10/08</td>
    <td>320</td>
    <td class="text-center"><span class="text-success">Complete</span></td>
    {{-- <td class="text-center"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash-2 icon"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path><line x1="10" y1="11" x2="10" y2="17"></line><line x1="14" y1="11" x2="14" y2="17"></line></svg></td>
--}}
    <td class="text-center">
        <a href="{{ route('datakrs.edit', 1) }}">
            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                stroke-linecap="round" stroke-linejoin="round"
                class="feather feather-edit-2">
                <path d="M17 3a2.828 2.828 0 1 1 4 4L7.5 20.5 2 22l1.5-5.5L17 3z"></path>
            </svg>
        </a>
        <form action="" method="POST"
            onsubmit="return confirm('Hapus Data, Anda Yakin ?')">
            {!! method_field('delete') . csrf_field() !!}
            <button class="dropdown-item" type="submit">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                    viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                    stroke-linecap="round" stroke-linejoin="round"
                    class="feather feather-trash-2 icon">
                    <polyline points="3 6 5 6 21 6"></polyline>
                    <path
                        d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2">
                    </path>
                </svg>
            </button>
        </form>

    </td>
</tr>