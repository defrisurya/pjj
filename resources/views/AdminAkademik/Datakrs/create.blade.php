@extends('layouts.cork')

@section('content')
<div id="flStackForm" class="col-lg-12 layout-spacing layout-top-spacing">
    <div class="statbox widget box box-shadow col-md-12">
        <div class="widget-header">                                
            <div class="row">
                <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                    <h4>Tambah Data KRS</h4>
                </div>                                                                        
            </div>
        </div>
        <div class="widget-content widget-content-area">
            <form action="{{ route('datakrs.store') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <label>Nama Kampus</label>
                    <select class="form-control  basic" name="kampus_id">
                        @forelse ($kampus as $item)
                            <option value="{{ $item->id }}">{{ $item->nama_kampus }}</option>
                        @empty
                        @endforelse
                    </select>
                </div>
                <div class="form-group">
                    <label>Nama Prodi</label>
                    <select class="form-control basic" name="prodi_id">
                        @forelse ($prodi as $item)
                            <option value="{{ $item->id }}">{{ $item->nama_prodi }}</option>
                        @empty
                        @endforelse
                    </select>
                </div>
                <div class="form-group">
                    <label>Pilih Tahun Akademik</label>
                    <input type="text" name="tahun_akademik" class="form-control @error('tahun_akademik') is-invalid @enderror" 
                    placeholder="Masukkan Nama tahun akademik" value="{{ old('tahun_akademik') }}">
                    @error('tahun_akademik')
                        <div class="invalid-feedback">{{ $message }} </div>
                    @enderror
                </div>
            
                <div class="form-group">
                    <label>Pilih Semester</label>
                    <input type="text" name="semester" class="form-control @error('semester') is-invalid @enderror" 
                    placeholder="Masukkan Nama tahun akademik" value="{{ old('semester') }}">
                    @error('semester')
                        <div class="invalid-feedback">{{ $message }} </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label>Pilih Matakuliah</label>
                    <input type="text" name="matakuliah" class="form-control @error('matakuliah') is-invalid @enderror" 
                    placeholder="Masukkan Nama tahun akademik" value="{{ old('matakuliah') }}">
                    @error('matakuliah')
                        <div class="invalid-feedback">{{ $message }} </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label>Jumlah SKS</label>
                    <input type="text" name="jumlah_sks" class="form-control @error('jumlah_sks') is-invalid @enderror" 
                    placeholder="Masukkan Nama tahun akademik" value="{{ old('jumlah_sks') }}">
                    @error('jumlah_sks')
                        <div class="invalid-feedback">{{ $message }} </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label>Sifat Matakuliah</label>
                    <select class="form-control" name="sifat" id="">
                        <option value="WAJIB">WAJIB</option>
                        <option value="PILIHAN">PILIHAN</option>
                    </select>
                </div>
                <button type="submit" class="btn btn-primary mt-3">SIMPAN</button>
            </form>
        </div>
    </div>
</div>
@endsection

@section('js')
<script>
    CKEDITOR.replace( 'editor1' );
</script>
@endsection