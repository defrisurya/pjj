@extends('layouts.cork')

@section('content')
    <div id="flStackForm" class="col-lg-12 layout-spacing layout-top-spacing">
        <div class="statbox widget box box-shadow col-md-12">
            <div class="widget-header">
                <div class="row">
                    <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                        <h4>Detail Pendaftar Camaba Oktober</h4>
                    </div>
                </div>
            </div>
            <div class="widget-content widget-content-area">
                <div class="form-group mt-5">
                    <div class="form-group mt-5">
                        <label>Nama Kampus</label>
                        <input type="text" name="kampus_id" class="form-control text-dark"
                            value="{{ $camaba->kampus['nama_kampus'] }}" readonly>
                    </div>
                </div>
                <div class="form-group">
                    <label>NIK</label>
                    <input type="text" name="nik" class="form-control text-dark" value="{{ $camaba->nik }}"
                        readonly>
                </div>
                <div class="form-group">
                    <label>Nama Camaba</label>
                    <input type="text" name="nama_camaba" class="form-control text-dark"
                        value="{{ $camaba->nama_camaba }}" readonly>
                </div>
                <div class="form-group">
                    <label>Tempat Lahir, Tanggal Lahir</label>
                    <input type="text" name="tempat_lahir" class="form-control text-dark"
                        value="{{ $camaba->tempat_lahir }}, {{ Carbon\Carbon::parse($camaba->tanggal_lahir)->translatedFormat('d F Y') }}"
                        readonly>
                </div>
                <div class="form-group">
                    <label>Jenis Kelamin</label>
                    <input type="text" name="jenis_kelamin" class="form-control text-dark"
                        value="{{ $camaba->jenis_kelamin }}" readonly>
                </div>
                <div class="form-group">
                    <label>Agama</label>
                    <input type="text" name="agama" class="form-control text-dark" value="{{ $camaba->agama }}"
                        readonly>
                </div>
                <div class="form-group">
                    <label>Nomor Telpon</label>
                    <input type="text" name="no_tlp" class="form-control text-dark" value="{{ $camaba->no_tlp }}"
                        readonly>
                </div>
                <div class="form-group">
                    <label>Email</label>
                    <input type="text" name="email" class="form-control text-dark" value="{{ $camaba->email }}"
                        readonly>
                </div>
                <div class="form-group mb-4">
                    <label for="exampleFormControlTextarea1">Alamat Lengkap</label>
                    <textarea name="alamat" class="form-control text-dark" id="exampleFormControlTextarea1" rows="3" readonly>{{ $camaba->alamat }}</textarea>
                </div>
                <div class="form-group">
                    <label>Asal Sekolah</label>
                    <input type="text" name="asal_sekolah" class="form-control text-dark"
                        value="{{ $camaba->asal_sekolah }}" readonly>
                </div>
                <div class="form-group">
                    <label>Program Studi yang di pilih</label>
                    <input type="text" name="prodi_id" class="form-control text-dark"
                        value="{{ $camaba->prodi['nama_prodi'] }}" readonly>
                </div>
                <div class="form-group">
                    <label>Foto Camaba</label>
                    <td align="center">
                        <img width="100px" src="{{ asset($camaba->foto) }}" alt="">
                        {{-- <button onclick="download()" target="_new" class="btn btn-primary mt-3">Download Foto</button> --}}
                    </td>
                </div>
                <div class="row">
                    <div class="col-xl-4 col-md-6 col-sm-6 col-12">
                        <label>FILE TRANSKIP</label>
                        <td align="center">
                            <iframe src="{{ asset($camaba->file_ijazah) }}" align="top" height="400" width="100%"
                                frameborder="0" scrolling="auto"></iframe>
                        </td>
                    </div>
                    <div class="col-xl-4 col-md-6 col-sm-6 col-12">
                        <label>FILE TRANSKIP</label>
                        <td align="center">
                            <iframe src="{{ asset($camaba->file_transkrip) }}" align="top" height="400" width="100%"
                                frameborder="1" scrolling="auto1"></iframe>
                        </td>
                    </div>

                    <div class="col-xl-4 col-md-6 col-sm-6 col-12">
                        <label>FILE TRANSKIP</label>
                        <td align="center">
                            <iframe src="{{ asset($camaba->file_raport) }}" align="top" height="400" width="100%"
                                frameborder="2" scrolling="auto2"></iframe>
                        </td>
                    </div>
                </div>
                <br>
                <br>
                <br>
                <br>
                <div class="breadcome-list link-submit">
                    <span class="form-group center">
                        <b style="position: center">Verifikasi Pendaftaran Mahasiswa</b>
                    </span>
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <a href="{{ url('Admin_Kemahasiswaan/diterima/' . $camaba->user_id . '/' . $camaba->id) }}"
                                type="button" class="btn btn-danger mt-3" style="position: right">DI TERIMA</a>
                            <a href="" type="button" class="btn btn-info mt-3" style="position: right">DI
                                TOLAK</a>
                        </div>
                    </div>
                </div>
                {{-- </form> --}}
            </div>
        </div>
    </div>
@endsection
<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.26.1/axios.min.js"></script>
<script>
    function download() {
        axios({
                url: 'https://source.unsplash.com/random/500x500',
                method: 'GET',
                responseType: 'blob'
            })
            .then((response) {
                const url = window.URL.createObjectURL(new Blob([response.data]))
                const link = document.createElement('a')
                link.href = link
                link.setAttribute('download', 'foto.png')
                document.body.appendChild(link)
                link.click()
            })
    }
</script>
