@extends('layouts.cork')

@section('content')
    <div id="tableHover" class="col-lg-12 col-12 layout-spacing">
        <div class="statbox widget box box-shadow">
            <div class="widget-header">
                <div class="row">
                    <div class="col-xl-6 col-md-6 col-sm-6 col-12">
                        <h4>Info Pendaftaran PMB</h4>
                    </div>
                    <div class="col-xl-3 col-md-6 col-sm-6 col-12 text-right">
                        <div class="form-group mt-3">
                            <select class="form-control" id="">
                                <option value="">Filter Tahun Akademik</option>
                                <option value="1">2020</option>
                                <option value="2">2021</option>
                                <option value="3">2022</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-6 col-sm-6 col-12 text-right">
                        <div class="form-group mt-3">
                            <select class="form-control" id="">
                                <option value="">Filter Kampus</option>
                                <option value="1">UTY</option>
                                <option value="2">UGM</option>
                                <option value="3">UAD</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="widget-content widget-content-area">
                <div class="table-responsive">
                    <table class="table table-bordered table-hover mb-4">
                        <thead>
                            <tr>
                                <th class="text-center">No</th>
                                <th class="text-center">Nama Kampus</th>
                                <th class="text-center">Prodi</th>
                                <th class="text-center">Nama Calon Mahasiswa</th>
                                <th class="text-center">Asal Sekolah</th>
                                <th class="text-center">Status Pendaftar</th>
                                <th class="text-center">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($data as $item => $camaba)
                                @php
                                    $user = App\User::where('id', $camaba->user_id)->first();
                                @endphp
                                <tr>
                                    <td class="text-center"> {{ $data->firstItem() + $item }} </td>
                                    {{-- <td class="text-center"> {{ $camaba->kampus_id }}</td> --}}
                                    <td class="text-center"> {{ $camaba->kampus['nama_kampus'] }}</td>
                                    <td class="text-center"> {{ $camaba->prodi['nama_prodi'] }}</td>
                                    <td class="text-center"> {{ $camaba->nama_camaba }}</td>
                                    <td class="text-center"> {{ $camaba->asal_sekolah }}</td>
                                    <td class="text-center"><a class="btn btn-warning"> {{ $user->role }} </a></td>
                                    <td class="text-center">
                                        <a href="{{ route('pendaftar.show', $camaba->id) }}">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                viewBox="0 0 24 24" fill="none" stroke="#000000" stroke-width="2"
                                                stroke-linecap="round" stroke-linejoin="round">
                                                <path d="M1 12s4-8 11-8 11 8 11 8-4 8-11 8-11-8-11-8z"></path>
                                                <circle cx="12" cy="12" r="3"></circle>
                                            </svg>
                                        </a>
                                    </td>
                                </tr>
                            @empty
                            @endforelse
                        </tbody>
                    </table>
                </div>
                {{ $data->links() }}
            </div>
        </div>
    </div>
@endsection
