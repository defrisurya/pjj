@extends('layouts.cork')

@section('content')
    <div id="flStackForm" class="col-lg-12 layout-spacing layout-top-spacing">
        <div class="statbox widget box box-shadow col-md-12">
            <div class="widget-header">
                <div class="row">
                    <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                        <h4>Tambah Data Kampus</h4>
                    </div>
                </div>
            </div>
            <div class="widget-content widget-content-area">
                <form action="{{ route('infopmb.store') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label>Pilih Kampus</label>
                        <select class="form-control basic" name="kampus_id" required>
                            <option value="">Pilih Kampus</option>
                            @foreach ($kampus as $item)
                                <option value="{{ $item->id }}">{{ $item->nama_kampus }} </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Judul Info PMB</label>
                        <input type="text" name="judul_info"
                            class="form-control @error('judul_info') is-invalid @enderror" placeholder="Judul Info PMB"
                            value="{{ old('judul_info') }}">
                        @error('judul_info')
                            <div class="invalid-feedback">{{ $message }} </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label>Waktu Pendaftaran</label>
                        <div class="row">
                            <div class="col-md-3 mb-3">
                                <input type="date" name="waktupendaftaran_start"
                                    class="form-control @error('waktupendaftaran_start') is-invalid @enderror"
                                    placeholder="" value="{{ old('waktupendaftaran_start') }}">
                                @error('waktupendaftaran_start')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="col-md-1 mb-3 mt-2 text-center">
                                <span><b>sampai</b></span>
                            </div>
                            <div class="col-md-3 mb-3">
                                <input type="date" name="waktupendaftaran_end"
                                    class="form-control @error('waktupendaftaran_end') is-invalid @enderror" placeholder=""
                                    value="{{ old('waktupendaftaran_end') }}">
                                @error('waktupendaftaran_end')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Biaya Pendaftaran</label>
                        <input type="number" name="biayapendaftaran"
                            class="form-control @error('biayapendaftaran') is-invalid @enderror"
                            placeholder="Masukkan Biaya Pendaftaran" value="{{ old('biayapendaftaran') }}">
                        @error('biayapendaftaran')
                            <div class="invalid-feedback">{{ $message }} </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label>Tahun Akademik</label>
                        <input type="text" name="tahun_akademik"
                            class="form-control @error('tahun_akademik') is-invalid @enderror"
                            placeholder="Masukkan Tahun Akademik" value="{{ old('tahun_akademik') }}">
                        @error('tahun_akademik')
                            <div class="invalid-feedback">{{ $message }} </div>
                        @enderror
                    </div>
                    <div class="form-group mb-4">
                        <label for="exampleFormControlTextarea1">Tahapan Seleksi</label>
                        <div class="row">
                            <div class="col-md-3 mb-3">
                                <input type="text" name="tahapanseleksi"
                                    class="form-control @error('tahapanseleksi') is-invalid @enderror"
                                    placeholder="Tahapan 1" value="{{ old('tahapanseleksi') }}">
                            </div>
                            <div class="col-md-3 mb-3">
                                <input type="text" name="tahapanseleksi_2"
                                    class="form-control @error('tahapanseleksi') is-invalid @enderror"
                                    placeholder="Tahapan 2" value="{{ old('tahapanseleksi') }}">
                            </div>
                            <div class="col-md-3 mb-3">
                                <input type="text" name="tahapanseleksi_3"
                                    class="form-control @error('tahapanseleksi') is-invalid @enderror"
                                    placeholder="Tahapan 3" value="{{ old('tahapanseleksi') }}">
                            </div>
                            <div class="col-md-3 mb-3">
                                <input type="text" name="tahapanseleksi_4"
                                    class="form-control @error('tahapanseleksi') is-invalid @enderror"
                                    placeholder="Tahapan 4" value="{{ old('tahapanseleksi') }}">
                            </div>
                            <div class="col-md-3 mb-3">
                                <input type="text" name="tahapanseleksi_5"
                                    class="form-control @error('tahapanseleksi') is-invalid @enderror"
                                    placeholder="Tahapan 5" value="{{ old('tahapanseleksi') }}">
                            </div>
                            <div class="col-md-3 mb-3">
                                <input type="text" name="tahapanseleksi_6"
                                    class="form-control @error('tahapanseleksi') is-invalid @enderror"
                                    placeholder="Tahapan 6" value="{{ old('tahapanseleksi') }}">
                            </div>
                            <div class="col-md-3 mb-3">
                                <input type="text" name="tahapanseleksi_7"
                                    class="form-control @error('tahapanseleksi') is-invalid @enderror"
                                    placeholder="Tahapan 7" value="{{ old('tahapanseleksi') }}">
                            </div>
                            <div class="col-md-3 mb-3">
                                <input type="text" name="tahapanseleksi_8"
                                    class="form-control @error('tahapanseleksi') is-invalid @enderror"
                                    placeholder="Tahapan 8" value="{{ old('tahapanseleksi') }}">
                            </div>
                        </div>
                    </div>
                    <div class="form-group mb-4 mt-3">
                        <label for="exampleFormControlFile1">Foto Poster</label>
                        <input type="file" name="fotoposter" class="form-control-file" id="exampleFormControlFile1">
                    </div>
                    <button type="submit" class="btn btn-primary mt-3">Simpan</button>
                </form>
            </div>
        </div>
    </div>
@endsection
