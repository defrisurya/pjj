@extends('layouts.cork')

@section('content')
<div id="flStackForm" class="col-lg-12 layout-spacing layout-top-spacing">
    <div class="statbox widget box box-shadow col-md-12">
        <div class="widget-header">                                
            <div class="row">
                <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                    <h4>Edit Data Kampus</h4>
                </div>                                                                        
            </div>
        </div>
        <div class="widget-content widget-content-area">
            <form action="{{ route('kampus.update', $kampus->id) }}" method="POST" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="form-group">
                    <label>Nama Kampus</label>
                    <input type="text" name="nama_kampus" class="form-control @error('nama_kampus') is-invalid @enderror" 
                    placeholder="Masukkan Nama Kampus" value="{{ $kampus->nama_kampus ?? old('nama_kampus') }}">
                    @error('nama_kampus')
                        <div class="invalid-feedback">{{ $message }}</div>
                    @enderror
                </div> 
                <div class="form-group">
                    <label>Akreditasi Kampus</label>
                    <select name="akreditasi_kampus" class="form-control">
                        <option value="A" @if($kampus->akreditasi_kampus == 'A') selected @endif>A</option>
                        <option value="B" @if($kampus->akreditasi_kampus == 'B') selected @endif>B</option>
                        <option value="C" @if($kampus->akreditasi_kampus == 'C') selected @endif>C</option>
                    </select>
                </div>
                <div class="form-group">
                    <label>Tipe Kampus</label>
                    <select name="tipe_kampus" class="form-control">
                        <option value="Swasta" @if($kampus->tipe_kampus == 'Swasta') selected @endif>SWASTA</option>
                        <option value="Negeri" @if($kampus->tipe_kampus == 'Negeri') selected @endif>NEGERI</option>
                    </select>
                </div>
                <div class="form-group">
                    <label>Video</label>
                    <input type="text" name="video_kampus" class="form-control @error('video_kampus') is-invalid @enderror"
                     placeholder="Masukkan Url VIdeo Youtube Kampus" value="{{ $kampus->video_kampus ?? old('video_kampus') }}">
                    @error('video_kampus')
                        <div class="invalid-feedback">{{ $message }}</div>
                    @enderror
                </div> 
                <div class="form-group mb-4 mt-3">
                    <label for="exampleFormControlFile1">Logo</label>
                    <input type="file" name="logo_kampus" class="form-control-file" id="exampleFormControlFile1">
                    <div class="form-group mt-2" style="max-width: 20rem;">
                        <img width="300" src="{{ asset($kampus->logo_kampus) }}" alt="">
                    </div>
                </div>
                <div class="form-group mb-4">
                    <label for="exampleFormControlTextarea1">Deskripsi Kampus</label>
                    <textarea name="deskripsi_kampus" class="form-control @error('deskripsi_kampus') is-invalid @enderror" 
                    id="exampleFormControlTextarea1" rows="3">{{  $kampus->deskripsi_kampus ?? old('deskripsi_kampus') }}</textarea>
                    @error('deskripsi_kampus')
                        <div class="invalid-feedback">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group mb-4">
                    <label for="exampleFormControlTextarea1">Alamat</label>
                    <textarea name="alamat_kampus" class="form-control @error('alamat_kampus') is-invalid @enderror" 
                    id="exampleFormControlTextarea1" rows="3">{{ $kampus->alamat_kampus ?? old('alamat_kampus') }}</textarea>
                    @error('alamat_kampus')
                        <div class="invalid-feedback">{{ $message }}</div>
                    @enderror
                </div>
                <button type="submit" class="btn btn-primary mt-3">Update</button>
            </form>
        </div>
    </div>
</div>
@endsection

@section('js')
    <script> 
        CKEDITOR.replace('deskripsi_kampus');
        CKEDITOR.replace('alamat_kampus');
    </script>
@endsection