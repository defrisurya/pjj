@extends('layouts.cork')

@section('content')
    <div id="flStackForm" class="col-lg-12 layout-spacing layout-top-spacing">
        <div class="statbox widget box box-shadow col-md-12">
            <div class="widget-header">
                <div class="row">
                    <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                        <h4>Tambah Data Kampus</h4>
                    </div>
                </div>
            </div>
            <div class="widget-content widget-content-area">
                <form action="{{ route('kampus.store') }}" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label>Nama Kampus</label>
                        <input type="text" name="nama_kampus" id="nama_kampus"
                            class="form-control @error('nama_kampus') is-invalid @enderror"
                            placeholder="Masukkan Nama Kampus" value="{{ old('nama_kampus') }}">
                        @error('nama_kampus')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>

                    {{-- <div class="form-group">
                    <label>Nama Kampus</label>
                    <input type="text" name="nama_kampus" id="nama_kampus" class="form-control @error('nama_kampus') is-invalid @enderror"
                    placeholder="Masukkan Nama Kampus" value="{{ old('nama_kampus') }}">
                    @error('nama_kampus')
                        <div class="invalid-feedback">{{ $message }}</div>
                    @enderror
                </div>  --}}
                    <div class="form-group">
                        <label>Akreditasi Kampus</label>
                        <select class="form-control" name="akreditasi_kampus">
                            <option value="">Pilih Akreditasi</option>
                            <option value="A">A</option>
                            <option value="B">B</option>
                            <option value="C">C</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Tipe Kampus</label>
                        <select class="form-control" name="tipe_kampus">
                            <option value="">Pilih Tipe Kampus</option>
                            <option value="NEGERI">NEGERI</option>
                            <option value="SWASTA">SWASTA</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Video</label>
                        <input type="text" name="video_kampus" id="video_kampus"
                            class="form-control @error('video_kampus') is-invalid @enderror"
                            placeholder="Masukkan Url VIdeo Youtube Kampus" value="{{ old('video_kampus') }}">
                        @error('nama_kampus')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group mb-4 mt-3">
                        <label for="exampleFormControlFile1">Logo Kampus</label>
                        <input type="file" name="logo_kampus" id="logo_kampus" class="form-control-file"
                            id="exampleFormControlFile1">
                    </div>
                    <div class="form-group mb-4">
                        <label for="exampleFormControlTextarea1">Deskripsi</label>
                        <textarea name="deskripsi_kampus" id="deskripsi_kampus"
                            class="form-control @error('diskripsi_kampus') is-invalid @enderror" id="exampleFormControlTextarea1"
                            rows="3">{{ old('deskripsi_kampus') }}</textarea>
                        @error('deskripsi_kampus')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group mb-4">
                        <label for="exampleFormControlTextarea1">Alamat</label>
                        <textarea name="alamat_kampus" id="alamat_kampus" class="form-control @error('alamat_kampus') is-invalid @enderror"
                            id="exampleFormControlTextarea1" rows="3">{{ old('alamat_kampus') }}</textarea>
                        @error('alamat_kampus')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                    <button type="submit" class="btn btn-primary mt-3">Simpan</button>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script>
        CKEDITOR.replace('deskripsi_kampus');
        CKEDITOR.replace('alamat_kampus');
    </script>
@endsection
