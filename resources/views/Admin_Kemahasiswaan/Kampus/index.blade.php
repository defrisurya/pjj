@extends('layouts.cork')

@section('content')
    <div id="tableHover" class="col-lg-12 col-12 layout-spacing">
        <div class="statbox widget box box-shadow">
            <div class="widget-header">
                <div class="row">
                    <div class="col-xl-6 col-md-6 col-sm-6 col-12">
                        <h4>DATA KAMPUS</h4>
                    </div>
                    <div class="col-xl-6 col-md-6 col-sm-6 col-12 text-right">
                        <a class="btn btn-outline-primary mt-3 mr-3" href="{{ route('kampus.create') }}">Tambah Data Kampus</a>
                    </div>
                </div>
            </div>
            <div class="widget-content widget-content-area">
                <div class="table-responsive">
                    <table class="table table-bordered table-hover mb-4">
                        <thead>
                            <tr>
                                <th class="text-center">No</th>
                                <th class="text-center">Nama</th>
                                <th class="text-center">Logo</th>
                                <th class="text-center">Akreditasi</th>
                                <th class="text-center">Tipe kampus</th>
                                <th class="text-center">Deskripsi</th>
                                <th class="text-center">Alamat</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            {{-- {{ dd($data) }} --}}
                            @forelse ($data as $item => $kampus)
                                <tr>
                                    <td align="center">{{ $data->firstItem() + $item }}</td>
                                    <td align="center">{{ $kampus->nama_kampus }}</td>
                                    <td align="center">
                                        <img width="100px" src="{{ asset($kampus->logo_kampus) }}" alt="">
                                    </td>
                                    <td align="center">{{ $kampus->akreditasi_kampus }}</td>
                                    <td align="center">{{ $kampus->tipe_kampus }}</td>
                                    <td align="center">{!! $kampus->deskripsi_kampus !!}</td>
                                    <td align="center">{!! $kampus->alamat_kampus !!}</td>
                                    <td class="text-center">
                                        <a href="{{ route('kampus.edit', $kampus->id) }}">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                                                stroke-linecap="round" stroke-linejoin="round"
                                                class="feather feather-edit-2">
                                                <path d="M17 3a2.828 2.828 0 1 1 4 4L7.5 20.5 2 22l1.5-5.5L17 3z"></path>
                                            </svg>
                                        </a>
                                        <form action="{{ route('kampus.destroy', $kampus) }}" method="POST"
                                            onsubmit="return confirm('Hapus Data, Anda Yakin ?')">
                                            {!! method_field('delete') . csrf_field() !!}
                                            <button class="dropdown-item" type="submit">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                    viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                                                    stroke-linecap="round" stroke-linejoin="round"
                                                    class="feather feather-trash-2 icon">
                                                    <polyline points="3 6 5 6 21 6"></polyline>
                                                    <path
                                                        d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2">
                                                    </path>
                                                </svg>
                                            </button>
                                        </form>

                                    </td>
                                </tr>
                                @empty
                            @endforelse
                        </tbody>
                    </table>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    {{ $data->links() }}  
                </div>
                
            </div>
        </div>
    </div>
@endsection
