@extends('layouts.cork')

@section('content')
    <div id="flStackForm" class="col-lg-12 layout-spacing layout-top-spacing">
        <div class="statbox widget box box-shadow col-md-12">
            <div class="widget-header">
                <div class="row">
                    <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                        <h4>Detail Data Dosen</h4>
                    </div>
                </div>
            </div>
            <div class="widget-content widget-content-area">
                <form action="#">
                   {{--  @csrf
                    @method('PUT') --}}
                    <div class="form-group">
                        <label>Foto</label>
                        <div class="form-group mt-2" style="max-width: 20rem;">
                            <img width="300" src="{{ asset($dosen->foto_dosen) }}" alt="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Nama Dosen</label>
                        <input type="text" name="name" class="form-control @error('name') is-invalid @enderror" 
                        placeholder="Masukkan Nama Dosen" value="{{ $dosen->user['name'] }}">
                        @error('name')
                            <div class="invalid-feedback">{{ $messeage }} </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label>Nip Dosen</label>
                        <input type="number" name="nip" class="form-control @error('nip') is-invalid @enderror" 
                        placeholder="Masukkan Nip Dosen" value="{{ $dosen->nip ?? old('nip') }}">
                        @error('nip')
                            <div class="invalid-feedback">{{ $messeage }} </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label>Jenis Kelamin</label>
                        <select class="form-control" name="jenis_kelamin" id="">
                            <option value="L" @if($dosen->jenis_kelamin == 'L') selected @endif>LAKI-LAKI</option>
                            <option value="P" @if($dosen->jenis_kelamin == 'P') selected @endif>PEREMPUAN</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Pendidikan Terakhir</label>
                        <select class="form-control" name="pendidikan_ayah" id="">
                            <option value="SMU" @if($dosen->pendidikan == 'SMU') selected @endif>SMU</option>
                            <option value="S1" @if($dosen->pendidikan == 'S1') selected @endif>S1</option>
                            <option value="S2" @if($dosen->pendidikan == 'S2') selected @endif>S2</option>
                            <option value="S3" @if($dosen->pendidikan == 'S3') selected @endif>S3</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Nomor Telpon</label>
                        <input type="text" name="no_telpon" class="form-control" value="{{ old('no_telpon', $dosen->no_telpon) }}">
                    </div>
                    <div class="form-group">
                        <label>Email</label>
                        <input type="text" name="email" class="form-control" value="{{ $dosen->email ?? old('email') }}">
                    </div>
                    <div class="form-group-inner">
                        <label>Alamat</label>
                        <textarea name="alamat" id="alamat" class="form-control" value="{{ old('alamat', $dosen->alamat ) }}">{{ $dosen->alamat }}</textarea>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
