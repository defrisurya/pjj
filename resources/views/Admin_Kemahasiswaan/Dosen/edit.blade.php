@extends('layouts.cork')

@section('content')
    <div id="flStackForm" class="col-lg-12 layout-spacing layout-top-spacing">
        <div class="statbox widget box box-shadow col-md-12">
            <div class="widget-header">
                <div class="row">
                    <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                        <h4>Edit Data Dosen</h4>
                    </div>
                </div>
            </div>
            <div class="widget-content widget-content-area">
                <form action="{{ route('dosen.update', $dosen->id) }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    <div class="form-group">
                        <label>Nama Kampus</label>
                        <select class="form-control basic" name="kampus_id[]">
                            <option value="">Pilih kampus</option>
                            @forelse ($kampus as $item)
                                <option {{ $dosen->kampus_id == $item->id ? 'selected' : '' }}
                                    value="{{ $item->id }}">{{ $item->nama_kampus }}</option>
                            @empty
                            @endforelse
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Nama Prodi</label>
                        <select class="form-control basic" name="prodi_id[]">
                            <option value="">Pilih Prodi</option>
                            @forelse ($prodi as $item)
                                <option {{ $dosen->prodi_id == $item->id ? 'selected' : '' }}
                                    value="{{ $item->id }}">{{ $item->nama_prodi }}</option>
                            @empty
                            @endforelse
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Nama Fakultas</label>
                        <select class="form-control basic" name="fakultas_id[]">
                            <option value="">Pilih Fakultas</option>
                            @forelse ($fakultas as $item)
                                <option {{ $dosen->fakultas_id == $item->id ? 'selected' : '' }}
                                    value="{{ $item->id }}">{{ $item->nama_fakultas }}</option>
                            @empty
                            @endforelse
                        </select>
                    </div>
                    <div class="form-group">
                            <label>Nama Dosen</label>
                            <input type="text" name="name" class="form-control @error('name') is-invalid @enderror" 
                            placeholder="Masukkan Nama Dosen" value="{{old('name', $user->name ) }}">
                            @error('name')
                                <div class="invalid-feedback">{{ $message }} </div>
                            @enderror
                    </div>
                    <div class="form-group">
                            <label>Email</label>
                            <input type="email" name="email" class="form-control @error('email') is-invalid @enderror" 
                            placeholder="Masukkan Nama Dosen" value="{{old('email', $user->email ) }}" readonly>
                            @error('email')
                                <div class="invalid-feedback">{{ $message }} </div>
                            @enderror
                    </div>
                    <div class="form-group">
                            <label>Password Baru</label>
                            <input type="text" name="password" class="form-control @error('password') is-invalid @enderror" 
                            placeholder="Masukkan Password Baru" value="{{old('password') }}">
                            @error('password')
                                <div class="invalid-feedback">{{ $message }} </div>
                            @enderror
                    </div>
                    <div class="form-group">
                        <div class="col-md-3 mb-3">
                            <label>Nip Dosen</label>
                            <input type="number" name="nip[]" class="form-control @error('nip') is-invalid @enderror" 
                            placeholder="Masukkan Nip Dosen" value="{{ $dosen->nip ?? old('nip') }}">
                            @error('nip')
                                <div class="invalid-feedback">{{ $message }} </div>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Jenis Kelamin</label>
                        <select class="form-control" name="jenis_kelamin[]" id="">
                            <option value="L" @if($dosen->jenis_kelamin == 'L') selected @endif>LAKI-LAKI</option>
                            <option value="P" @if($dosen->jenis_kelamin == 'P') selected @endif>PEREMPUAN</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Pendidikan Dosen</label>
                        <select class="form-control" name="pendidikan[]" id="">
                            <option value="SMU" @if($dosen->pendidikan == 'SMU') selected @endif>SMU</option>
                            <option value="S1" @if($dosen->pendidikan == 'S1') selected @endif>S1</option>
                            <option value="S2" @if($dosen->pendidikan == 'S2') selected @endif>S2</option>
                            <option value="S3" @if($dosen->pendidikan == 'S3') selected @endif>S3</option>
                        </select>
                    </div> 
                    <div class="form-group">
                            <label>No Telpon</label>
                            <input type="number" name="no_telpon[]" class="form-control @error('no_telpon') is-invalid @enderror" 
                            placeholder="Masukkan No Telpon" value="{{ $dosen->no_telpon ?? old('no_telpon') }}">
                            @error('no_telpon')
                                <div class="invalid-feedback">{{ $message }} </div>
                            @enderror
                    </div>
                    <div class="form-group mb-4 mt-3">
                        <label for="exampleFormControlFile1">Foto Dosen</label>
                        <input type="file" name="foto_dosen" class="form-control-file" id="exampleFormControlFile1">
                        <div class="form-group mt-2" style="max-width: 20rem;">
                            <img width="300" src="{{ asset($dosen->foto_dosen) }}" alt="">
                        </div>
                    </div>
                    <div class="form-group mb-4">
                        <label for="exampleFormControlTextarea1">Alamat</label>
                        <textarea name="alamat[]" class="form-control" id="exampleFormControlTextarea1"
                            rows="3">{!! $dosen->alamat !!}</textarea>
                    </div>
                    <button type="submit" class="btn btn-primary mt-3">Update</button>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('js')
<script>
    CKEDITOR.replace( 'alamat[]' );
</script>
@endsection