 @extends('layouts.cork')

@section('content')
    <div id="tableHover" class="col-lg-12 col-12 layout-spacing">
        <div class="statbox widget box box-shadow">
            <div class="widget-header">
                <div class="row">
                    <div class="col-xl-3 col-md-3 col-sm-3 col-12">
                        <h4>Data Dosen</h4>
                    </div>
                    <div class="col-xl-3 col-md-3 col-sm-3 col-12 text-right">
                        <div class="form-group mt-3">
                            <select class="form-control" id="">
                                <option value="">Filter Tahun Akademik  Agustusan</option>
                                <option value="1">2020</option>
                                <option value="2">2021</option>
                                <option value="3">2022</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-3 col-sm-3 col-12 text-right">
                        <div class="form-group mt-3">
                            <select class="form-control" id="">
                                <option value="">Filter Kampus</option>
                                <option value="1">UTY</option>
                                <option value="2">UGM</option>
                                <option value="3">UAD</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-6 col-sm-6 col-12 text-right">
                        <a class="btn btn-outline-primary mt-3 mr-3" href="{{ route('dosen.create') }}">Tambah Data Dosen</a>
                    </div>
                </div>
            </div>
            <div class="widget-content widget-content-area">
                <div class="table-responsive">
                    <table class="table table-bordered table-hover mb-4">
                        <thead>
                            <tr>
                                <th class="text-center">No</th>
                                <th class="text-center">Nama Kampus</th>
                                <th class="text-center">Foto</th>
                                <th class="text-center">Nama Dosen</th>
                                <th class="text-center">NIP</th>
                                <th class="text-center">Jenis Kelamin</th>
                                <th class="text-center">Pendidikan</th>
                                <th class="text-center">Email</th>
                                <th class="text-center">No Telpon</th>
                                <th class="text-center">Alamat</th>
                                <th class="text-center">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($data as $item => $dosen)
                            <tr class="text-center"> 
                                <td>{{ $data->firstItem() + $item }} </td>
                                <td>{{ $dosen->kampus['nama_kampus'] }} </td>
                                <td>
                                    <img width="100px" src="{{ asset($dosen->foto_dosen) }}" alt="">
                                </td>
                                <td>{{ $dosen->user['name'] }} </td>
                                <td>{{ $dosen->nip }} </td>
                                <td>{{ $dosen->jenis_kelamin }} </td>
                                <td>{{ $dosen->pendidikan }} </td>
                                <td>{{ $dosen->email }} </td>
                                <td>{{ $dosen->no_telpon }} </td>
                                <td>{!! $dosen->alamat !!} </td>
                                <td class="text-center">
                                    <a href="{{ route('dosen.show', $dosen) }}">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                            fill="none" stroke="#000000" stroke-width="2" stroke-linecap="round"
                                            stroke-linejoin="round">
                                            <path d="M1 12s4-8 11-8 11 8 11 8-4 8-11 8-11-8-11-8z"></path>
                                            <circle cx="12" cy="12" r="3"></circle>
                                        </svg>
                                    </a>
                                    <a href="{{ route('dosen.edit', $dosen) }}">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                            fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                            stroke-linejoin="round" class="feather feather-edit-2">
                                            <path d="M17 3a2.828 2.828 0 1 1 4 4L7.5 20.5 2 22l1.5-5.5L17 3z"></path>
                                        </svg>
                                    </a>
                                    <form action="{{ route('dosen.destroy', $dosen) }}" method="POST"
                                        onsubmit="return confirm('Hapus Data, Anda Yakin ?')">
                                        {!! method_field('delete') . csrf_field() !!}
                                        <button class="dropdown-item" type="submit">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                                                stroke-linecap="round" stroke-linejoin="round"
                                                class="feather feather-trash-2 icon">
                                                <polyline points="3 6 5 6 21 6"></polyline>
                                                <path
                                                    d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2">
                                                </path>
                                            </svg>
                                        </button>
                                    </form>
                                </td>
                            </tr>
                            @empty
                            @endforelse
                        </tbody>
                    </table>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    {{ $data->links() }}  
                </div>
            </div>
        </div>
    </div>
@endsection
