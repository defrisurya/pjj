@extends('layouts.cork')

@section('content')
    <div id="flStackForm" class="col-lg-12 layout-spacing layout-top-spacing">
        <div class="statbox widget box box-shadow col-md-12">
            <div class="widget-header">
                <div class="row">
                    <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                        <h4>Tambah Data Fakultas</h4>
                    </div>
                </div>
            </div>
            <div class="widget-content widget-content-area">
                <form action="{{ route('fakultas.store') }}" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label>Nama Kampus</label>
                        <select class="form-control  basic" name="kampus_id">
                            <option value="">Pilih kampus</option>
                            @forelse ($kampus as $item)
                                <option value="{{ $item->id }}">{{ $item->nama_kampus }}</option>
                            @empty
                            @endforelse
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Nama Fakultas</label>
                        <input type="text" name="nama_fakultas" class="form-control @error('nama_fakultas') is-invalid @enderror" 
                        placeholder="Masukkan Nama Fakultas" value="{{ old('nama_fakultas') }}">
                        @error('nama_fakultas')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label>Akreditasi Fakultas</label>
                        <select class="form-control" name="akreditasi_fakultas">
                            <option value="">Pilih Akreditasi</option>
                            <option value="A">A</option>
                            <option value="B">B</option>
                            <option value="C">C</option>
                        </select>
                    </div>
                    <button type="submit" class="btn btn-primary mt-3">Simpan</button>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script>
        CKEDITOR.replace('tahapan');
    </script>
@endsection
