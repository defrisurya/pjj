@extends('layouts.cork')

@section('content')
<div id="flStackForm" class="col-lg-12 layout-spacing layout-top-spacing">
    <div class="statbox widget box box-shadow col-md-12">
        <div class="widget-header">                                
            <div class="row">
                <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                    <h4>Edit Data Fakultas</h4>
                </div>                                                                        
            </div>
        </div>
        <div class="widget-content widget-content-area">
            <form action="{{ route('fakultas.update', $fakultas->id ) }}" method="POST" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="form-group">
                    <label>Nama Kampus</label>
                    <select class="form-control basic" name="kampus_id">
                        <option value="">Pilih kampus</option>
                        @forelse ($kampus as $item)
                            <option {{ $fakultas->kampus_id == $item->id ? 'selected' : '' }}
                                value="{{ $item->id }}">{{ $item->nama_kampus }}</option>
                        @empty
                        @endforelse
                    </select>
                </div>
                <div class="form-group">
                    <label>Nama Fakultas</label>
                    <input type="text" name="nama_fakultas" class="form-control" placeholder="Masukkan Nama"
                        value="{{ $fakultas->nama_fakultas ?? old('nama_fakultas') }}">
                </div>
                <div class="form-group">
                    <label>Akreditasi Fakultas</label>
                    <select name="akreditasi_fakultas" class="form-control">
                        <option value="A" @if($fakultas->akreditasi_fakultas == 'A') selected @endif>A</option>
                        <option value="B" @if($fakultas->akreditasi_fakultas == 'B') selected @endif>B</option>
                        <option value="C" @if($fakultas->akreditasi_fakultas == 'C') selected @endif>C</option>
                    </select>
                </div>
                <button type="submit" class="btn btn-primary mt-3">Update</button>
            </form>
        </div>
    </div>
</div>
@endsection

@section('js')

<script>
    var ss = $(".basic").select2({
        tags: true,
    });
</script>


<script>
    CKEDITOR.replace( 'editor1' );
</script>
@endsection