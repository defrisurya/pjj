@extends('layouts.cork')

@section('content')
    <div id="flStackForm" class="col-lg-12 layout-spacing layout-top-spacing">
        <div class="statbox widget box box-shadow col-md-12">
            <div class="widget-header">
                <div class="row">
                    <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                        <h4>Detail Mahasiswa</h4>
                    </div>
                </div>
            </div>
            <div class="widget-content widget-content-area">
                <form action="{{-- {{ route('student.update', 1) }} --}}" method="POST" enctype="multipart/form-data">
                    <div class="form-group-inner">
                        <label>No Induk Mahasiswa (NIM)</label>
                        <input type="number" class="form-control" 
                         value="{{ $student->no_induk_mahasiswa ?? old('no_induk_mahasiswa') }}" readonly>
                    </div>
                    <div class="form-group-inner">
                        <label>No Induk Kependudukan (NIK)</label>
                        <input type="number" class="form-control" 
                         value="{{ $student->no_induk_kependudukan ?? old('no_induk_kependudukan') }}" readonly>
                    </div>
                    <div class="form-group-inner">
                        <label>Nama Lengkap Mahasiswa</label>
                        <input type="text" class="form-control" 
                         value="{{ $student->nama_lengkap ?? old('nama_lengkap') }}" readonly>
                    </div>
                    <div class="form-group-inner">
                        <label>Jenis Kelamin</label>
                        <input type="text" class="form-control" 
                         value="{{ $student->jenis_kelamin ?? old('jenis_kelamin') }}" readonly>
                    </div>
                    {{-- <div class="form-group form-select-list">
                        <label>Jenis Kelamin</label>
                        <select class="form-control custom-select-value">
                            <option value="">-- Pilih Jenis Kelamin --</option>
                            <option value="Laki-Laki">Laki-Laki</option>
                            <option value="Perempuan">Perempuan</option>
                        </select>
                    </div> --}}
                    {{-- <div class="form-group form-select-list">
                        <label>Program Studi</label>
                        <select class="form-control custom-select-value">
                            <option value="">-- Pilih Program Studi --</option>
                            <option value="Sistem Informasi">Sistem Informasi</option>
                            <option value="Teknik Informatika">Teknik Informatika</option>
                            <option value="Manajemen Informatika">Manajemen Informatika</option>
                        </select>
                    </div> --}}
                    <div class="form-group">
                        <label>Program Studi</label>
                        <input type="text" name="agama" id="agama" class="form-control" value="{{ $student->prodi['nama_prodi'] }}" readonly/>
                    </div>
                    <div class="form-group">
                        <label>Agama</label>
                        <input type="text" name="agama" id="agama" class="form-control" value="{{ $student->agama ?? old('agama') }}" readonly/>
                    </div>
                    {{-- <div class="form-group">
                        <label>Kewarganegaraan</label>
                        <input type="text" name="kewarganegaraan" id="kewarganegaraan" class="form-control" value="{{ $student->kewarganegaraan ?? old('kewarganegaraan') }}" readonly/>
                    </div> --}}
                    <div class="form-group">
                        <label>Tempat Lahir</label>
                        <input type="text" name="tempat_lahir" id="tempat_lahir" class="form-control" value="{{ $student->tempat_lahir ?? old('tempat_lahir') }}" readonly/>
                    </div>
                    <div class="form-group">
                        <label>Tanggal Lahir</label>
                        <input type="date" name="tanggal_lahir" id="tanggal_lahir" class="form-control" value="{{ $student->tanggal_lahir ?? old('tanggal_lahir') }}" readonly/>
                    </div>
                    <div class="form-group">
                        <label>E-mail</label>
                        <input type="text" name="email" id="email" class="form-control" value="{{ $student->email ?? old('email') }}" readonly/>
                    </div>
                    <div class="form-group">
                        <label>Nomor Handphone</label>
                        <input type="number" name="no_telpon" id="no_telpon" class="form-control" value="{{ $student->no_telpon ?? old('no_telpon') }}" readonly/>
                    </div>
                    <div class="form-group">
                        <label>Asal Sekolah</label>
                        <input type="text" name="asal_sekolah" id="asal_sekolah" class="form-control" value="{{ $student->asal_sekolah ?? old('asal_sekolah') }}" readonly/>
                    </div>
                    <div class="form-group">
                        <label>Penerima KPS (Kartu Perlindungan Sosial)</label>
                        <input type="text" name="penerima_kps" id="penerima_kps" class="form-control" value="{{ $student->penerima_kps ?? old('penerima_kps') }}" readonly/>
                    </div>
                    <div class="form-group">
                        <label>Penerima KPS (YA/TIDAK)</label>
                        <input type="text" name="penerima_kps" id="penerima_kps" class="form-control" value="{{ $student->penerima_kps ?? old('penerima_kps') }}" readonly/>
                    </div>
                    {{-- <div class="form-group">
                        <label>Penerima KPS (Kartu Perlindungan Sosial)</label>
                        <div class="bt-df-checkbox">
                            <label>
                                <input class="pull-left radio-checked" type="radio" checked="" value="Ya"
                                    id="optionsRadios1" name="optionsRadios">
                                YA</label>
                        </div>
                        <div class="bt-df-checkbox">
                            <label>
                                <input class="pull-left" type="radio" value="Tidak" id="optionsRadios2"
                                    name="optionsRadios"> TIDAK</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Penerima KPS (YA/TIDAK)</label>
                        <input type="text" name="penerima_kps" id="penerima_kps" class="form-control" />
                    </div> --}}
                    <div class="form-group">
                        <label>Alamat</label>
                        <textarea name="alamat" id="alamat" class="form-control" readonly> {{ $student->alamat ?? old('alamat') }}</textarea>
                    </div>
                    {{-- <div class="form-group">
                        <label>Provinsi</label>
                        <input type="text" name="provinsi" class="form-control" value="{{ $student->provinces['name'] }}"  readonly>
                    </div> --}}
                    {{-- <div class="form-group">
                        <label>Kabupaten</label>
                        <input type="text" name="" id="" class="form-control" value="{{ $student->regencies['name'] }}" readonly>
                    </div> --}}
                    <div class="form-group-inner">
                        <label>Kecamatan</label>
                        <input type="text" name="" id="" class="form-control" value="{{ $student->districts['name'] }}" readonly>
                    </div>
                    {{-- <div class="form-group-inner">
                        <label>Desa</label>
                        <input type="text" name="" id="" class="form-control" value="{{ $student->villages['name'] }}" readonly>
                    </div> --}}
                    {{-- <div class="form-group">
                        <label>Kode Pos</label>
                        <input type="number" name="kode_pos" id="kode_pos" class="form-control" value="{{ $student->kode_pos ?? old('kode_pos') }}" readonly/>
                    </div> --}}
                    <div class="breadcome-list">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <ul class="breadcome-heading">
                                    <li>
                                        <span class="nama">
                                            Biodata Ayah Kandung
                                        </span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Nomor Induk Kependudukan (NIK) Ayah</label>
                        <input type="number" name="no_induk_kependudukan_ayah" id="no_induk_kependudukan_ayah" class="form-control" value="{{ $student->no_induk_kependudukan_ayah ?? old('no_induk_kependudukan_ayah') }}" readonly/>
                    </div>
                    <div class="form-group">
                        <label>Nama Lengkap Ayah</label>
                        <input type="text" name="nama_ayah" id="nama_ayah" class="form-control" value="{{ $student->nama_lengkap_ayah ?? old('nama_ayah') }}" readonly/>
                    </div>
                    <div class="form-group">
                        <label>Tanggal Lahir Ayah</label>
                        <input type="date" name="tanggal_lahir_ayah" id="tanggal_lahir_ayah" class="form-control" value="{{ $student->tanggal_lahir_ayah ?? old('tanggal_lahir_ayah') }}" readonly/>
                    </div>
                    <div class="form-group">
                        <label>Pendidikan Terakhir Ayah</label>
                        <input type="text" name="pendidikan_ayah" id="pendidikan_ayah" class="form-control" value="{{ $student->pendidikan_ayah ?? old('pendidikan_ayah') }}" readonly/>
                    </div>
                    {{-- <div class="form-group form-select-list">
                        <label>Pendidikan Terakhir</label>
                        <select class="form-control custom-select-value">
                            <option value="">-- Pilih Pendidikan Terakhir --</option>
                            <option value="SD">SD</option>
                            <option value="SMP">SMP</option>
                            <option value="SMA">SMA</option>
                            <option value="Sarjana">Sarjana</option>
                            <option value="Diploma">Diploma</option>
                        </select>
                    </div> --}}
                    {{-- <div class="form-group form-select-list">
                        <label>Pekerjaan</label>
                        <select class="form-control custom-select-value">
                            <option value="">-- Pilih Pekerjaan --</option>
                            <option value="Buruh">Buruh</option>
                            <option value="Wiraswasta">Wiraswasta</option>
                            <option value="PNS">PNS</option>
                        </select>
                    </div> --}}
                    <div class="form-group">
                        <label>Pekerjaan Ayah</label>
                        <input type="text" name="pekerjaan_ayah" id="pekerjaan_ayah" class="form-control" value="{{ $student->pekerjaan_ayah ?? old('pekerjaan_ayah') }}" readonly/>
                    </div>
                    <div class="form-group">
                        <label>Rentang Penghasilan Ayah</label>
                        <input type="text" name="rentang_penghasilan_ayah" id="rentang_penghasilan_ayah" class="form-control" value="{{ $student->rentang_penghasilan_ayah ?? old('rentang_penghasilan_ayah') }}" readonly/>
                    </div>
                    
                    <div class="form-group">
                        <label>Nomor Han Phone Ayah</label>
                        <input type="text" name="no_telpon_ayah" id="no_telpon_ayah" class="form-control" value="{{ $student->no_telpon_ayah ?? old('no_telpon_ayah') }}" readonly/>
                    </div>
                    <div class="form-group">
                        <label>Alamat Ayah</label>
                        <textarea name="alamat_ayah" id="alamat_ayah" class="form-control" readonly> {{ $student->alamat_ayah ?? old('alamat_ayah') }}</textarea>
                    </div>
                    
                    <div class="breadcome-list">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <ul class="breadcome-heading">
                                    <li>
                                        <span class="nama">
                                            Biodata Ibu Kandung
                                        </span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Nomor Induk Kependudukan (NIK) Ibu</label>
                        <input type="number" name="no_induk_kependudukan_ibu" id="no_induk_kependudukan_ibu" class="form-control" value="{{ $student->no_induk_kependudukan_ibu ?? old('no_induk_kependudukan_ibu') }}" readonly/>
                    </div>
                    <div class="form-group">
                        <label>Nama Lengkap Ibu</label>
                        <input type="text" name="nama_ibu" id="nama_ibu" class="form-control" value="{{ $student->nama_lengkap_ibu ?? old('nama_ibu') }}" readonly/>
                    </div>
                    <div class="form-group">
                        <label>Tanggal Lahir Ibu</label>
                        <input type="date" name="tanggal_lahir_ibu" id="tanggal_lahir_ibu" class="form-control" value="{{ $student->tanggal_lahir_ibu ?? old('tanggal_lahir_ibu') }}" readonly/>
                    </div>
                    <div class="form-group">
                        <label>Pendidikan Terakhir Ibu</label>
                        <input type="text" name="pendidikan_ibu" id="pendidikan_ibu" class="form-control" value="{{ $student->pendidikan_ibu ?? old('pendidikan_ibu') }}" readonly/>
                    </div>
                    <div class="form-group">
                        <label>Pekerjaan Ibu</label>
                        <input type="text" name="pekerjaan_ibu" id="pekerjaan_ibu" class="form-control" value="{{ $student->pekerjaan_ibu ?? old('pekerjaan_ibu') }}" readonly/>
                    </div>
                    <div class="form-group">
                        <label>Rentang Penghasilan Ibu</label>
                        <input type="text" name="rentang_penghasilan_ibu" id="rentang_penghasilan_ibu" class="form-control" value="{{ $student->rentang_penghasilan_ibu ?? old('rentang_penghasilan_ibu') }}" readonly/>
                    </div>
                    
                    <div class="form-group">
                        <label>Nomor Han Phone Ibu</label>
                        <input type="text" name="no_telpon_ibu" id="no_telpon_ibu" class="form-control" value="{{ $student->no_telpon_ibu ?? old('no_telpon_ibu') }}" readonly/>
                    </div>
                    <div class="form-group">
                        <label>Alamat Ibu</label>
                        <textarea name="alamat_ayah" id="alamat_ibu" class="form-control" readonly> {{ $student->alamat_ibu ?? old('alamat_ibu') }}</textarea>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
