@extends('layouts.cork')

@section('content')
    <div id="tableHover" class="col-lg-12 col-12 layout-spacing">
        <div class="statbox widget box box-shadow">
            <div class="widget-header">
                <div class="row">
                    <div class="col-xl-3 col-md-6 col-sm-6 col-12">
                        <h4>Daftar Mahasiswa</h4>
                    </div>
                    <div class="col-xl-3 col-md-6 col-sm-6 col-12 text-right">
                        <div class="form-group mt-3">
                            <select class="form-control" id="">
                                <option value="">Filter Tahun Akademik</option>
                                <option value="1">2020</option>
                                <option value="2">2021</option>
                                <option value="3">2022</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-6 col-sm-6 col-12 text-right">
                        <div class="form-group mt-3">
                            <select class="form-control" id="">
                                <option value="">Filter Kampus</option>
                                <option value="1">UTY</option>
                                <option value="2">UGM</option>
                                <option value="3">UAD</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-6 col-sm-6 col-12 text-right">
                        <div class="form-group mt-3">
                            <select class="form-control" id="">
                                <option value="">Filter Prodi</option>
                                <option value="1">Prodi 1</option>
                                <option value="2">Prodi 2</option>
                                <option value="3">Prodi 3</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="widget-content widget-content-area">
                <div class="table-responsive">
                    <table class="table table-bordered table-hover mb-4">
                        <thead>
                            <tr>
                                <th class="text-center">No</th>
                                <th class="text-center">Nama Kampus</th>
                                <th class="text-center">Nama Mahasiswa</th>
                                <th class="text-center">No Induk Mhs</th>
                                <th class="text-center">Asal Sekolah</th>
                                <th class="text-center">Prodi</th>
                                <th class="text-center">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($data as $item => $student)
                                <tr> 
                                    <td align="center"> {{ $data->firstItem() + $item }} </td>
                                    <td align="center"> {{ $student->kampus['nama_kampus'] }}  </td>
                                    <td align="center"> {{ $student->nama_lengkap }} </td>
                                    <td align="center"> {{ $student->no_induk_mahasiswa }} </td>
                                    <td align="center"> {{ $student->asal_sekolah }} </td>
                                    <td align="center"> {{ $student->prodi['nama_prodi'] }} </td>
                                    <td class="text-center">
                                        <a href="{{ route('student.show', $student) }}">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                                fill="none" stroke="#000000" stroke-width="2" stroke-linecap="round"
                                                stroke-linejoin="round">
                                                <path d="M1 12s4-8 11-8 11 8 11 8-4 8-11 8-11-8-11-8z"></path>
                                                <circle cx="12" cy="12" r="3"></circle>
                                            </svg>
                                        </a>
                                        <form action="{{ route('student.destroy', $student) }}" method="POST"
                                            onsubmit="return confirm('Hapus Data, Anda Yakin ?')">
                                            {!! method_field('delete') . csrf_field() !!}
                                            <button class="dropdown-item" type="submit">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                    viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                                                    stroke-linecap="round" stroke-linejoin="round"
                                                    class="feather feather-trash-2 icon">
                                                    <polyline points="3 6 5 6 21 6"></polyline>
                                                    <path
                                                        d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2">
                                                    </path>
                                                </svg>
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                            @empty
                            @endforelse
                        </tbody>
                    </table>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    {{ $data->links() }}  
                </div>
            </div>
        </div>
    </div>
@endsection



