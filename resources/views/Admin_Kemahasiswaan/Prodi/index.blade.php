@extends('layouts.cork')

@section('content')
    <div id="tableHover" class="col-lg-12 col-12 layout-spacing">
        <div class="statbox widget box box-shadow">
            <div class="widget-header">
                <div class="row">
                    <div class="col-xl-6 col-md-6 col-sm-6 col-12">
                        <h4>DATA PRODI</h4>
                    </div>
                    <div class="col-xl-6 col-md-6 col-sm-6 col-12 text-right">
                        <a class="btn btn-outline-primary mt-3 mr-3" href="{{ route('prodi.create') }}">Tambah Data Prodi</a>
                    </div>
                </div>
            </div>
            <div class="widget-content widget-content-area">
                <div class="table-responsive">
                    <table class="table table-bordered table-hover mb-4">
                        <thead>
                            <tr>
                                <th class="text-center">No</th>
                                <th class="text-center">Nama Kampus</th>
                                <th class="text-center">Nama Fakultas</th>
                                <th class="text-center">Kode Prodi</th>
                                <th class="text-center">Nama Prodi</th>
                                <th class="text-center">Profil Prodi</th>
                                <th class="text-center">Deskripsi Prodi</th>
                                <th class="text-center">Biaya Prodi</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($data as $item => $prodi)
                                <tr class="text-center">
                                    <td>{{ $data->firstItem() + $item }}</td>
                                    <td>{{ $prodi->kampus['nama_kampus'] }}</td>
                                    <td>{{ $prodi->fakultas['nama_fakultas'] }}</td>
                                    <td>{{ $prodi->kode_prodi }}</td>
                                    <td>{{ $prodi->nama_prodi }}</td>
                                    <td>{!! $prodi->profile_prodi !!}</td>
                                    <td>{!! $prodi->deskripsi_prodi !!}</td>
                                    <td>{{ $prodi->biaya_prodi }}</td>
                                    <td class="text-center">
                                        <a href="{{ route('prodi.edit', $prodi) }}">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                                                stroke-linecap="round" stroke-linejoin="round"
                                                class="feather feather-edit-2">
                                                <path d="M17 3a2.828 2.828 0 1 1 4 4L7.5 20.5 2 22l1.5-5.5L17 3z"></path>
                                            </svg>
                                        </a>
                                        <form action="{{ route('prodi.destroy', $prodi) }}" method="POST"
                                            onsubmit="return confirm('Hapus Data, Anda Yakin ?')">
                                            {!! method_field('delete') . csrf_field() !!}
                                            <button class="dropdown-item" type="submit">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                    viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                                                    stroke-linecap="round" stroke-linejoin="round"
                                                    class="feather feather-trash-2 icon">
                                                    <polyline points="3 6 5 6 21 6"></polyline>
                                                    <path
                                                        d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2">
                                                    </path>
                                                </svg>
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                            @empty
                            @endforelse
                        </tbody>
                    </table>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    {{ $data->links() }}  
                </div>
                
            </div>
        </div>
    </div>
@endsection
