@extends('layouts.cork')

@section('content')
    <div id="flStackForm" class="col-lg-12 layout-spacing layout-top-spacing">
        <div class="statbox widget box box-shadow col-md-12">
            <div class="widget-header">
                <div class="row">
                    <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                        <h4>Tambah Data Prodi</h4>
                    </div>
                </div>
            </div>
            <div class="widget-content widget-content-area">
                <form action="{{ route('prodi.store') }}" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label>Nama Kampus</label>
                        <select class="form-control  basic" name="kampus_id" required>
                            <option value="">Pilih kampus</option>
                            @foreach ($kampus as $item)
                                <option value="{{ $item->id }}">{{ $item->nama_kampus }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Nama Fakultas</label>
                        <select class="form-control basic" name="fakultas_id" required>
                            <option value="">Pilih Fakultas</option>
                            @foreach ($fakultas as $item)
                                <option value="{{ $item->id }}">{{ $item->nama_fakultas }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Jenjang Pendidikan</label>
                        <select class="form-control basic" name="jenjang_pend_id" required>
                            <option value="">Pilih Jenjang Pendidikan</option>
                            @foreach ($jenj_pendidikan as $item)
                                <option value="{{ $item->id }}">{{ $item->jenjang_pend }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group mb-4">
                        <label for="exampleFormControlTextarea1">Kode Prodi</label>
                        <input type="text" name="kode_prodi"
                            class="form-control @error('kode_prodi') is-invalid @enderror" placeholder="Masukkan Kode Prodi"
                            value="{{ old('kode_prodi') }}">
                        @error('kode_prodi')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group mb-4">
                        <label for="exampleFormControlTextarea1">Nama Prodi</label>
                        <input type="text" name="nama_prodi"
                            class="form-control @error('nama_prodi') is-invalid @enderror" placeholder="Masukkan Nama Prodi"
                            value="{{ old('nama_prodi') }}">
                        @error('nama_prodi')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label>Akreditasi Prodi</label>
                        <select class="form-control" name="akreditasi_prodi">
                            <option value="">Pilih Akreditasi Prodi</option>
                            <option value="A">A</option>
                            <option value="B">B</option>
                            <option value="C">C</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Biaya Per Semester</label>
                        <input type="number" name="biaya_prodi"
                            class="form-control @error('biaya_prodi') is-invalid @enderror"
                            placeholder="Masukkan Biaya Prodi" value="{{ old('biaya_prodi') }}">
                        @error('biaya_prodi')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group mb-4">
                        <label for="exampleFormControlTextarea1">Deskripsi Prodi</label>
                        <textarea name="deskripsi_prodi" class="form-control" id="exampleFormControlTextarea1" rows="3">{{ old('deskripsi_prodi') }}</textarea>
                    </div>
                    <button type="submit" class="btn btn-primary mt-3">Simpan</button>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script>
        CKEDITOR.replace('deskripsi_prodi');
    </script>
@endsection
