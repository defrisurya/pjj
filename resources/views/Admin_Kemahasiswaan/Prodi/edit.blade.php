@extends('layouts.cork')

@section('content')
    <div id="flStackForm" class="col-lg-12 layout-spacing layout-top-spacing">
        <div class="statbox widget box box-shadow col-md-12">
            <div class="widget-header">
                <div class="row">
                    <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                        <h4>Edit Data Prodi</h4>
                    </div>
                </div>
            </div>
            <div class="widget-content widget-content-area">
                <form action="{{ route('prodi.update', $prodi) }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    <div class="form-group">
                        <label>Nama Kampus</label>
                        <select class="form-control basic" name="kampus_id">
                            <option value="">Pilih kampus</option>
                            @forelse ($kampus as $item)
                                <option {{ $prodi->kampus_id == $item->id ? 'selected' : '' }}
                                    value="{{ $item->id }}">{{ $item->nama_kampus }}</option>
                            @empty
                            @endforelse
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Nama Fakultas</label>
                        <select class="form-control basic" name="fakultas_id">
                            <option value="">Pilih Fakultas</option>
                            @forelse ($fakultas as $item)
                                <option {{ $prodi->fakultas_id == $item->id ? 'selected' : '' }}
                                    value="{{ $item->id }}">{{ $item->nama_fakultas }}</option>
                            @empty
                            @endforelse
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Kode Prodi</label>
                        <input type="text" name="kode_prodi" class="form-control" placeholder="Masukkan Kode Prodi"
                            value="{{ $prodi->kode_prodi ?? old('kode_prodi') }}">
                    </div>
                    <div class="form-group">
                        <label>Nama Prodi</label>
                        <input type="text" name="nama_prodi" class="form-control" placeholder="Masukkan Nama Prodi"
                            value="{{ $prodi->nama_prodi ?? old('nama_prodi') }}">
                    </div>
                    <div class="form-group">
                        <label>Akreditasi Prodi</label>
                        <select class="form-control" name="akreditasi_prodi">
                            <option value="{{ $prodi->akreditasi_prodi ?? old('akreditasi_prodi') }}">Pilih Akreditasi</option>
                            <option value="A">A</option>
                            <option value="B">B</option>
                            <option value="C">C</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Biaya Prodi</label>
                        <input type="text" name="biaya_prodi" class="form-control" 
                        placeholder="Masukkan Biaya Prodi"
                            value="{{ $prodi->biaya_prodi ?? old('biaya_prodi') }}">
                    </div>
                    <div class="form-group mb-4">
                        <label for="exampleFormControlTextarea1">Profile Prodi</label>
                        <textarea name="profile_prodi" class="form-control" id="exampleFormControlTextarea1"
                            rows="3">{{ $prodi->profile_prodi ?? old('profile_prodi') }}</textarea>
                    </div>
                    <div class="form-group mb-4">
                        <label for="exampleFormControlTextarea1">Deskripsi Prodi</label>
                        <textarea name="deskripsi_prodi" class="form-control" id="exampleFormControlTextarea1"
                            rows="3">{{ $prodi->deskripsi_prodi ?? old('deskripsi_prodi') }}</textarea>
                    </div>
                    <button type="submit" class="btn btn-primary mt-3">Update</button>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('js')
 

    <script>
        var ss = $(".basic").select2({
            tags: true,
        });
    </script>
        <script>
            CKEDITOR.replace('profile_prodi'); 
            CKEDITOR.replace('deskripsi_prodi');
        </script>
@endsection
