@extends('Front.layouts.apps')

@section('title', 'Login PMB')

@section('content')
    <!-- Header -->
    <header id="header" class="header">
        <div class="header-content-pmb">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-xl-6">
                        <div class="form">
                            <div class="container">
                                <div class="row">
                                    <h3 style="text-align: center; margin-bottom: 1.25rem;">
                                        Universitas Teknology Yogyakarta
                                    </h3>
                                    <div class="col-lg-12">
                                        <div class="text-container">
                                            <h4 style="text-align: center; margin-bottom: 1.25rem;">
                                                Login Mahasiswa
                                            </h4>
                                            <!-- Newsletter Form -->
                                            <form id="newsletterForm" data-toggle="validator" data-focus="false">
                                                <div class="form-group">
                                                    <input type="email" class="form-control-input" id="email" required>
                                                    <label class="label-control" for="email">Alamat Email</label>
                                                    <div class="help-block with-errors"></div>
                                                </div>
                                                <div class="form-group">
                                                    <input type="password" class="form-control-input" id="password"
                                                        required>
                                                    <label class="label-control" for="password">Password</label>
                                                    <div class="help-block with-errors"></div>
                                                </div>
                                                {{-- <div class="form-group checkbox">
                                <input type="checkbox" id="nterms" value="Agreed-to-Terms" required>I've read and agree to
                                Tivo's written <a href="privacy-policy.html">Privacy Policy</a> and <a
                                    href="terms-conditions.html">Terms Conditions</a>
                                <div class="help-block with-errors"></div>
                            </div> --}}
                                                <div class="form-group">
                                                    <button type="submit" class="form-control-submit-button">
                                                        Login
                                                    </button>
                                                </div>
                                                <div class="form-message">
                                                    <div id="nmsgSubmit" class="h3 text-center hidden"></div>
                                                </div>
                                            </form>
                                            <!-- end of newsletter form -->
                                        </div> <!-- end of text-container -->
                                    </div> <!-- end of col -->
                                </div> <!-- end of row -->
                            </div> <!-- end of container -->
                        </div> <!-- end of form -->
                        <!-- End of Regristrasi PMB -->
                        {{-- <div class="text-container">
                            <h3>Pendaftaran Mahasiswa Baru Universitas Teknology Yogyakarta</h3>
                        </div> --}}
                        <!-- end of text-container -->
                        <!-- Regristrasi PMB -->
                    </div> <!-- end of col -->
                </div> <!-- end of row -->
            </div> <!-- end of container -->
        </div> <!-- end of header-content -->
        <div class="wave"></div>
    </header> <!-- end of header -->
    <!-- end of header -->
@endsection
