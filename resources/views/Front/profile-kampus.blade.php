@extends('Front.layouts.apps')
@section('title', 'Profile Kampus')
@section('content')
    <!-- Header -->
    <header id="header" class="header">
        <div class="header-content">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-xl-5">
                        <div class="text-container">
                            {{-- @php
                                dd($data->nama_kampus);
                            @endphp --}}
                            <h3>{{ $data->nama_kampus }}</h3>
                            <p> </p>
                            <ul class="list-unstyled li-space-lg">
                                <li class="media">
                                    <div class="media-body">
                                        <strong>Akreditasi : {{ $data->akreditasi_kampus }}</strong>
                                    </div>
                                </li>
                                <li class="media">
                                    <div class="media-body">
                                        <strong>Jumlah Prodi PJJ : {{ $jmlprodi }} Prodi</strong>
                                    </div>
                                </li>
                                {{-- <li class="media">
                                    <div class="media-body">
                                        <strong>Biaya Kuliah/Semester :
                                            Rp. {{ number_format($infoPMB->biayakuliahpersemester) }}</strong>
                                    </div>
                                </li> --}}
                            </ul>
                            @if (auth()->user() == null)
                                <a class="btn-solid-reg page-scroll" href="{{ route('register') }}">Daftar PMB</a>
                            @endif
                            @if (auth()->user() != null)
                                <a class="btn-solid-reg page-scroll" href="{{ route('daftarpmb', $data->slug) }}">Daftar
                                    PMB</a>
                            @endif
                        </div> <!-- end of text-container -->
                    </div> <!-- end of col -->
                    <div class="col-lg-6 col-xl-7">
                        <div class="image-container">
                            <div class="img-wrapper">
                                <img class="img-fluid" src="{{ asset('front') }}/images/gmbrheader.png" alt="alternative">
                            </div> <!-- end of img-wrapper -->
                        </div> <!-- end of image-container -->
                    </div> <!-- end of col -->
                </div> <!-- end of row -->
            </div> <!-- end of container -->
        </div> <!-- end of header-content -->
        <div class="wave"></div>
    </header> <!-- end of header -->
    <!-- end of header -->

    <!-- Profile Kampus -->
    <div id="details" class="basic-1">
        <div class="container">
            <div class="cards-1">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <h1 class="card-title">Profile Kampus</h1>
                        </div> <!-- end of col -->
                    </div> <!-- end of row -->
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="text-container">
                        <p>
                            {!! $data->deskripsi_kampus !!}
                        </p>
                        {{-- <ul class="list-unstyled li-space-lg">
                            <li class="media">
                                <div class="media-body">
                                    1. Sekolah Tinggi Ilmu Ekonomi Yogyakarta (STIEYO)
                                </div>
                            </li>
                            <li class="media">
                                <div class="media-body">
                                    2. Akademi Bahasa Asing Yogyakarta (ABAYO)
                                </div>
                            </li>
                            <li class="media">
                                <div class="media-body">
                                    3. Sekolah Tinggi Manajemen Informatika dan Komputer (STMIK) Dharma Bangsa
                                </div>
                            </li>
                        </ul> --}}
                    </div> <!-- end of text-container -->
                </div> <!-- end of col -->
            </div> <!-- end of row -->
        </div> <!-- end of container -->
    </div> <!-- end of basic-1 -->
    <!-- end of Profile Kampus  -->

    <!-- Prodi Kampus PJJ -->
    <div id="details" class="basic-1">
        <div class="container">
            <div class="cards-1">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <h1 class="card-title">Prodi Kampus {{ $data->nama_kampus }}</h1>
                        </div> <!-- end of col -->
                    </div> <!-- end of row -->
                </div>
            </div>
            <div class="accordion" id="accordionExample">
                @foreach ($prodi as $key => $item)
                    <div class="card">
                        <div class="card-header" id="headingOne">
                            <h2 class="mb-0">
                                <button class="btn btn-link" type="button" data-toggle="collapse"
                                    data-target="#collapse{{ $key }}" aria-expanded="true"
                                    aria-controls="collapse{{ $key }}">
                                    {{ $item->nama_prodi }} ({{ $item->jenj_pend->jenjang_pend }})
                                </button>
                            </h2>
                        </div>
                        <div id="collapse{{ $key }}" class="collapse show" aria-labelledby="headingOne"
                            data-parent="#accordionExample">
                            <div class="card-body pjj">
                                {!! $item->deskripsi_prodi !!}
                                <p><b>Biaya Kuliah Per Semester : Rp. {{ number_format($item->biaya_prodi) }}</b></p>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div> <!-- end of container -->
    </div> <!-- end of basic-1 -->
    <!-- end of Prodi Kampus PJJ  -->

    <!-- Manfaat Dan Keunggulan -->
    <div id="details" class="basic-1">
        <div class="container">
            <div class="cards-1">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <h1 class="card-title">Info PMB {{ $data->nama_kampus }}</h1>
                        </div> <!-- end of col -->
                    </div> <!-- end of row -->
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <!-- Tabs Content -->
                    <div class="tab-content" id="argoTabsContent">
                        <!-- Tab -->
                        <div class="tab-pane fade show active" id="tab-1" role="tabpanel" aria-labelledby="tab-1">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="image-container">
                                        <img class="img-fluid" src="{{ asset('front') }}/images/features-1.png"
                                            alt="alternative">
                                    </div> <!-- end of image-container -->
                                </div> <!-- end of col -->
                                <div class="col-lg-6">
                                    <div class="text-container">
                                        <h5>Pendaftaran Mahasiswa Baru Tahun Akademik {{ $infoPMB->tahun_akademik }}</h5>
                                        <p>
                                            <strong>Waktu Pendaftaran :</strong>
                                            {{ Carbon\Carbon::parse($infoPMB->waktupendaftaran_start)->translatedFormat('d F Y') }}
                                            -
                                            {{ Carbon\Carbon::parse($infoPMB->waktupendaftaran_end)->translatedFormat('d F Y') }}
                                        </p>
                                        <p>
                                            <strong>Biaya Registrasi :</strong> Rp.
                                            {{ number_format($infoPMB->biayapendaftaran) }}
                                        </p>
                                        <h5>Tahapan Seleksi</h5>
                                        <ul class="list-unstyled li-space-lg">
                                            <li class="media">
                                                <i class="fas fa-check-circle"></i>
                                                <div class="media-body">
                                                    Registrasi Account
                                                </div>
                                            </li>
                                            <li class="media">
                                                <i class="fas fa-check-circle"></i>
                                                <div class="media-body">
                                                    Isi Data
                                                </div>
                                            </li>
                                            <li class="media">
                                                <i class="fas fa-check-circle"></i>
                                                <div class="media-body">
                                                    Informasi Hasil
                                                </div>
                                            </li>
                                            <li class="media">
                                                <i class="fas fa-check-circle"></i>
                                                <div class="media-body">
                                                    Pembayaran Registrasi
                                                </div>
                                            </li>
                                        </ul>
                                    </div> <!-- end of text-container -->
                                </div> <!-- end of col -->
                            </div> <!-- end of row -->
                        </div> <!-- end of tab-pane -->
                        <!-- end of tab -->
                    </div> <!-- end of tab content -->
                    <!-- end of tabs content -->
                </div> <!-- end of col -->
            </div> <!-- end of row -->
        </div> <!-- end of container -->
    </div> <!-- end of tabs -->
    <!-- end of Manfaat Dan Keunggulan -->
@endsection
