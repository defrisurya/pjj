@extends('Front.layouts.app')
@section('title', 'Daftar Semua Kampus')
@section('content')
    <!-- Header -->
    <header id="header" class="header">
        <div class="header-content">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-xl-5">
                        <div class="text-container">
                            <h2>Digital Platform distance learning for all students activity</h2>
                        </div> <!-- end of text-container -->
                    </div> <!-- end of col -->
                    <div class="col-lg-6 col-xl-7">
                        <div class="image-container">
                            <div class="img-wrapper">
                                <img class="img-fluid" src="{{ asset('front') }}/images/gmbrheader.png"
                                    alt="alternative">
                            </div> <!-- end of img-wrapper -->
                        </div> <!-- end of image-container -->
                    </div> <!-- end of col -->
                </div> <!-- end of row -->
            </div> <!-- end of container -->
        </div> <!-- end of header-content -->
        <div class="wave"></div>
    </header> <!-- end of header -->
    <!-- end of header -->

    <!-- Daftar Kampus -->
    <div id="pricing" class="cards-2">
        <div class="container">
            <div class="row">
                <div class="col-lg-7">
                    <h2 class="h2-heading">Daftar Semua Kampus</h2>
                </div> <!-- end of col -->
                <div class="col-lg-5">
                    <div class="sparkline10-graph">
                        <div class="basic-login-form-ad">
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="basic-login-inner inline-basic-form">
                                        <form action="#">
                                            <div class="form-group-inner">

                                                <div class="row">
                                                    <form action="/dashboard">
                                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                            <input type="text" name="cari" value="{{ request('cari') }}" class="form-control basic-ele-mg-b-10 responsive-mg-b-10" placeholder="Cari Kampus" />
                                                        </div>
                                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                            <div class="login-btn-inner">
                                                                <div class="login-horizental lg-hz-mg">
                                                                    <form class="d-flex" action="{{ URL::current() }}" method="GET">
                                                                        <button class="btn btn-solid-reg" type="submit">Cari</button>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>

                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> <!-- end of col -->
            </div> <!-- end of row -->
            <div class="row">
                <!-- Card-->
                @foreach ($kampus as $p)
                <div class="col-md-3">
                    <a class="link-kampus" href="{{ route('profilkampus', $p->id) }}">
                    <div class="card">
                        <div class="card-body daftar-kampus">
                            <div class="price">
                                    <img class="img-fluid" src="{{ asset('front') }}/images/features-1.png"
                                        alt="alternative">
                            </div>
                                <div class="card-title"> {{ $p->nama_kampus }} </div>
                            <p class="text-xs font-weight-bold mb-0">
                                {!! \Illuminate\Support\Str::limit($p->deskripsi_kampus, 30, $end='...') !!}
                            </p>
                        </div>
                    </div> <!-- end of card -->
                </div> 
            </a><!-- end of col -->
            <!-- end of card -->
            @endforeach
            </div> <!-- end of row -->

            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                {{ $kampus->links() }}  
            </div>
            
         
        </div> <!-- end of container -->
    </div> <!-- end of cards-2 -->
    <!-- end of Daftar Kampus -->
@endsection
