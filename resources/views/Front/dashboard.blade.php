@extends('Front.layouts.app')

@section('title', 'Kampusku')

@section('content')
    <!-- Header -->
    <header id="header" class="header">
        <div class="header-content">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-xl-5">
                        <div class="text-container">
                            <h2>Digital Platform distance learning for all students activity</h2>
                        </div> <!-- end of text-container -->
                    </div> <!-- end of col -->
                    <div class="col-lg-6 col-xl-7">
                        <div class="image-container">
                            <div class="img-wrapper">
                                <img class="img-fluid" src="{{ asset('front') }}/images/gmbrheader.png" alt="alternative">
                            </div> <!-- end of img-wrapper -->
                        </div> <!-- end of image-container -->
                    </div> <!-- end of col -->
                </div> <!-- end of row -->
            </div> <!-- end of container -->
        </div> <!-- end of header-content -->
        <div class="wave"></div>
    </header> <!-- end of header -->
    <!-- end of header -->

    <!-- Features -->
    <div class="cards-1">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="card-title">Features</h1>
                    <p>
                        Layanan yang kami berikan dalam menunjang belajar online
                    </p>
                </div> <!-- end of col -->
            </div> <!-- end of row -->
            <div class="row">
                <div class="col-lg-3">
                    <!-- Card -->
                    <div class="card">
                        <div class="card-image">
                            <img class="img-fluid" src="{{ asset('front') }}/images/responsive.png" alt="alternative">
                        </div>
                        <div class="card-body">
                            <h4 class="card-title">Responsive</h4>
                            <p>Layanan dapat di akses melalui laptop/hp</p>
                        </div>
                    </div>
                    <!-- end of card -->
                </div> <!-- end of col -->

                <!-- Card -->
                <div class="col-lg-3">
                    <div class="card">
                        <div class="card-image">
                            <img class="img-fluid" src="{{ asset('front') }}/images/KRS.png" alt="alternative">
                        </div>
                        <div class="card-body">
                            <h4 class="card-title">KRS/KHS</h4>
                            <p>Sistem KRS/KHS Online</p>
                        </div>
                    </div>
                </div>
                <!-- end of card -->

                <!-- Card -->
                <div class="col-lg-3">
                    <div class="card">
                        <div class="card-image">
                            <img class="img-fluid" src="{{ asset('front') }}/images/integrationsystem.png"
                                alt="alternative">
                        </div>
                        <div class="card-body">
                            <h4 class="card-title">Integrated System</h4>
                            <p>Layanan sistem saling terintegrasi</p>
                        </div>
                    </div>
                </div>
                <!-- end of card -->

                <!-- Card -->
                <div class="col-lg-3">
                    <div class="card">
                        <div class="card-image">
                            <img class="img-fluid" src="{{ asset('front') }}/images/videoacademy.png" alt="alternative">
                        </div>
                        <div class="card-body">
                            <h4 class="card-title">Video Academy</h4>
                            <p>Materi pembelajaran dalam bentuk video</p>
                        </div>
                    </div>
                </div>
                <!-- end of card -->
            </div> <!-- end of row -->
        </div> <!-- end of container -->
    </div> <!-- end of cards-1 -->
    <!-- end of Features -->

    <!-- Daftar Kampus -->
    <div id="pricing" class="cards-2">
        <div class="container">
            <div class="row">
                <div class="col-lg-7">
                    <h2 class="h2-heading">Daftar Kampus</h2>
                </div> <!-- end of col -->
                <div class="col-lg-5">
                    <div class="sparkline10-graph">
                        <div class="basic-login-form-ad">
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="basic-login-inner inline-basic-form">
                                        <form action="#">
                                            <div class="form-group-inner">

                                                <div class="row">
                                                    <form action="/dashboard">
                                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                            <input type="text" name="cari"
                                                                value="{{ request('cari') }}"
                                                                class="form-control basic-ele-mg-b-10 responsive-mg-b-10"
                                                                placeholder="Search " />
                                                        </div>
                                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                            <div class="login-btn-inner">
                                                                <div class="login-horizental lg-hz-mg">
                                                                    <form class="d-flex" action="{{ URL::current() }}"
                                                                        method="GET">
                                                                        <button class="btn btn-solid-reg md-4"
                                                                            type="submit">Cari</button>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>

                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> <!-- end of col -->
            </div> <!-- end of row -->

            <section class="store-gallery mt-5">
                <div class="row justify-content-center">
                    <!-- Card-->
                    @foreach ($kampus as $p)
                        <div class="col-md-3">
                            <a class="link-kampus" href="{{ route('profilkampus', $p->slug) }}">
                                <div class="card">
                                    <div class="card-body daftar-kampus">
                                        <div class="price">
                                            <img class="img-fluid" src="{{ asset('front') }}/images/features-1.png"
                                                alt="alternative">
                                        </div>
                                        <div class="card-title"> {{ $p->nama_kampus }} </div>
                                        <p class="text-xs font-weight-bold mb-0">
                                            {!! \Illuminate\Support\Str::limit($p->deskripsi_kampus, 30, $end = '...') !!}
                                        </p>
                                    </div>
                                </div> <!-- end of card -->
                        </div>
                        </a><!-- end of col -->
                        <!-- end of card -->
                    @endforeach
                    <!-- end of card -->
                </div> <!-- end of row -->

                {{--    {{ $kampus->links() }}     --}}

            </section>
            {{-- <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
            <div class="login-btn-inner">
                <div class="login-horizental lg-hz-mg">
                    <button a href="{{ route('all-kampus') }}" class="btn btn-solid-reg">Lihat Semua Kampus</button>
                </div>
            </div>
        </div> --}}

            <div class="col-lg-12">
                <a class="btn-solid-reg page-scroll" href="{{ route('all-kampus') }}">
                    Lihat Semua Kampus
                </a>
            </div>
        </div> <!-- end of container -->
    </div> <!-- end of cards-2 -->
    <!-- end of Daftar Kampus -->

    <!-- Video -->
    <div id="video" class="basic-2">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="image-container">
                        <div class="video-wrapper">
                            <a class="popup-youtube" href="https://www.youtube.com/watch?v=fLCjQJCekTs"
                                data-effect="fadeIn">
                                <img class="img-fluid" src="{{ asset('front') }}/images/video-image.png"
                                    alt="alternative">
                                <span class="video-play-button">
                                    <span></span>
                                </span>
                            </a>
                        </div> <!-- end of video-wrapper -->
                    </div> <!-- end of image-container -->
                    <!-- end of video preview -->

                    <div class="p-heading">What better way to show off Tivo marketing automation saas app than
                        presenting you some great situations of each module and tool available to users in a video</div>
                </div> <!-- end of col -->
            </div> <!-- end of row -->
        </div> <!-- end of container -->
    </div> <!-- end of basic-2 -->
    <!-- end of video -->

    <!-- Manfaat Dan Keunggulan -->
    <div id="features" class="tabs">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">

                    <!-- Tabs Content -->
                    <div class="tab-content" id="argoTabsContent">
                        <!-- Tab -->
                        <div class="tab-pane fade show active" id="tab-1" role="tabpanel" aria-labelledby="tab-1">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="image-container">
                                        <img class="img-fluid" src="{{ asset('front') }}/images/features-1.png"
                                            alt="alternative">
                                    </div> <!-- end of image-container -->
                                </div> <!-- end of col -->
                                <div class="col-lg-6">
                                    <div class="text-container">
                                        <h3>Manfaat dan Keunggulan Pratform Kami</h3>
                                        <ul class="list-unstyled li-space-lg">
                                            <li class="media">
                                                <i class="fas fa-check-circle"></i>
                                                <div class="media-body">
                                                    Fleksibilitas waktu
                                                </div>
                                            </li>
                                            <li class="media">
                                                <i class="fas fa-check-circle"></i>
                                                <div class="media-body">
                                                    Ijazah asli dikeluarkan oleh kampus
                                                </div>
                                            </li>
                                            <li class="media">
                                                <i class="fas fa-check-circle"></i>
                                                <div class="media-body">
                                                    Izin resmi DIKTI
                                                </div>
                                            </li>
                                            <li class="media">
                                                <i class="fas fa-check-circle"></i>
                                                <div class="media-body">
                                                    Proses regristrasi cepat
                                                </div>
                                            </li>
                                        </ul>
                                    </div> <!-- end of text-container -->
                                </div> <!-- end of col -->
                            </div> <!-- end of row -->
                        </div> <!-- end of tab-pane -->
                        <!-- end of tab -->
                    </div> <!-- end of tab content -->
                    <!-- end of tabs content -->

                </div> <!-- end of col -->
            </div> <!-- end of row -->
        </div> <!-- end of container -->
    </div> <!-- end of tabs -->
    <!-- end of Manfaat Dan Keunggulan -->

@endsection
