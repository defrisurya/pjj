@extends('Pmb.layouts.app')

@section('title', 'Home Pmb')

@section('content')
    <div class="breadcome-area">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="breadcome-list">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <ul class="breadcome-heading">
                                    <li>
                                        <span class="nama">PMB true</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="analytics-sparkle-area">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-6 col-sm-6 col-xs-12">
                    <div class="sparkline8-list">
                        <div class="sparkline8-hd">
                            <div class="portlet-title">
                            </div>
                        </div>
                        <div class="sparkline12-graph">
                            <div class="basic-login-form-ad">
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="all-form-element-inner">
                                            <form action="" method="POST" enctype="multipart/form-data">
                                                {{ csrf_field() }}
                                                <div class="form-group">
                                                    <label>Nama Kampus</label>
                                                    <select class="form-control  basic" name="kampus_id">
                                                        <option value="">Pilih kampus</option>
                                                        {{-- @forelse ($kampus as $item)
                                                            <option value="{{ $item->id }}">{{ $item->nama_kampus }}</option>
                                                        @empty
                                                        @endforelse --}}
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label>Nama Prodi</label>
                                                    <select class="form-control basic" name="prodi_id">
                                                        <option value="">Pilih Prodi</option>
                                                        {{-- @forelse ($prodi as $item)
                                                            <option value="{{ $item->id }}">{{ $item->nama_prodi }}</option>
                                                        @empty
                                                        @endforelse --}}
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label>Nama Fakultas</label>
                                                    <select class="form-control basic" name="fakultas_id">
                                                        <option value="">Pilih Fakultas</option>
                                                        {{-- @forelse ($fakultas as $item)
                                                            <option value="{{ $item->id }}">{{ $item->nama_fakultas }}</option>
                                                        @empty
                                                        @endforelse --}}
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label>Nama Lengkap</label>
                                                    <input type="text" name="nama_camaba" class="form-control @error('nama_camaba') is-invalid @enderror" 
                                                    placeholder="Masukkan Nama Lengkap" value="{{ old('nama_camaba') }}">
                                                    @error('nama_camaba')
                                                        <div class="invalid-feedback">{{ $messeage }} </div>
                                                    @enderror
                                                </div>
                                                <div class="form-group">
                                                    <label>Tempat Lahir</label>
                                                    <input type="text" name="tempat_lahir" class="form-control @error('tempat_lahir') is-invalid @enderror" 
                                                    placeholder="Masukkan Tempat Lahir" value="{{ old('tempat_lahir') }}">
                                                    @error('tempat_lahir')
                                                        <div class="invalid-feedback">{{ $messeage }} </div>
                                                    @enderror
                                                </div>
                                                <div class="form-group">
                                                    <label>Tanggal Lahir</label>
                                                    <input type="date" name="tanggal_lahir" class="form-control @error('tanggal_lahir') is-invalid @enderror" 
                                                    placeholder="Masukkan Tempat Lahir" value="{{ old('tanggal_lahir') }}">
                                                    @error('tanggal_lahir')
                                                        <div class="invalid-feedback">{{ $messeage }} </div>
                                                    @enderror
                                                </div>
                                                <div class="form-group">
                                                    <label>Jenis Kelamin</label>
                                                    <select name="jenis_kelamin" class="form-control custom-select-value">
                                                        <option value="L">Laki-Laki</option>
                                                        <option value="P">Perempuan</option>
                                                   </select>
                                                    @error('jenis_kelamin')
                                                        <div class="invalid-feedback">{{ $messeage }} </div>
                                                    @enderror
                                                </div>
                                                <div class="form-group">
                                                    <label>Agama</label>
                                                    <input type="text" name="agama" class="form-control @error('agama') is-invalid @enderror" 
                                                    placeholder="Masukkan Tempat Lahir" value="{{ old('agama') }}">
                                                    @error('agama')
                                                        <div class="invalid-feedback">{{ $messeage }} </div>
                                                    @enderror
                                                </div>
                                                <div class="form-group">
                                                    <label>Email</label>
                                                    <input type="text" name="email" class="form-control @error('email') is-invalid @enderror" 
                                                    placeholder="Masukkan Tempat Lahir" value="{{ old('email') }}">
                                                    @error('email')
                                                        <div class="invalid-feedback">{{ $messeage }} </div>
                                                    @enderror
                                                </div>
                                                <div class="form-group">
                                                    <label>Nomor Handphone</label>
                                                    <input type="text" name="no_tlp" class="form-control @error('no_tlp') is-invalid @enderror" 
                                                    placeholder="Masukkan No Telpon" value="{{ old('no_tlp') }}">
                                                    @error('no_tlp')
                                                        <div class="invalid-feedback">{{ $messeage }} </div>
                                                    @enderror
                                                </div>
                                                <div class="form-group">
                                                    <label>Alamat</label>
                                                    <textarea type="text" name="alamat" class="form-control @error('alamat') is-invalid @enderror" 
                                                    placeholder="Masukkan Alamat" value="{{ old('alamat') }}"> </textarea>
                                                    @error('alamat')
                                                        <div class="invalid-feedback">{{ $messeage }} </div>
                                                    @enderror
                                                </div>
                                                <div class="form-group">
                                                    <label>Provinsi</label>
                                                    <input type="text" name="asal_provinsi" class="form-control @error('asal_provinsi') is-invalid @enderror" 
                                                    placeholder="Masukkan Nama asal_provinsi" value="{{ old('asal_provinsi') }}">
                                                    @error('asal_provinsi')
                                                        <div class="invalid-feedback">{{ $messeage }} </div>
                                                    @enderror
                                                </div>
                                                <div class="form-group">
                                                    <label>Kabupaten</label>
                                                    <input type="text" name="asal_kabupaten" class="form-control @error('asal_kabupaten') is-invalid @enderror" 
                                                    placeholder="Masukkan Nama asal_kabupaten" value="{{ old('asal_kabupaten') }}">
                                                    @error('asal_kabupaten')
                                                        <div class="invalid-feedback">{{ $messeage }} </div>
                                                    @enderror
                                                </div>
                                                <div class="form-group">
                                                    <label>Asal Sekolah</label>
                                                    <input type="text" name="asal_sekolah" class="form-control @error('asal_sekolah') is-invalid @enderror" 
                                                    placeholder="Masukkan Nama asal_sekolah" value="{{ old('asal_sekolah') }}">
                                                    @error('asal_sekolah')
                                                        <div class="invalid-feedback">{{ $messeage }} </div>
                                                    @enderror
                                                </div>
                                                <div class="form-group">
                                                    <label>Tahun Ajaran</label>
                                                    <input type="text" name="tahun_ajaran" class="form-control @error('tahun_ajaran') is-invalid @enderror" 
                                                    placeholder="Masukkan Nama tahun_ajaran" value="{{ old('tahun_ajaran') }}">
                                                    @error('tahun_ajaran')
                                                        <div class="invalid-feedback">{{ $messeage }} </div>
                                                    @enderror
                                                </div>
                                                <div class="form-group">
                                                    <label>NIK</label>
                                                    <input type="text" name="nik" class="form-control @error('nik') is-invalid @enderror" 
                                                    placeholder="Masukkan NIk" value="{{ old('nik') }}">
                                                    @error('nik')
                                                        <div class="invalid-feedback">{{ $messeage }} </div>
                                                    @enderror
                                                </div>
                                                <div class="form-group-inner">
                                                    <label>Ijazah</label>
                                                    <input type="file" name="file_ijazah" id=""
                                                        class="form-control" required>
                                                </div>
                                                <div class="form-group-inner">
                                                    <label>Transkip Nilai</label>
                                                    <input type="file" name="file_transkip" id=""
                                                        class="form-control" required>
                                                </div>
                                                <div class="form-group-inner">
                                                    <label>Raport</label>
                                                    <input type="file" name="file_raport" id=""
                                                        class="form-control" required>
                                                </div>
                                                <button type="submit" class="btn btn-success btn-md">Simpan</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="breadcome-list link-submit">
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <a href="{{ route('mhs') }}" class="link-profile">
                                        <button class="btn btn-primary btn-md">
                                            Kembali
                                        </button>
                                    </a>
                                    <button type="submit" class="btn btn-success btn-md">
                                        Simpan
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
