<!-- Footer -->
<div class="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="footer-col first">
                    <a href="{{ url('/Kampusku') }}">
                        <img class="logo-footer" src="{{ asset('front') }}/images/logofooterpjj.png"
                            alt="alternative">
                    </a>
                    <p class="p-small">
                        Alamat: Jl. Garuda No.18A, Pelem Mulong, Banguntapan, Kec. Banguntapan, Kabupaten Bantul, Daerah
                        Istimewa Yogyakarta
                        55198
                    </p>
                </div>
            </div> <!-- end of col -->

            <div class="col-md-4">
                <div class="footer-col last">
                    <h4>Contact</h4>
                    <ul class="list-unstyled li-space-lg p-small">
                        <li class="media">
                            <i class="fas fa-phone"></i>
                            <div class="media-body">0276-9800-000</div>
                        </li>
                        <li class="media">
                            <i class="fas fa-envelope"></i>
                            <div class="media-body">
                                <a class="white" href="mailto:kampusku@kampusku.com">
                                    kampusku@kampusku.com
                                </a>
                            </div>
                        </li>
                        <li class="media">
                            <i class="fab fa-whatsapp"></i>
                            <div class="media-body">0877-9807-0009</div>
                        </li>
                    </ul>
                </div>
            </div> <!-- end of col -->

            <div class="col-md-4">
                <div class="footer-col middle">
                    <h4>Tentang Kampusku</h4>
                    <ul class="list-unstyled li-space-lg p-small">
                        <li class="media">
                            <div class="media-body">
                                <a class="white" href="#your-link">
                                    Kampusku info
                                </a>
                            </div>
                        </li>
                        <li class="media">
                            <div class="media-body">
                                <a class="white" href="terms-conditions.html">
                                    Karir
                                </a>
                            </div>
                        </li>
                        <li class="media">
                            <div class="media-body">
                                <a class="white" href="terms-conditions.html">
                                    Kampusku Blog
                                </a>
                            </div>
                        </li>
                        <li class="media">
                            <div class="media-body">
                                <a class="white" href="terms-conditions.html">
                                    Service
                                </a>
                            </div>
                        </li>
                    </ul>
                </div>
            </div> <!-- end of col -->

        </div> <!-- end of row -->
    </div> <!-- end of container -->
</div> <!-- end of footer -->
<!-- end of footer -->

<!-- Copyright -->
<div class="copyright">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <p class="p-small">Copyright © 2022 PT. Inovasi Nuswantara Digital
                </p>
            </div> <!-- end of col -->
        </div> <!-- enf of row -->
    </div> <!-- end of container -->
</div> <!-- end of copyright -->
<!-- end of copyright -->
