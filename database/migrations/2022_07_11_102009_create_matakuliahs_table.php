<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMatakuliahsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('matakuliahs', function (Blueprint $table) {
            $table->id();
            $table->foreignId('kampus_id')->constrained('kampus')->onDelete('cascade')->onUpdate('cascade');
            $table->foreignId('prodi_id')->constrained('prodis')->onDelete('cascade')->onUpdate('cascade');
            $table->foreignId('dosen_id')->constrained('dosens')->onDelete('cascade')->onUpdate('cascade');
            $table->foreignId('jenjang_pend_id')->constrained('jenjang_pends')->onDelete('cascade')->onUpdate('cascade');
            $table->string('semester');
            $table->string('kode_matakuliah');
            $table->string('nama_matakuliah');
            $table->integer('jumlah_sks');
            $table->enum('sifat_matakuliah', ['Wajib', 'Pilihan']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('matakuliahs');
    }
}
