<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCamabasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('camabas', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->constrained('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreignId('kampus_id')->constrained('kampus')->onDelete('cascade')->onUpdate('cascade');
            $table->foreignId('prodi_id')->constrained('prodis')->onDelete('cascade')->onUpdate('cascade');
            $table->foreignId('fakultas_id')->constrained('fakultas')->onDelete('cascade')->onUpdate('cascade');
            $table->foreignId('provinsi_id')->constrained('provinces')->onDelete('cascade')->onUpdate('cascade');
            $table->foreignId('kabupaten_id')->constrained('regencies')->onDelete('cascade')->onUpdate('cascade');
            $table->foreignId('kecamatan_id')->constrained('districts')->onDelete('cascade')->onUpdate('cascade');
            $table->foreignId('desa_id')->constrained('villages')->onDelete('cascade')->onUpdate('cascade');
            $table->string('nama_camaba');
            $table->string('nik');
            $table->string('tempat_lahir');
            $table->date('tanggal_lahir');
            $table->string('jenis_kelamin');
            $table->string('agama');
            $table->string('email');
            $table->string('no_tlp');
            $table->string('alamat');
            $table->string('asal_sekolah');
            $table->string('tahun_ajaran');
            $table->string('foto');
            $table->string('file_ijazah');
            $table->string('file_transkrip');
            $table->string('file_raport');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('camabas');
    }
}
