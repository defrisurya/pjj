<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMahasiswasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mahasiswas', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->constrained('users')->onDelete('cascade')->onUpdate('cascade')->nullable();
            $table->foreignId('kampus_id')->constrained('kampus')->onDelete('cascade')->onUpdate('cascade')->nullable();
            $table->foreignId('prodi_id')->constrained('prodis')->onDelete('cascade')->onUpdate('cascade')->nullable();
            $table->foreignId('fakultas_id')->constrained('fakultas')->onDelete('cascade')->onUpdate('cascade')->nullable();
            $table->foreignId('camaba_id')->constrained('camabas')->onDelete('cascade')->onUpdate('cascade')->nullable();
            $table->foreignId('provinsi_id')->constrained('provinces')->onDelete('cascade')->onUpdate('cascade')->nullable();
            $table->foreignId('kabupaten_id')->constrained('regencies')->onDelete('cascade')->onUpdate('cascade')->nullable();
            $table->foreignId('kecamatan_id')->constrained('districts')->onDelete('cascade')->onUpdate('cascade')->nullable();
            $table->foreignId('desa_id')->constrained('villages')->onDelete('cascade')->onUpdate('cascade')->nullable();
            $table->string('no_induk_mahasiswa')->nullable();
            $table->string('no_induk_kependudukan')->nullable();
            $table->string('nama_lengkap')->nullable();
            $table->string('jenis_kelamin')->nullable();
            $table->string('program_studi')->nullable();
            $table->string('agama')->nullable();
            $table->string('kewarganegaraan')->nullable();
            $table->string('tempat_lahir')->nullable();
            $table->date('tanggal_lahir')->nullable();
            $table->string('email')->nullable();
            $table->string('no_telpon')->nullable();
            $table->string('asal_sekolah')->nullable();
            $table->string('penerima_kps')->nullable();
            $table->text('alamat')->nullable();
            $table->string('kode_pos')->nullable();
            $table->integer('no_induk_kependudukan_ayah')->nullable();
            $table->string('nama_lengkap_ayah')->nullable();
            $table->date('tanggal_lahir_ayah')->nullable();
            $table->string('pendidikan_ayah')->nullable();
            $table->string('pekerjaan_ayah')->nullable();
            $table->string('rentang_penghasilan_ayah')->nullable();
            $table->string('no_telpon_ayah')->nullable();
            $table->text('alamat_ayah')->nullable();
            $table->integer('no_induk_kependudukan_ibu')->nullable();
            $table->string('nama_lengkap_ibu')->nullable();
            $table->date('tanggal_lahir_ibu')->nullable();
            $table->string('pendidikan_ibu')->nullable();
            $table->string('pekerjaan_ibu')->nullable();
            $table->string('rentang_penghasilan_ibu')->nullable();
            $table->string('no_telpon_ibu')->nullable();
            $table->text('alamat_ibu')->nullable();
            $table->string('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mahasiswas');
    }
}
