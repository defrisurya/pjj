<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKhsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('khs', function (Blueprint $table) {
            $table->id();
            $table->foreignId('kampus_id')->constrained('kampus')->onDelete('cascade')->onUpdate('cascade');
            $table->foreignId('prodi_id')->constrained('prodis')->onDelete('cascade')->onUpdate('cascade');
           // $table->foreignId('mahasiswa_id')->constrained('mahasiswas')->onDelete('cascade')->onUpdate('cascade');
            $table->string('tahun_akademik');
            $table->string('semester');
            $table->string('matakuliah');
            $table->integer('nilai_angka');
            $table->integer('nilai_huruf');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('khs');
    }
}