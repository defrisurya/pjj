<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKampusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kampus', function (Blueprint $table) {
            $table->id();
            $table->string('logo_kampus')->nullable();
            $table->string('nama_kampus');
            $table->string('video_kampus')->nullable();
            $table->string('deskripsi_kampus');
            $table->string('akreditasi_kampus');
            $table->string('tipe_kampus');
            $table->string('alamat_kampus');
            $table->string('slug');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kampus');
    }
}
