<?php

use Faker\Provider\Lorem;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'name'     => Str::random(10),
                'email'    => 'student@mail.com',
                'role'     => 'mahasiswa',
                'password' => Hash::make('password'),
            ],
            [
                'name'     => Str::random(10),
                'email'    => 'admin@mail.com',
                'role'     => 'admin',
                'password' => Hash::make('password'),
            ],
            [
                'name'     => Str::random(10),
                'email'    => 'admin.akademik@mail.com',
                'role'     => 'adminakademik',
                'password' => Hash::make('password'),
            ],
            [
                'name'     => Str::random(10),
                'email'    => 'admin.dosen@mail.com',
                'role'     => 'admindosen',
                'password' => Hash::make('password'),
            ]
        ]);
    }
}
