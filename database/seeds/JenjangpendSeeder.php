<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class JenjangpendSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('jenjang_pends')->insert([
            [
                'jenjang_pend'     => 'S2',
            ],
            [
                'jenjang_pend'     => 'S3',
            ],
            [
                'jenjang_pend'     => 'D1',
            ],
            [
                'jenjang_pend'     => 'D2',
            ],
            [
                'jenjang_pend'     => 'D3',
            ],
            [
                'jenjang_pend'     => 'D4/S1',
            ]
        ]);
    }
}
