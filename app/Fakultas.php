<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fakultas extends Model
{
    protected $table = 'fakultas';

    protected $fillable = [
        'kampus_id',
        'nama_fakultas',
        'akreditasi_fakultas',
    ];

    public function kampus()
    {
        return $this->hasOne(Kampus::class, 'id', 'kampus_id');
    }

    public function prodi()
    {
        return $this->hasMany(Prodi::class, 'prodi_id', 'id');
    }



    // public function kampus()
    // {
    //     // satu kampus memiliki banyak fakultas
    //     return $this->hasMany(Kampus::class, 'kampus_id', 'id');
    // }
}