<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Models\Province;

class Kampus extends Model
{
    protected $table = 'kampus';
    protected $fillable = [
        'logo_kampus',
        'nama_kampus',
        'video_kampus',
        'deskripsi_kampus',
        'akreditasi_kampus',
        'tipe_kampus',
        'alamat_kampus',
        'slug',
    ];

    public function prodi()
    {
        return $this->hasMany(Prodi::class, 'kampus_id', 'id');
    }

    public function fakultas()
    {
        return $this->hasMany(Fakultas::class, 'kampus_id', 'id');
    }

    public function infopmb()
    {
        return $this->hasMany(Infopmb::class, 'infopmb_id', 'id');
    }

    public function student()
    {
        return $this->hasMany(Student::class, 'student_id', 'id');
    }

    // public function kampus()
    // {
    //     return $this->hasMany(Kampus::class, 'id', 'kampus_id');
    // }


    // public function fakultas()
    // {
    //     // satu kampus memiliki banyak fakultas
    //     return $this->hasOne(Fakultas::class, 'fakultas_id', 'id');
    // }

    //     public function size()
    //     {
    //         return $this->hasMany(Size::class, 'product_id', 'id');
    //     }
    //     public function warna()
    //     {
    //         return $this->hasMany(Warna::class, 'product_id', 'id');
    //     }
}
