<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class ProdiCreate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'kampus_id' => 'required',
            'nama_prodi' => 'required',
            'profile_prodi' => 'required',
            'program_jurusan' => 'required',
            'biaya_kuliah' => 'required',
        ];
    }
}
