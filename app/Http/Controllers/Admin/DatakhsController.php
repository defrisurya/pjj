<?php

namespace App\Http\Controllers\Admin;

use App\Datakhs;
use App\Http\Controllers\Controller;
use App\Kampus;
use App\Prodi;
use Illuminate\Http\Request;

class DatakhsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data  = Datakhs::with('kampus', 'prodi')->paginate(10);

        return view('AdminAkademik.Datakhs.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kampus = Kampus::all();
        $prodi  = Prodi::all();

        return view('AdminAkademik.Datakhs.create', compact('kampus', 'prodi'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'tahun_akademik'    => 'required',
            'semester'          => 'required',
            'matakuliah'        => 'required',
            'nilai_angka'       => 'required',
            'nilai_huruf'       => 'required',
        ]);

        $data = $request->all();
        Datakhs::create($data);

        toast('Data Berhasil Di tambahkan', 'success');
        return redirect()->route('datakhs.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $kampus = Kampus::get();
        $prodi = Prodi::get();
        $datakhs = Datakhs::find($id);

        return view('AdminAkademik.Datakhs.edit', compact('kampus', 'prodi', 'datakhs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'tahun_akademik'    => 'required',
            'semester'          => 'required',
            'matakuliah'        => 'required',
            'nilai_angka'       => 'required',
            'nilai_huruf'       => 'required',
        ]);

        $data     = $request->all();
        $datakhs  = Datakhs::findorfail($id);
        $datakhs->update($data);

        toast('Data Berhasil Di Update', 'success');
        return redirect()->route('datakhs.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Datakhs::where('id', $id)->delete();
        return redirect()->back();
    }
}