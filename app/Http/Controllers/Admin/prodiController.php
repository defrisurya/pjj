<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\ProdiCreate;
use App\Kampus;
use App\Prodi;
use App\Fakultas;
use App\JenjangPend;
use Illuminate\Http\Request;

use function GuzzleHttp\Promise\all;

class prodiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Prodi::with('kampus', 'fakultas')->paginate(10);

        return view('Admin_Kemahasiswaan.Prodi.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kampus    = Kampus::all();
        $fakultas  = Fakultas::all();
        $jenj_pendidikan = JenjangPend::all();

        return view('Admin_Kemahasiswaan.Prodi.create', compact('kampus', 'fakultas', 'jenj_pendidikan'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'kode_prodi'            => 'required',
            'nama_prodi'            => 'required',
            'deskripsi_prodi'       => 'required',
            'akreditasi_prodi'      => 'required',
            'biaya_prodi'           => 'required',
        ]);

        $data = $request->all();

        Prodi::create($data);

        toast('Data Prodi Berhasil Ditambahkan', 'success');
        return redirect()->route('prodi.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $kampus     = Kampus::get();
        $fakultas   = Fakultas::get();

        $prodi = Prodi::find($id);

        return view('Admin_Kemahasiswaan.Prodi.edit', compact('prodi', 'kampus', 'fakultas'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $request->validate([
            'nama_prodi'            => 'required',
            'deskripsi_prodi'       => 'required',
            'akreditasi_prodi'      => 'required',
            'biaya_prodi'           => 'required',
        ]);

        $data = $request->all();
        // $prodi = Prodi::findorfail($id);
        $prodi = Prodi::find($id);

        $prodi->update($data);

        toast('Data Prodi Berhasil Di Update', 'success');
        return redirect()->route('prodi.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Prodi::where('id', $id)->delete();

        return redirect()->back();
    }
}
