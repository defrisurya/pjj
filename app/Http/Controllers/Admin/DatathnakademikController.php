<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Datathnakademik;
use Illuminate\Http\Request;
use App\Kampus;

class DatathnakademikController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $kampus   = Kampus::all();
        $data = Datathnakademik::with('kampus')->paginate(10);

        // dd($data);
        return view('AdminAkademik.Datathnakademik.index', compact('data', 'kampus'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'tahun_akademik'   => 'required',
            'status'           => 'required',
        ]);

        $data = $request->all();

        Datathnakademik::create($data);

        toast('Data Berhasil Ditambahkan', 'success');
        return redirect()->route('datatahunakademik.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $kampus = Kampus::get();
        $datatahunakademik = Datathnakademik::find($id);

        return view('AdminAkademik.Datathnakademik.edit', compact('kampus','datatahunakademik'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // $request->validate([
        //     'tahun_akademik'   => 'required',
        //     'status'           => 'required'
        // ]);

        $data               = $request->all();
        $datatahunakademik  = Datathnakademik::findorfail($id);
        $datatahunakademik->update($data);

        toast('Data Akademik berhasil di Update', 'success');
        return redirect()->route('datatahunakademik.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Datathnakademik::where('id', $id)->delete();

        return redirect()->back();
    }
}