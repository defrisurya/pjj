<?php

namespace App\Http\Controllers\Admin;

use App\Datakrs;
use App\Http\Controllers\Controller;
use App\Kampus;
use App\Prodi;
use Illuminate\Http\Request;

class DatakrsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $data = Datakrs::with('kampus', 'prodi')->paginate(10);

        return view('AdminAkademik.Datakrs.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $kampus = Kampus::all();
        $prodi   = Prodi::all();

        return view('AdminAkademik.Datakrs.create', compact('kampus', 'prodi'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'tahun_akademik'    => 'required',
            'semester'          => 'required',
            'matakuliah'        => 'required',
            'jumlah_sks'        => 'required',
            'sifat'             => 'required',
        ]);

        $data = $request->all();
        Datakrs::create($data);

        toast('Data Jadwal Kuliah Di tambahkan', 'success');
        return redirect()->route('datakrs.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $kampus = Kampus::get();
        $prodi  = Prodi::get();
        $datakrs = Datakrs::find($id);

        return view('AdminAkademik.Datakrs.edit', compact('kampus', 'prodi', 'datakrs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'tahun_akademik'    => 'required',
            'semester'          => 'required',
            'matakuliah'        => 'required',
            'jumlah_sks'        => 'required',
            'sifat'             => 'required',
        ]);

        $data      = $request->all();
        $datakrs   = Datakrs::findorfail($id);
        $datakrs->update($data);

        toast('Data KRS Berhasil Di Update', 'success');
        return redirect()->route('datakrs.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Datakrs::where('id', $id)->delete();

        return redirect()->back();
    }
}