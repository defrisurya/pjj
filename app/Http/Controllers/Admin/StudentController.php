<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Mahasiswa;
use App\Student;
use App\Kampus;
use App\Prodi;
use Illuminate\Http\Request;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $data = Mahasiswa::paginate(10);
        // $data = Mahasiswa::with('prodi', 'fakultas');
         $data = Mahasiswa::with('kampus','prodi')->paginate(10);
        // $data = Mahasiswa::with('prodi')->get();

         // dd($data);
        return view('Admin_Kemahasiswaan.Student.index', compact('data'));
       // return view('Admin_Kemahasiswaan.Student.index', compact('mahasiswa'));
    
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kampus     = Kampus::all();
        $prodi      = Prodi::all();

        return view('Admin_Kemahasiswaan.Student.create', compact('kampus','prodi'));
        // return view('Admin_Kemahasiswaan.Student.index', compact('mahasiswa'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $student = mahasiswa::find($id);

        return view('Admin_Kemahasiswaan.Student.show', compact('student'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Mahasiswa::where('id', $id)->delete();

        return redirect()->back();
    }
}
