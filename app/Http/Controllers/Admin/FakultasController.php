<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\FakultasCreate;
use App\Http\Requests\Admin\FakultasUpdate;
use Illuminate\Http\Request;
use App\Fakultas;
use App\Kampus;

class FakultasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $data = Fakultas::paginate(15);
        $data = Fakultas::with('kampus')->paginate(10);
        //dd($data);

        return view('Admin_Kemahasiswaan.Fakultas.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kampus = Kampus::all();
        // dd($kampus);
        return view('Admin_Kemahasiswaan.Fakultas.create', compact('kampus'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'nama_fakultas'        => 'required',
            'akreditasi_fakultas'  => 'required',
        ]);
        $data = $request->all();
        // dd($data);
        Fakultas::create($data);

        toast('Data Fakultas Berhasil Ditambahkan', 'success');
        return redirect()->route('fakultas.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function edit( /* $id  */)
    public function edit($id)
    {
        //
        $kampus = Kampus::get();
        // $kampus = Kampus::find($id)->first();
        $fakultas = Fakultas::find($id);
        // dd($fakultas);
        return view('Admin_Kemahasiswaan.Fakultas.edit', compact('kampus', 'fakultas'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $request->validate([
            'nama_fakultas'        => 'required',
            'akreditasi_fakultas'  => 'required',
        ]);

        $data = $request->all();
        $fakultas = Fakultas::find($id);

        $fakultas->update($data);

        toast('Data Fakultas berhasil Di Update', 'success');
        return redirect()->route('fakultas.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Fakultas::where('id', $id)->delete();

        return redirect()->back();
    }
}