<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\KampusCreate;
use App\Http\Requests\Admin\KampusUpdate;
use App\Kampus;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use function GuzzleHttp\Promise\all;

class kampusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Kampus::paginate(10);

        return view('Admin_Kemahasiswaan.Kampus.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Admin_Kemahasiswaan.Kampus.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama_kampus'         => 'required',
            'deskripsi_kampus'    => 'required',
            'akreditasi_kampus'   => 'required',
            'tipe_kampus'         => 'required',
            'alamat_kampus'       => 'required',
        ]);

        $data = $request->all();
        $data['slug'] = Str::slug($data['nama_kampus'], '-');
        if ($request->has('logo_kampus')) {
            $foto           = $request->logo_kampus;
            $new_foto       = time() . 'produk' . $foto->getClientOriginalName();
            $tujuan_uploud  = 'uplouds/Produk/';
            $foto->move($tujuan_uploud, $new_foto);
            $data['logo_kampus']   = $tujuan_uploud . $new_foto;
        }
        Kampus::create($data);

        // dd($data);
        toast('Data Kampus Berhasil Ditambahkan', 'success');
        return redirect()->route('kampus.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $kampus = Kampus::findorfail($id);
        // dd($kampus);
        return view('Admin_Kemahasiswaan.Kampus.edit', compact('kampus'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama_kampus'         => 'required',
            'deskripsi_kampus'    => 'required',
            'akreditasi_kampus'   => 'required',
            'tipe_kampus'         => 'required',
            'alamat_kampus'       => 'required',
        ]);

        $data = $request->all();
        $kampus = Kampus::findorfail($id);
        $data['logo_kampus'] = $kampus->logo_kampus;
        $data['slug'] = Str::slug($data['nama_kampus'], '-');


        if ($request->has('logo_kampus')) {
            $foto = $request->logo_kampus;
            $new_foto = time() . 'produk' . $foto->getClientOriginalName();
            $tujuan_uploud = 'uplouds/Produk/';
            $foto->move($tujuan_uploud, $new_foto);
            $data['logo_kampus'] = $tujuan_uploud . $new_foto;
        }
        // dd($data);
        $kampus->update($data);

        toast('Data Kampus Berhasil Di Update', 'success');
        return redirect()->route('kampus.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Kampus::where('id', $id)->delete();

        return redirect()->back();
    }
}
