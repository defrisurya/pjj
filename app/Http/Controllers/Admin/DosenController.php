<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Dosen;
use App\Fakultas;
use App\Kampus;
use App\Prodi;
use App\User;
use Illuminate\Http\Request;

class DosenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Dosen::with('kampus','user')->paginate(10);

        return view('Admin_Kemahasiswaan.Dosen.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kampus     = Kampus::all();
        $prodi      = Prodi::all();
        $fakultas   = Fakultas::all();
        $user = User::where('role', 'admindosen' )->get();

        // dd($user);

        return view('Admin_Kemahasiswaan.Dosen.create', compact('kampus', 'prodi', 'fakultas','user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nip'               => 'required',
            'jenis_kelamin'     => 'required',
            'pendidikan'        => 'required',
            'email'             => 'required',
            'no_telpon'         => 'required',
            'alamat'            => 'required',
        ]);

        $data = $request->all();

        $data['role'] = 'admindosen';
        $data['password'] = bcrypt($data['password']);
        // dd($data);
        
        $user = User::create($data);

            if ($request->has('foto_dosen')) {
                $foto           = $request->foto_dosen;
                $new_foto       = time() . 'produk' . $foto->getClientOriginalName();
                $tujuan_uploud  = 'uplouds/Produk/';
                $foto->move($tujuan_uploud, $new_foto);
                // $data['foto_dosen']   = $tujuan_uploud . $new_foto;
            }

            foreach ($data['nip'] as $key => $value) {
                $data2 = array(
                    'kampus_id'     => $data['kampus_id'][$key],
                    'prodi_id'      => $data['prodi_id'][$key],
                    'fakultas_id'   => $data['fakultas_id'][$key],
                    'user_id'       => $user->id,
                    'nip'           => $data['nip'][$key],
                    'jenis_kelamin' => $data['jenis_kelamin'][$key],
                    'email'         => $user->email,
                    'pendidikan'    => $data['pendidikan'][$key],
                    'no_telpon'     => $data['no_telpon'][$key],
                    'alamat'        => $data['alamat'][$key],
                    'foto_dosen'    => $tujuan_uploud . $new_foto,
                );
                // dd($data2);
                Dosen::create($data2);
            }
        toast('Data Dosen Berhasil Ditambahkan', 'success');
        return redirect()->route('dosen.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $dosen = Dosen::find($id);
        // dd($dosen);
        return view('Admin_Kemahasiswaan.Dosen.show', compact('dosen'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $kampus = Kampus::get();
        $prodi  = Prodi::get();
        $fakultas = Fakultas::get();

         $dosen = Dosen::find($id);
         $user = User::where('id', $dosen->user_id)->first();
        // $dosen = Dosen::where('user_id', auth()->user()->id)->find();




        return view('Admin_Kemahasiswaan.Dosen.edit', compact('kampus', 'prodi', 'fakultas', 'dosen', 'user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nip'               => 'required',
            'jenis_kelamin'     => 'required',
            'pendidikan'        => 'required',
            'email'             => 'required',
            'no_telpon'         => 'required',
            'alamat'            => 'required',
        ]);

        $dosen = Dosen::find($id);
        $user = User::where('id', $dosen->user_id)->first();
        $data       = $request->all();
        $data['password'] = bcrypt($data['password']);
        // dd($data);
        User::findorfail($user->id)->update($data);

        

        foreach ($data['nip'] as $key => $value) {
            $data2 = array(
                'kampus_id'     => $data['kampus_id'][$key],
                'prodi_id'      => $data['prodi_id'][$key],
                'fakultas_id'   => $data['fakultas_id'][$key],
                'user_id'       => $user->id,
                'nip'           => $data['nip'][$key],
                'jenis_kelamin' => $data['jenis_kelamin'][$key],
                'email'         => $user->email,
                'pendidikan'    => $data['pendidikan'][$key],
                'no_telpon'     => $data['no_telpon'][$key],
                'alamat'        => $data['alamat'][$key],
            );
            if ($request->has('foto_dosen')) {
                $foto           = $request->foto_dosen;
                $new_foto       = time() . 'produk' . $foto->getClientOriginalName();
                $tujuan_uploud  = 'uplouds/Produk/';
                $foto->move($tujuan_uploud, $new_foto);
                $data2['foto_dosen']    = $tujuan_uploud . $new_foto;
            }
            // dd($data2);
            Dosen::findorfail($id)->update($data2);
        }


        toast('Data Dosen Berhasil Di Update', 'success');
        return redirect()->route('dosen.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Dosen::where('id', $id)->delete();

        return redirect()->back();
    }
}