<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Datamatakuliah;
use App\Kampus;
use App\Prodi;

class DatamatakuliahController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Datamatakuliah::with('kampus', 'prodi')->paginate(10);

        return view('AdminAkademik.Datamatakuliah.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kampus = Kampus::all();
        $prodi  = Prodi::all();


        return view('AdminAkademik.Datamatakuliah.create', compact('kampus', 'prodi'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'semester'           => 'required',
            'kode_matakuliah'    => 'required',
            'nama_matakuliah'    => 'required',
            'jumlah_sks'         => 'required',
            'sifat_matakuliah'   => 'required',
        ]);

        $data       = $request->all();
        Datamatakuliah::create($data);

        toast('Data Mata kuliah berhasil di Tambahkan', 'success');
        return redirect()->route('datamatakuliah.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $kampus = Kampus::get();
        $prodi  = Prodi::get();

        $datamatakuliah = Datamatakuliah::find($id);

        return view('AdminAkademik.Datamatakuliah.edit', compact('kampus', 'prodi', 'datamatakuliah'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'semester'           => 'required',
            'kode_matakuliah'    => 'required',
            'nama_matakuliah'    => 'required',
            'jumlah_sks'         => 'required',
            'sifat_matakuliah'   => 'required',
        ]);

        $data              = $request->all();
        $datamatakuliah    = Datamatakuliah::findorfail($id);
        $datamatakuliah->update($data);

        toast('Data Mata Kuliah Berhasil Di Update', 'success');
        return redirect()->route('datamatakuliah.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Datamatakuliah::where('id', $id)->delete();

        return redirect()->back();
    }
}