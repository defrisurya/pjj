<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Camaba;
use App\Kampus;
use App\Prodi;
use App\Fakultas;
use App\Mahasiswa;
use Illuminate\Http\Request;

use App\Models\Province;
use App\Models\Regency;
use App\Models\District;
use App\Models\Village;
use App\User;

// Get semua data
$provinces = Province::all();
$regencies = Regency::all();
$districts = District::all();
$villages = Village::all();

class CamabaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data  = Camaba::with('kampus', 'prodi', 'fakultas', 'user')
            ->paginate(10);

        return view('Admin_Kemahasiswaan.Pendaftar.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Admin_Kemahasiswaan.Pendaftar.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $camaba = Camaba::find($id);
        $test = Prodi::where('id', $camaba->prodi_id)->first();

        return view('Admin_Kemahasiswaan.Pendaftar.show', compact('camaba', 'test'));
    }

    public function diterima($id, $key)
    {
        $value = Camaba::findOrFail($key);
        $test = Prodi::where('id', $value->prodi_id)->first();
        $data = User::findOrFail($id);
        
        $data->update([
            'role' => 'mahasiswa',
        ]);
        
        $mhs = new Mahasiswa;
        $mhs->user_id               = $data->id;
        $mhs->kampus_id             = $value->kampus_id;
        $mhs->prodi_id              = $value->prodi_id;
        $mhs->fakultas_id           = $value->fakultas_id;
        $mhs->camaba_id             = $value->id;
        $mhs->provinsi_id           = $value->provinsi_id;
        $mhs->kabupaten_id          = $value->kabupaten_id;
        $mhs->kecamatan_id          = $value->kecamatan_id;
        $mhs->desa_id               = $value->desa_id;
        $mhs->no_induk_mahasiswa    = substr($value->tahun_ajaran, 2) . $test->kode_prodi . $value->id;
        $mhs->no_induk_kependudukan = $value->nik;
        $mhs->nama_lengkap          = $value->nama_camaba;
        $mhs->jenis_kelamin         = $value->jenis_kelamin;
        $mhs->program_studi         = $test->nama_prodi;
        $mhs->agama                 = $value->agama;
        $mhs->tempat_lahir          = $value->tempat_lahir;
        $mhs->tanggal_lahir         = $value->tanggal_lahir;
        $mhs->email                 = $value->email;
        $mhs->no_telpon             = $value->no_tlp;
        $mhs->asal_sekolah          = $value->asal_sekolah;
        $mhs->alamat                = $value->alamat;
        $mhs->status                = 'Mahasiswa';
        //dd($mhs);
        $mhs->save();

        return redirect()->route('pendaftar.index');
    }

    public function ditolak($id)
    {
        $data = Camaba::find($id);

        $data->update([
            'status' => 'Success',
        ]);

        return redirect()->route('Admin_Kemahasiswaan.Pendaftar.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Camaba::where('id', $id)
            ->delete();

        return redirect()->back();
    }
}


 // $mhs = new Mahasiswa;
        // $mhs->kampus_id             = $value->kampus_id;
        // $mhs->prodi_id              = $value->prodi_id;
        // $mhs->fakultas_id           = $value->fakultas_id;
        // $mhs->camaba_id             = $value->id;
        // $mhs->provinsi_id           = $value->provinsi_id;
        // $mhs->kabupaten_id          = $value->kabupaten_id;
        // $mhs->kecamatan_id          = $value->kecamatan_id;
        // $mhs->desa_id               = $value->desa_id;
        // $mhs->no_induk_mahasiswa    = substr($value->tahun_ajaran, 2) . $test->kode_prodi . $value->id;
        // $mhs->no_induk_kependudukan = $value->nik;
        // $mhs->nama_lengkap          = $value->nama_camaba;
        // $mhs->jenis_kelamin         = $value->jenis_kelamin;
        // $mhs->program_studi         = $test->nama_prodi;
        // $mhs->agama                 = $value->agama;
        // $mhs->tempat_lahir          = $value->tempat_lahir;
        // $mhs->tanggal_lahir         = $value->tanggal_lahir;
        // $mhs->email                 = $value->email;
        // $mhs->no_telpon             = $value->no_tlp;
        // $mhs->asal_sekolah          = $value->asal_sekolah;
        // $mhs->alamat                = $value->alamat;
        // $mhs->status                = 'Mahasiswa';
        // $mhs->save();