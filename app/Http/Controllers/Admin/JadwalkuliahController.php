<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Jadwalkuliah;
use App\Dosen;
use App\Kampus;
use App\Prodi;
use Illuminate\Http\Request;

class JadwalkuliahController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data    = Jadwalkuliah::with('kampus', 'prodi', 'dosen')->paginate(10);

        return view('AdminAkademik.Jadwalkul.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kampus   = Kampus::all();
        $prodi     = Prodi::all();
        $dosen     = Dosen::all();

        return view('AdminAkademik.Jadwalkul.create', compact('kampus', 'prodi', 'dosen'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'tahun_akademik'     => 'required',
            'hari'               => 'required',
            'nama_matakuliah'    => 'required',
            'jumlah_sks'         => 'required',
            'sifat_matakuliah'   => 'required',
        ]);

        $data   = $request->all();
        Jadwalkuliah::create($data);

        toast('Data Jadwal Kuliah berhasil di tambahkan', 'success');
        return redirect()->route('jadwalkuliah.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $kampus     = Kampus::get();
        $prodi      = Prodi::get();
        $dosen      = Dosen::get();

        $datajadwalkuliah   = Jadwalkuliah::find($id);

        return view('AdminAkademik.Jadwalkul.edit', compact('kampus', 'prodi', 'dosen', 'datajadwalkuliah'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'tahun_akademik'     => 'required',
            'hari'               => 'required',
            'nama_matakuliah'    => 'required',
            'jumlah_sks'         => 'required',
            'sifat_matakuliah'   => 'required',
        ]);

        $data              = $request->all();
        $datajadwalkuliah  = Jadwalkuliah::findorfail($id);
        $datajadwalkuliah->update($data);

        toast('Data Jadwal Kuliah berhasil di Update', 'success');
        return redirect()->route('jadwalkuliah.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Jadwalkuliah::where('id', $id)->delete();

        return redirect()->back();
    }
}