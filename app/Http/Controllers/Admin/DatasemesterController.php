<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Datasemester;
use App\Kampus;
use Illuminate\Http\Request;

class DatasemesterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $kampus = Kampus::get();

        $data = Datasemester::with('kampus')->paginate(10);

        return view('AdminAkademik.Datasemester.index', compact('data', 'kampus'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'tahun_akademik'   => 'required',
            'semester'           => 'required',
        ]);

        $data = $request->all();
        Datasemester::create($data);

        toast('Data Berhasil DItambahkan', 'success');
        return redirect()->route('datasemester.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $kampus  = Kampus::get();
        $datasemester   = Datasemester::find($id);

        return view('AdminAkademik.Datasemester.edit', compact('kampus', 'datasemester'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'tahun_akademik'   => 'required',
            'semester'           => 'required',
        ]);

        $data        = $request->all();
        $semester    = Datasemester::findorfail($id);
        $semester->update($data);

        toast('Data Semester berhasil di Update', 'success');
        return redirect()->route('datasemester.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Datasemester::where('id', $id)->delete();

        return redirect()->back();
    }
}