<?php

namespace App\Http\Controllers\Admin;

use App\Datatranskipnilai;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Kampus;
use App\Prodi;
use App\Dosen;
use App\Mahasiswa;

class DatatranskipnilaiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       //$data = Datatranskipnilai::with('kampus', 'prodi', 'dosen')->paginate(10);

       // return view('AdminAkademik.Datatranskipnilai.index', compact('data'));
       return view('AdminAkademik.Datatranskipnilai.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $kampus   = Kampus::all();
        $prodi     = Prodi::all();
        $dosen     = Dosen::all();

        return view('AdminAkademik.Datatranskipnilai.create', compact('kampus','prodi','dosen'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(   /* $id  */  )
    {
        //
        return view('AdminAkademik.Datatranskipnilai.edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
