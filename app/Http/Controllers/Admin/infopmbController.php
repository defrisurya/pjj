<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Infopmb;
use App\Kampus;
use Illuminate\Http\Request;
use Symfony\Polyfill\Intl\Idn\Info;

class infopmbController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Infopmb::with('kampus')->paginate(10);

        return view('Admin_Kemahasiswaan.Infopmb.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kampus   = Kampus::all();

        return view('Admin_Kemahasiswaan.Infopmb.create', compact('kampus'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'judul_info'        => 'required',
            'tahapanseleksi'    => 'required',
            'waktupendaftaran_start'  => 'required',
            'waktupendaftaran_end'  => 'required',
            'biayapendaftaran'  => 'required',
        ]);

        $data_array = [
            'seleksi_1' => $request->tahapanseleksi,
            'seleksi_2' => $request->tahapanseleksi_2,
            'seleksi_3' => $request->tahapanseleksi_3,
            'seleksi_4' => $request->tahapanseleksi_4,
            'seleksi_5' => $request->tahapanseleksi_5,
            'seleksi_6' => $request->tahapanseleksi_6,
            'seleksi_7' => $request->tahapanseleksi_7,
            'seleksi_8' => $request->tahapanseleksi_8,
        ];

        // $data = $request->all();

        $data['kampus_id'] = $request->kampus_id;
        $data['judul_info'] = $request->judul_info;
        $data['waktupendaftaran_start'] = $request->waktupendaftaran_start;
        $data['waktupendaftaran_end'] = $request->waktupendaftaran_end;
        $data['biayapendaftaran'] = $request->biayapendaftaran;
        $data['tahun_akademik'] = $request->tahun_akademik;
        $data['tahapanseleksi'] = json_encode($data_array);

        if ($request->has('fotoposter')) {
            $foto           = $request->fotoposter;
            $new_foto       = time() . 'infoPMB' . $foto->getClientOriginalName();
            $tujuan_uploud  = 'uplouds/InfoPMB/';
            $foto->move($tujuan_uploud, $new_foto);
            $data['fotoposter']   = $tujuan_uploud . $new_foto;
        }

        // dd($data);

        Infopmb::create($data);

        toast('Data Berhasil Ditambahkan', 'success');
        return redirect()->route('infopmb.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $kampus = Kampus::get();

        $infopmb = Infopmb::find($id);

        $json = json_decode($infopmb->tahapanseleksi, true);

        return view('Admin_Kemahasiswaan.Infopmb.edit', compact('infopmb', 'kampus', 'json'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'judul_info'        => 'required',
            'tahapanseleksi'    => 'required',
            'waktupendaftaran_start'  => 'required',
            'waktupendaftaran_end'  => 'required',
            'biayapendaftaran'  => 'required',
        ]);

        $data_array = [
            'seleksi_1' => $request->tahapanseleksi,
            'seleksi_2' => $request->tahapanseleksi_2,
            'seleksi_3' => $request->tahapanseleksi_3,
            'seleksi_4' => $request->tahapanseleksi_4,
            'seleksi_5' => $request->tahapanseleksi_5,
            'seleksi_6' => $request->tahapanseleksi_6,
            'seleksi_7' => $request->tahapanseleksi_7,
            'seleksi_8' => $request->tahapanseleksi_8,
        ];

        $info = Infopmb::findorfail($id);

        if ($request->has('fotoposter')) {

            //upload new image
            $foto = $request->fotoposter;
            $new_foto = time() . 'infoPMB' . $foto->getClientOriginalName();
            $tujuan_uploud = 'uplouds/InfoPMB/';
            $foto->move($tujuan_uploud, $new_foto);

            //delete old image in local
            if (file_exists(($info->logo))) {
                unlink(($info->logo));
            }

            //update with new image
            $info->update([
                'fotoposter'              => $tujuan_uploud . $new_foto,
                'kampus_id'               => $request->kampus_id,
                'judul_info'              => $request->judul_info,
                'waktupendaftaran_start'  => $request->waktupendaftaran_start,
                'waktupendaftaran_end'    => $request->waktupendaftaran_end,
                'biayapendaftaran'        => $request->biayapendaftaran,
                'tahun_akademik'          => $request->tahun_akademik,
                'tahapanseleksi'          => json_encode($data_array),
            ]);
        } else {
            //update without image
            $info->update([
                'kampus_id'               => $request->kampus_id,
                'judul_info'              => $request->judul_info,
                'waktupendaftaran_start'  => $request->waktupendaftaran_start,
                'waktupendaftaran_end'    => $request->waktupendaftaran_end,
                'biayapendaftaran'        => $request->biayapendaftaran,
                'tahun_akademik'          => $request->tahun_akademik,
                'tahapanseleksi'          => json_encode($data_array),
            ]);
        }

        toast('Data Info PMB Berhasil Di Update', 'success');
        return redirect()->route('infopmb.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Infopmb::where('id', $id)->delete();

        return redirect()->back();
    }
}
