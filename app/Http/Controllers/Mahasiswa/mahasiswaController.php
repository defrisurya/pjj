<?php

namespace App\Http\Controllers\Mahasiswa;

use App\Http\Controllers\Controller;
use App\Http\Requests\Mahasiswa\MahasiswaCreate;
use App\Http\Requests\Mahasiswa\MahasiswaUpdate;
use App\Mahasiswa;
use Illuminate\Http\Request;

use App\Models\Province;
use App\Models\Regency;
use App\Models\District;
use App\Models\Village;

class mahasiswaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function index()
    {
        // $mahasiswa = Mahasiswa::with('prodi', 'fakultas')->find($id);

        return view('Mahasiswa.profile.profile');
    }

    public function krs()
    {
        return view('Mahasiswa.krs.krs');
    }

    public function khs()
    {
        return view('Mahasiswa.khs.khs');
    }

    public function nilai()
    {
        return view('Mahasiswa.transkrip.nilai');
    }

    public function learning()
    {
        return view('Mahasiswa.learning.learning');
    }

    public function materi()
    {
        return view('Mahasiswa.learning.materi');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $mahasiswa = Mahasiswa::where('user_id', auth()->user()->id)->first();
        $mhs = Mahasiswa::where('user_id', auth()->user()->id)->first();

        return view('Mahasiswa.profile.create', compact('mahasiswa', 'mhs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        Mahasiswa::create($data);

        toast('Data Prodi Berhasil di Tambahkan !!!');
        return redirect()->route('Mahasiswa.profile.profile');
    }

    public function mhscreate($id)
    {
        $province  = Province::all();
        // $mahasiswa = Mahasiswa::with('prodi', 'fakultas')->find($id);
        $mahasiswa = Mahasiswa::with('prodi', 'fakultas', 'camaba')->find($id);

        return view('Mahasiswa.profile.profile', compact('province', 'mahasiswa'));
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        $mahasiswa = Mahasiswa::where('user_id', auth()->user()->id)->first();

        return view('Mahasiswa.profile.edit', compact('mahasiswa'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function update(MahasiswaUpdate $request, Mahasiswa $mahasiswa)
    public function update(Request $request, $id)
    {
        /* $request->validate([
             'no_induk_mahasiswa'             => 'required',
             'nama_lengkap'                   => 'required',
             'jenis_kelamin'                  => 'required',
             'agama'                          => 'required',
             'program_studi'                  => 'required',
             'kewarganegaraan'                => 'required',
             'tempat_lahir'                   => 'required',
             'tanggal_lahir'                  => 'required',
             'email'                          => 'required',
             'no_telpon'                      => 'required',
             'asal_sekolah'                   => 'required',
             'alamat'                         => 'required',
             'kode_pos'                       => 'required',
             'no_induk_kependudukan_ayah'     => 'required',
             'nama_lengkap_ayah'              => 'required',
             'tanggal_lahir_ayah'             => 'required',
             'pendidikan_ayah'                => 'required',
             'pekerjaan_ayah'                 => 'required',
             'rentang_penghasilan_ayah'       => 'required',
             'no_telpon_ayah'                 => 'required',
             'alamat_ayah'                    => 'required',
             'no_induk_kependudukan_ibu'      => 'required',
             'nama_lengkap_ibu'               => 'required',
             'tanggal_lahir_ibu'              => 'required',
             'pendidikan_ibu'                 => 'required',
             'pekerjaan_ibu'                  => 'required',
             'rentang_penghasilan_ibu'        => 'required',
             'no_telpon_ibu'                  => 'required',
             'alamat_ibu'                     => 'required',
         ]); */

        $data         = $request->all();
        $mahasiswa    = Mahasiswa::findorfail($id);

        $mahasiswa->update($data);

        toast('Data Berhasil di Simpan', 'success');
        //return redirect()->route('Mahasiswa.profile.profile');
        return redirect()->route('mhs');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    }
}
