<?php

namespace App\Http\Controllers;

use App\Infopmb;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Kampus;
use App\Prodi;
use App\User;
use Auth;

class frontController extends Controller
{
    //

    public function index(Request $request)
    {
        // dd(request('search'));
        $kampus = Kampus::limit(8)->get();

        if ($request->cari) {
            if ($request->cari == "nama_kampus") {
                $kampus = Kampus::where("nama_kampus")->latest();
            } else {
                $kampus = Kampus::where(function ($kampus) use ($request) {
                    $kampus->where("nama_kampus", 'like', "%$request->cari%");
                })->latest()->paginate(8);
            }
        }

        return view('Front.dashboard', compact('kampus'));
    }

    public function allkampus(Request $request)
    {
        $kampus = Kampus::paginate(8);

        if ($request->cari) {
            if ($request->cari == "nama_kampus") {
                $kampus = Kampus::where("nama_kampus")->latest();
            } else {
                $kampus = Kampus::where(function ($kampus) use ($request) {
                    $kampus->where("nama_kampus", 'like', "%$request->cari%");
                })->latest()->paginate(8);
            }
        }

        return view('Front.all-kampus', compact('kampus'));
    }

    public function profilkampus($slug)
    {
        $data = Kampus::where('slug', $slug)->first();
        $jmlprodi = Prodi::where('kampus_id', $data->id)->count();
        $prodi = Prodi::with('jenj_pend')->where('kampus_id', $data->id)->get();
        $infoPMB = Infopmb::where('kampus_id', $data->id)->first();

        // dd($prodi);
        return view('Front.profile-kampus', compact('data', 'jmlprodi', 'prodi', 'infoPMB'));
    }

    // public function daftar(Request $request)
    // {
    //     $data = $request->all();
    //     $data['role'] = 'camaba';
    //     $data['password'] = Hash::make($data['password']);

    //     // dd($data);

    //     User::create($data);

    //     return redirect()->route('login');
    // }
}
