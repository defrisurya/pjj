<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */

    protected $redirectTo = RouteServiceProvider::HOME;

    // protected $redirectTo = '/login'; // '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name'     => ['required', 'string', 'max:255'],
            'email'    => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'role'     => ['required'],
            'password' => ['required'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name'       => $data['name'],
            'email'      => $data['email'],
            'role'       => $data['role'], // default
            'password'   => Hash::make($data['password']),
            // 'foto1'      => $data['foto1'], // default
        ]);

        // $user->save();
    }
}


// $data = $request->all();
        // //  dd($data);
        // $data['role'] = 'camaba';
        // $data['password'] = Hash::make($data['password']);

        // if ($request->has('foto1')) {
        //     $foto           = $request->foto1;
        //     $new_foto       = time() . 'camaba' . $foto->getClientOriginalName();
        //     $tujuan_uploud  = 'uplouds/camaba/';
        //     $foto->move($tujuan_uploud, $new_foto);
        //     $data['foto1']   = $tujuan_uploud . $new_foto;
        // }
        // User::create($data);
