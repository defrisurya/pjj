<?php

namespace App\Http\Controllers;

use App\Kampus;
use Illuminate\Http\Request;
use App\User;
use App\Camaba;
use App\Fakultas;
use App\Prodi;
use App\File;
use App\Dosen;
use App\Models\Province;
use App\Models\Regency;
use App\Models\District;
use App\Models\Village;
use App\Mahasiswa;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // verifikasi email
        // $this->middleware(['auth','verified']);
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        // dd(request('search'));
        // role mahasiswa
        if (auth()->user()->role == 'mahasiswa') {
            return redirect()->route('mhs');
        }

        // role camaba
        if (auth()->user()->role == 'camaba') {
            return redirect()->route('kampusku');
        }

        // role dosen
        if (auth()->user()->role == 'admindosen') {
            return redirect()->route('dosen');
        }

        return view('home');
    }

    public function dasindex()
    {
        $kampus = Kampus::all();

        return view('Front.dashboard', compact('kampus'));
    }

    public function mhs()
    {
        $mhs = Mahasiswa::where('user_id', auth()->user()->id)->first();

        return view('Mahasiswa.dashboard', compact('mhs'));
    }

    //dosen
    public function dosen(/* $id */)
    {
        $data = Dosen::first();
        // dd($data);

        //return view('Admindosen.Profildosen.index');
        return view('Admindosen.Profildosen.index', compact('data'));
    }

    public function pmb()
    {
        return view('Pmb.dashboard');
    }

    public function daftarpmb($slug)
    {
        // Get semua data
        $province = Province::all();
        $kampus = Kampus::with('prodi', 'fakultas')->where('slug', $slug)->first();
        $user = User::where('id', auth()->user()->id)->first();
        return view('Pmb.pendaftaran.pendaftaranpmb', compact('kampus', 'province', 'user'));
    }

    public function getkabupaten(Request $request)
    {
        $id_provinsi = $request->id_provinsi;

        $kabupatens = Regency::where('province_id', $id_provinsi)->get();

        foreach ($kabupatens as $kabupaten) {
            echo "<option value='$kabupaten->id'>$kabupaten->name</option>";
        }
    }

    public function getkecamatan(Request $request)
    {
        $id_kabupaten = $request->id_kabupaten;

        $kecamatans = District::where('regency_id', $id_kabupaten)->get();

        foreach ($kecamatans as $kecamatan) {
            echo "<option value='$kecamatan->id'>$kecamatan->name</option>";
        }
    }

    public function getdesa(Request $request)
    {
        $id_kecamatan = $request->id_kecamatan;

        $desas = Village::where('district_id', $id_kecamatan)->get();

        foreach ($desas as $desa) {
            echo "<option value='$desa->id'>$desa->name</option>";
        }
    }

    public function daftarpmbstore(Request $request)
    {
        $request->validate([
            'foto' => 'file|image|mimes:jpeg,png,jpg|max:1024',
            'file_ijazah' => 'file|mimes:pdf,xlx,csv|max:1024',
            'file_transkrip' => 'file|mimes:pdf,xlx,csv|max:1024',
            'file_raport' => 'file|mimes:pdf,xlx,csv|max:1024',
            'kampus_id' => 'required',
            'prodi_id' => 'required',
            'user_id' => 'required',
            'fakultas_id' => 'required',
            'provinsi_id' => 'required',
            'kabupaten_id' => 'required',
            'kecamatan_id' => 'required',
            'desa_id' => 'required',
            'nama_camaba' => 'required',
            'tempat_lahir' => 'required',
            'tanggal_lahir' => 'required',
            'jenis_kelamin' => 'required',
            'agama' => 'required',
            'email' => 'required',
            'no_tlp' => 'required',
            'alamat' => 'required',
            'asal_sekolah' => 'required',
            'tahun_ajaran' => 'required',
            'nik' => 'required',
        ]);

        $data = $request->all();

        // dd($data);

        if ($request->has('foto')) {
            $foto           = $request->foto;
            $new_foto       = time() . 'FileCamaba' . $foto->getClientOriginalName();
            $tujuan_uploud  = 'uplouds/FileCamaba/';
            $foto->move($tujuan_uploud, $new_foto);
            $data['foto']   = $tujuan_uploud . $new_foto;
        }
        //upload ijazah, tanskip ,raport
        if ($request->has('file_ijazah')) {
            $file_ijazah         = $request->file_ijazah;
            $new_foto            = time() . 'FileCamaba' . $file_ijazah->getClientOriginalName();
            $tujuan_uploud       = 'uplouds/FileCamaba/';
            $file_ijazah->move($tujuan_uploud, $new_foto);
            $data['file_ijazah'] = $tujuan_uploud . $new_foto;
        }
        //transkip
        if ($request->has('file_transkrip')) {
            $file_transkrip         = $request->file_transkrip;
            $new_foto              = time() . 'FileCamaba' . $file_transkrip->getClientOriginalName();
            $tujuan_uploud         = 'uplouds/FileCamaba/';
            $file_transkrip->move($tujuan_uploud, $new_foto);
            $data['file_transkrip'] = $tujuan_uploud . $new_foto;
        }
        //raport
        if ($request->has('file_raport')) {
            $file_raport         = $request->file_raport;
            $new_foto            = time() . 'FileCamaba' . $file_raport->getClientOriginalName();
            $tujuan_uploud       = 'uplouds/FileCamaba/';
            $file_raport->move($tujuan_uploud, $new_foto);
            $data['file_raport'] = $tujuan_uploud . $new_foto;
        }

        Camaba::create($data);

        toast('Data berhasil Ditambahkan', 'success');
        return redirect('/pmb');
    }
}
