<?php

namespace App\Http\Controllers\Pmb;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\ProdiCreate;
use App\Kampus;
use App\Prodi;
use App\Fakultas;
use App\Camaba;
use Illuminate\Http\Request;

class pmbController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('Pmb.dashboard');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $kampus    = Kampus::all();
        $prodi     = Prodi::all();
        $fakultas  = Fakultas::all();

        return view('Pmb.pendaftaran.pendaftaranpmb', compact('kampus', 'prodi', 'fakultas'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $request->validate([
        //     'nama_camaba'       => 'required',
        //     'tempat_lahir'      => 'required',
        //     'tanggal_lahir'     => 'required',
        //     'jenis_kelamin'     => 'required',
        //     'agama'             => 'required',
        //     'email'             => 'required',
        //     'no_tlp'            => 'required',
        //     'alamat'            => 'required',
        //     'asal_provinsi'     => 'required',
        //     'asal_kabupaten'    => 'required',
        //     'asal_sekolah'      => 'required',
        //     'tahun_ajaran'      => 'required',
        //     'nik'               => 'required',
        // ]);

        $request->validate([
            'foto'           => 'required|max:1000|mimes:png,jpg,jpeg',
            'file_ijazah'    => 'required|max:1000|mimes:pdf',
            'file_transkrip' => 'required|max:1000|mimes:pdf',
            'file_raport'    => 'required|max:1000|mimes:pdf',
        ]);

        $data = $request->all();

        dd($data);
        
        if ($request->has('foto')) {
            $foto             = $request->foto;
            $new_foto         = time() . 'fotopendaftar' . $foto->getClientOriginalName();
            $tujuan_upload    = 'uploads/fotopendaftar';
            $foto->move($tujuan_upload, $new_foto);
            $data['foto'] = $new_foto;
        }
        if ($request->has('file_ijazah')) {
            $foto             = $request->file_ijazah;
            $new_foto         = time() . 'filependaftar' . $foto->getClientOriginalName();
            $tujuan_upload    = 'uploads/filependaftar';
            $foto->move($tujuan_upload, $new_foto);
            $data['file_ijazah'] = $new_foto;
        }

        if ($request->has('file_transkip')) {
            $foto           = $request->file_transkip;
            $new_foto       = time() . 'filependaftar' . $foto->getClientOriginalName();
            $tujuan_upload  = 'uploads/filependaftar';
            $foto->move($tujuan_upload, $new_foto);
            $data['file_transkip'] = $new_foto;
        }

        if ($request->has('file_raport')) {
            $foto           = $request->file_transkip;
            $new_foto       = time() . 'filependaftar' . $foto->getClientOriginalName();
            $tujuan_upload  = 'uploads/filependaftar';
            $foto->move($tujuan_upload, $new_foto);
            $data['file_raport'] = $new_foto;
        }

        Camaba::create($data);

        // toast('Data Cah maba berhasil di Tambahkan Oyeeacchh ', 'success');
        return redirect()->route('pmb');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}