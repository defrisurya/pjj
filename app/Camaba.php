<?php

namespace App;

use App\Models\District;
use App\Models\Regency;
use App\Models\Village;
use Illuminate\Database\Eloquent\Model;

class Camaba extends Model
{
    protected $table = 'camabas';
    protected $fillable = [
        'kampus_id',
        'user_id',
        'prodi_id',
        'fakultas_id',
        'provinsi_id',
        'kabupaten_id',
        'kecamatan_id',
        'desa_id',
        'nama_camaba',
        'tempat_lahir',
        'tanggal_lahir',
        'jenis_kelamin',
        'agama',
        'email',
        'no_tlp',
        'alamat',
        'asal_sekolah',
        'tahun_ajaran',
        'nik',
        'foto',
        'file_ijazah',
        'file_transkrip',
        'file_raport',
    
    ];

    public function kampus()
    {
        return $this->hasOne(Kampus::class, 'id', 'kampus_id');
    }

    public function user()
    {
        return $this->hasOne(User::class, 'id');
    }

    public function prodi()
    {
        return $this->hasOne(Prodi::class, 'id', 'prodi_id');
    }

    public function fakultas()
    {
        return $this->hasOne(Fakultas::class, 'id', 'fakultas_id');
    }

    public function province()
    {
        return $this->hasOne(Province::class, 'id', 'provinsi_id');
    }

    public function regency()
    {
        return $this->hasOne(Regency::class, 'id', 'kabupaten_id');
    }

    public function districts()
    {
        return $this->hasOne(District::class, 'id', 'kecamatan_id');
    }

    public function village()
    {
        return $this->hasOne(Village::class, 'id', 'desa_id');
    }
}
