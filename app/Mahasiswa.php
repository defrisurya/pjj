<?php

namespace App;

use App\Models\District;
use App\Models\Province;
use App\Models\Regency;
use App\Models\Village;
use Illuminate\Database\Eloquent\Model;

class Mahasiswa extends Model
{
    //
    public $guarded = [];

    protected $table = 'mahasiswas';

    public function kampus()
    {
        return $this->hasOne(Kampus::class, 'id', 'kampus_id');
    }

    public function prodi()
    {
        return $this->hasOne(Prodi::class, 'id', 'prodi_id');
    }

    public function fakultas()
    {
        return $this->hasOne(Fakultas::class, 'id', 'fakultas_id');
    }

    public function province()
    {
        return $this->hasOne(Province::class, 'id', 'provinsi_id');
    }

    public function regency()
    {
        return $this->hasOne(Regency::class, 'id', 'kabupaten_id');
    }

    public function districts()
    {
        return $this->hasOne(District::class, 'id', 'kecamatan_id');
    }

    public function village()
    {
        return $this->hasOne(Village::class, 'id', 'desa_id');
    }
}