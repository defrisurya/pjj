<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JenjangPend extends Model
{
    protected $table = 'jenjang_pends';

    protected $guarded = [];
}
