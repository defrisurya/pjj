<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jadwalkuliah extends Model
{
    public $guarded = [];

    protected $table = 'jadwalkuliahs';

    public function kampus()
    {
        return $this->hasOne(Kampus::class, 'id', 'kampus_id');
    }

    public function prodi()
    {
        return $this->hasOne(Prodi::class, 'id', 'prodi_id');
    }

    public function dosen()
    {
        return $this->hasOne(Dosen::class, 'id', 'dosen_id');
    }
}