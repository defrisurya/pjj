<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Infopmb extends Model
{
    protected $table = 'info_pmbs';

    protected $guarded = [];

    public function kampus()
    {
        return $this->hasOne(Kampus::class, 'id', 'kampus_id');
    }
}
