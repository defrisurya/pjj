<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Carbon;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        config(['app.locale' => 'id']);
        Carbon::setLocale('id');

        Gate::define('isAdmin', function ($user) {
            return $user->role == 'admin';
        });

        Gate::define('isMahasiswa', function ($user) {
            return $user->role == 'mahasiswa';
        });

        Gate::define('isAdminAkademik', function ($user) {
            return $user->role == 'adminakademik';
        });
        Gate::define('isAdminDosen', function ($user) {
            return $user->role == 'admindosen';
        });
    }
}
