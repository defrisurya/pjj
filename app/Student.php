<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    //
    public $guarded = [];

    protected $table = 'mahasiswas';

    public function kampus()
    {
        return $this->hasOne(Kampus::class, 'id', 'kampus_id');
    }

    public function prodi()
    {
        return $this->hasOne(Prodi::class, 'id', 'prodi_id');
    }
}
