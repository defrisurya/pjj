<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dosen extends Model
{
    //
    protected $table = 'dosens';

    protected $fillable = [
        'kampus_id',
        'prodi_id',
        'fakultas_id',
        'user_id',
        'nip',
        'jenis_kelamin',
        'pendidikan',
        'email',
        'no_telpon',
        'foto_dosen',
        'alamat',
    ];

    public function kampus()
    {
        return $this->hasOne(Kampus::class, 'id', 'kampus_id');
    }
    
    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function prodi()
    {
        return $this->hasOne(Prodi::class, 'id', 'prodi_id');
    }

    public function fakultas()
    {
        return $this->hasOne(Fakultas::class, 'id', 'fakultas_id');
    }
}
