<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Datasemester extends Model
{
    //
    protected $table = 'semesters';

    protected $fillable = [
        'kampus_id',
        'tahun_akademik',
        'semester',
    ];

    public function kampus()
    {
        return $this->hasOne(Kampus::class, 'id', 'kampus_id');
    }
}
