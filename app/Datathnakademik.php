<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Datathnakademik extends Model
{
    //
    protected $table = 'akademiks';

    protected $fillable = [
        'kampus_id',
        'tahun_akademik',
        'status',
    ];

    public function kampus()
    {
        return $this->hasOne(Kampus::class, 'id', 'kampus_id');
    }
}
