<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Prodi extends Model
{
    protected $guarded = [];

    public function kampus()
    {
        return $this->hasOne(Kampus::class, 'id', 'kampus_id');
    }

    public function fakultas()
    {
        return $this->hasOne(Fakultas::class, 'id', 'fakultas_id');
    }

    public function jenj_pend()
    {
        return $this->hasOne(JenjangPend::class, 'id', 'jenjang_pend_id');
    }
}
