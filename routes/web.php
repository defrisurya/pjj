<?php

// use App\Http\Controllers\IndoregionController
use App\Http\Controllers\Mahasiswa\mahasiswaController;
use App\Kampus;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|   // $kampus = Kampus::paginate(8);
*/

Route::get('/', 'frontController@index')->name('kampusku');
Route::get('profilkampus/{id}', 'frontController@profilkampus')->name('profilkampus');
Route::get('/DaftarSemuaKampus', 'frontController@allkampus')->name('all-kampus');

// Auth::routes(['verify' => true]);
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/siakad', 'HomeController@mhs')->name('mhs');
// dosen
Route::get('/dosen', 'Admin\ProfildosenController@index')->name('dosen');


Route::get('/pmb', 'HomeController@pmb')->name('pmb');
Route::get('/daftarpmb/{id}', 'HomeController@daftarpmb')->name('daftarpmb');
Route::post('/daftarpmb/daftarpmbstore', 'HomeController@daftarpmbstore')->name('daftarpmbstore');
Route::post('/getkabupaten', 'HomeController@getkabupaten')->name('getkabupaten');
Route::post('/getkecamatan', 'HomeController@getkecamatan')->name('getkecamatan');
Route::post('/getdesa', 'HomeController@getdesa')->name('getdesa');

//Camaba
Route::group(['middleware' => ['auth', 'CekRole:camaba']], function () {
    Route::get('formpendaftaran', 'Pmb\pmbController@create')->name('form.daftar');
    Route::post('pendaftaran', 'Pmb\pmbController@store')->name('simpan.daftar');
});

Route::group(['middleware' => ['auth', 'CekRole:admin'], 'prefix' => 'Admin_Kemahasiswaan'], function () {
    Route::namespace('Admin')->group(function () {
        Route::resource('kampus', 'kampusController')->parameters(['kampus' => 'kampus',]);
        Route::resource('prodi', 'prodiController');
        Route::resource('fakultas', 'FakultasController');
        // Route::resource('dataprodikampus', 'prodikampusController');
        Route::resource('infopmb', 'infopmbController');
        Route::resource('pendaftar', 'CamabaController');
        Route::resource('student', 'StudentController');
        Route::resource('dosen', 'DosenController');
        Route::get('diterima/{id}/{key}', 'CamabaController@diterima')->name('diterima');
    });
});

Route::group(['middleware' => ['auth', 'CekRole:mahasiswa'], 'prefix' => 'Mahasiswa'], function () {
    Route::namespace('Mahasiswa')->group(function () {

        /* Route::get('/siswa','SiswaController@index');
        Route::post('/siswa/create','SiswaController@create');
        Route::get('/siswa/{siswa}/edit','SiswaController@edit');
        Route::post('/siswa/{siswa}/update','SiswaController@update');
        Route::get('/siswa/{siswa}/delete','SiswaController@delete');
        Route::get('/siswa/{siswa}/profile','SiswaController@profile');
        Route::post('/siswa/{siswa}/addnilai','SiswaController@addnilai'); */

        // Route::get('/mahasiswa/{mahasiswa}/edit','mahasiswaController@edit');

        // Route::get('/mahasiswa/{mahasiswa}/edit', 'mahasiswaController@edit');
        Route::resource('mahasiswa', 'mahasiswaController');
        // Route::resource('mahasiswa', 'mahasiswaController')->parameters(['mahasiswa' => 'mahasiswa',]);
        Route::get('krs', 'mahasiswaController@krs')->name('krs');
        Route::get('khs', 'mahasiswaController@khs')->name('khs');
        Route::get('transkrip', 'mahasiswaController@nilai')->name('nilai');
        Route::get('Elearning', 'mahasiswaController@learning')->name('learning');
        Route::get('materi', 'mahasiswaController@materi')->name('materi');
    });
});

Route::group(['middleware' => ['auth', 'verified', 'CekRole:pmb'], 'prefix' => 'Pmb'], function () {
    Route::namespace('Pmb')->group(function () {
        Route::resource('pmb', 'pmbController')->parameters(['pmb' => 'pmb',]);
        Route::get('pendaftaran', 'pmbController@pendaftaran')->name('pendaftaran');
    });
});

Route::group(['middleware' => ['auth', 'CekRole:adminakademik'], 'prefix' => 'Adminakademik'], function () {
    Route::namespace('Admin')->group(function () {
        Route::resource('datatahunakademik', 'DatathnakademikController')->parameters(['datatahunakademik' => 'datatahunakademik',]);
        Route::resource('datasemester', 'DatasemesterController');
        Route::resource('datamatakuliah', 'DatamatakuliahController');
        Route::resource('jadwalkuliah', 'JadwalkuliahController');
        Route::resource('datakrs', 'DatakrsController');
        Route::resource('datakhs', 'DatakhsController');
        Route::resource('datatranskipnilai', 'DatatranskipnilaiController');
    });
});

Route::group(['middleware' => ['auth', 'CekRole:admindosen'], 'prefix' => 'Admindosen'], function () {
    Route::namespace('Admin')->group(function () {
        Route::resource('profildosen', 'ProfildosenController')->parameters(['profildosen' => 'profildosen',]);
        Route::resource('jadwaldosen', 'JadwalkuliahdosenController');
        Route::resource('absensi', 'AbsensiController');
        Route::resource('modulmateri', 'ModulmateriController');
        Route::get('/detailmateri/{id}/materi', 'ModulmateriController@materi')->name('detailmateri');
    });
});
